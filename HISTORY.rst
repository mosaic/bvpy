=======
History
=======

creation (2020-08-26)
------------------------

* First release on PyPI.

version 1.1.0 (2024-03-25)
--------------------------

### **New Features**

1. Added **PyVista** module `visu_pysta` for visualization.

2. Improved handling of **3D geometry (tetra)** in `domains`.

3. Improved access to solver parameters for better usability and flexibility:

    - List all the parameters that can be access in `parameter.py`

version 1.3.0 (2025-01-11)
--------------------------

### **Bug Fixes**

- Fixed normal computation in the `boundary_normal` function for 3D cases.

---

### **New Features**

1. **API Enhancements for Hyperelastic Models**

   - Introduced new classes for potential energy models:

     - `NeoHookeanPotential`
     - `StVenantKirchoffPotential`
     - `QuadraticGreenLagrangePotential`

   - Updated the API of `HyperElasticForm` to now require a potential energy as an argument. This change simplifies the implementation of new potential energy models.

2. **Morphoelastic Problem Modeling**

   - Added `MorphoElasticForm` and `GBVP` classes to support modeling morphoelastic problems.

3. **Domain and Mesh Handling**

   - Added `CustomDomainGmsh` class, which allows loading `.msh` files for working with complex domains.

4. **Growth Laws and Heterogeneous Growth**

   - Added classes for different growth laws:

     - `ConstantGrowth`
     - `TimeDependentGrowth`
     - `StrainDependentGrowth`

   - Introduced the `HeterogeneousGrowth` class, enabling assignment of different growth laws to various subdomains.

5. **Boundary Conditions Enhancement**
   - Improved boundary condition handling. Boundary conditions can now be scalar, vector, or tensor values with variables, increasing flexibility.

---

### **Documentation and Tutorials**

- Added new tutorials:

  - **Visualization**: Demonstrations using `pyvista`.
  - **Domain Creation**: Tutorials on working with complex domains using `gmsh`.
  - **Morphoelastic Problems**: Examples of handling morphoelastic problems.

version 1.3.1 (2025-01-15)
--------------------------

### **Bug Fixes**

- Fixed issues of missing packages with the Anaconda package `bvpy`.

---

### **Improvements**

1. **Installation Improvements**:

   - Improved the documentation installation procedure for simplicity and clarity.

2. **Repository Cleanup**:

   - Cleaned and reorganized the repository structure (e.g., data files, old setup files).

3. **Example Module**:

   - Added a lightweight `example` module that simplifies verification of the installation. This module includes a basic example script that helps users validate their environment and confirm successful installation.


---

### **Documentation Enhancements**

- Added a downloadable `.zip` archive of tutorials with associated data, available directly in the documentation.
