=======
Credits
=======

Development Lead
----------------

* Manuel Petit, <manuel.petit@inria.fr>


Main Contributors
------------

* ALI Olivier <olivier.ali@inria.fr>
* Florian Gacon, <florian.gacon@inria.fr>


Other Contributors
------------------

* Jonathan Legrand
* Guillaume Cerutti
* Adrien Heymans
* Gonzalo Revilla

