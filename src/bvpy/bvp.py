#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.bvp
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe

from functools import reduce
import operator

from ufl.classes import Zero

from typing import Union

from bvpy import logger
from bvpy.domains.abstract import AbstractDomain
from bvpy.vforms.abstract import AbstractVform, Element
from bvpy.boundary_conditions.dirichlet import (ConstantDirichlet,
                                                VariableDirichlet,
                                                ZeroDirichlet,
                                                NormalDirichlet)
from bvpy.boundary_conditions.neumann import (ConstantNeumann, VariableNeumann,
                                              NormalNeumann)
from bvpy.boundary_conditions.periodic import (SquarePeriodicBoundary,
                                               LinearPeriodicBoundary)
from bvpy.solvers.linear import LinearSolver
from bvpy.solvers.nonlinear import NonLinearSolver


class BVP(object):
    """Main class of the library, encodes a Boundary-Value Problem.

    Parameters
    ----------
    domain : subclass of :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`
        Integration domain, described by both a geometry and a mesh
        (the default is None).
    vform : subclass of :class:`AbstractVform<bvpy.vform.abstract.AbstractVform>`
        Variational form to solve. Encompass the type(s) of Finite Elements
        to consider (the default is None).
    bc : :class:`DOLFIN DirichletBC<Dolfin.cpp.fem.DirichletBC>`
        Boundary conditions in a Dolfin/FEniCS-readable format.

    Attributes
    ----------
    functionSpace :
        Function space to consider for the Finite Element Method at stake.
    testFunction : :class:`Function<dolfin.cpp.function.Function>`
        Test function corresponding to the considered function space.
    trialFunction : :class:`Function<dolfin.cpp.function.Function>`
        Trial function corresponding to the considered function space.
    solution : :class:`Function<dolfin.cpp.function.Function>`
        Solution of the problem.
    dirichletCondition :
        Dirichlet boundary conditions to apply.
    _dirichlet_class : list
        Container for the dirichlet.
    neumannCondition : dict
        Neumann condition to apply
    pointSource : :class:`PointSource<dolfin.cpp.fem.PointSource>`
        A specific type of boundary condition where the constrain is only
        applied to a single point.
    solver :
        Solver to use to estimate the seeked solution.
    bnd_conds : list of ???
        List of all the boundary conditions considered within the problem.

    """

    def __init__(self, domain: AbstractDomain = None,
                 vform: AbstractVform = None, bc=None):

        self.functionSpace: Union[fe.FunctionSpace, fe.VectorFunctionSpace,
                                  fe.TensorFunctionSpace] = None
        self.testFunction: fe.TestFunction = None
        self.trialFunction: fe.TrialFunction = None
        self.solution: fe.Function = None

        self._pointsources = None
        self.dirichletCondition: list = []
        self._dirichlet_class: list = []
        self.neumannCondition: dict = {'constant': [],
                                       'variable': []}
        self._neumann_domain_id: list = {'constant': [],
                                        'variable': []}

        self.solver: Union[LinearSolver, NonLinearSolver] = None

        if domain is not None:
            self.set_domain(domain)
        else:
            self.domain: AbstractDomain = None

        if domain is not None and vform is not None:
            self.set_vform(vform)
        else:
            self.vform: AbstractVform = None

        if vform is not None and bc is not None:
            self.add_boundary_condition(bc)
        else:
            self.bnd_conds: list = []

    def __repr__(self):
        """Yields a description of the considered BVP.

        Returns
        -------
        str
            A printable expression of the BVP.

        """
        return '<'+str(self.problem_name)+', at '+str(hex(id(self)))+'>'

    def __str__(self):
        """Provides a detailed description of the BVP.

        Returns
        -------
        str
            A detailed description of the BVP characteristics.

        """
        return self._details

    @property
    def problem_name(self):
        """Name of the problem at stake.

        Returns
        -------
        str
            The name of the problem.

        """
        descr = self.__class__.__name__ + ' of '
        descr += self.vform.__class__.__name__ + ' on '
        descr += self.domain.__class__.__name__
        return descr

    @property
    def _details(self):
        """Sums up the characteristics of the problem.

        Returns
        -------
        str
            A printable description of the problem.

        """

        description = self.domain._details + '\n'
        description += self.vform._details + '\n'
        description += 'Boundary conditions \n-------------------\n'

        for diri in self._dirichlet_class:
            description += f"    * {diri.__repr__()[1:-1].split(',')[0]}"
            description += '\n'
            for info in diri.__repr__()[1:-1].split(',')[1:]:
                description += f'\t- {info}' + '\n'

        for n_type, neum in self.neumannCondition.items():
            description += f"    * {n_type}: {neum.__repr__()[1:-1].split(',')[0]}"
            description += '\n'
            for info in neum.__repr__()[1:-1].split(',')[1:]:
                description += f'\t- {info}' + '\n'

        description += '\nSystem to solve \n---------------\n'
        description += f"    * Number of DOFs: {self.functionSpace.dim()}"

        return description

    def set_domain(self, domain: AbstractDomain):
        """Sets the domain to consider.

        Parameters
        ----------
        domain : :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`
                 or subclass.
            Integration domain to consider.

        Returns
        -------
        None

        """
        assert issubclass(type(domain), AbstractDomain)
        self.domain = domain
        if self.domain.mesh is None:
            self.domain.discretize()

    def set_vform(self, vform: AbstractVform, constrain=None):
        """Instanciates the function space and the trial and test functions.

        Parameters
        ----------
        vform : :class:`AbstractVform<bvpy.vform.abstract.AbstractVform>`
                or subclass
            Variational form to solve.
        constrain : ???
            Description of parameter `constrain` (the default is None).

        Returns
        -------
        None

        """
        assert issubclass(type(vform), AbstractVform)
        self.vform = vform

        if vform.elements:
            elems = self._finite_element_constructor(self.vform.elements)
        else:
            logger.error('no elements found in vform:{form}'.format(form=vform)
                         )

        if constrain is not None:
            self.functionSpace = fe.FunctionSpace(self.domain.mesh,
                                                  reduce(operator.mul, elems),
                                                  constrained_domain=constrain)
        else:
            self.functionSpace = fe.FunctionSpace(self.domain.mesh,
                                                  reduce(operator.mul, elems))

        self.trialFunction = fe.TrialFunction(self.functionSpace)
        self.testFunction = fe.TestFunction(self.functionSpace)
        name = self.vform.__class__.__name__.replace('Form', '')
        self.solution = fe.Function(self.functionSpace, name=name)

        self.vform._set_measures(self.domain.dx, self.domain.ds)

        self._pointsources = [fe.PointSource(self.functionSpace,
                                             d['point'], d['value'])
                              for d in self.vform._pointsources]

    def _finite_element_constructor(self, elems):
        finite_elems = []
        ct = self.domain.mesh.ufl_cell()

        for e in elems:
            assert isinstance(e, Element)
            if e.dim == 'scalar':
                finite_elems.append(fe.FiniteElement(e.family, ct, e.degree))
            elif e.dim == 'vector':
                finite_elems.append(fe.VectorElement(e.family, ct, e.degree))
            elif e.dim == 'tensor':
                finite_elems.append(fe.TensorElement(e.family, ct, e.degree))

        return finite_elems

    def _apply_dirichlet(self, dir_class):
        """Add a Dirichlet boundary condition to the problem.

        Parameters
        ----------
        dir_class : :class:`ConstantDirichlet<femtk.boundary_conditions.
                                              dirichlet.ConstantDirichlet>`
                    or
                    :class:`VariableDirichlet<femtk.boundary_conditions.
                                              dirichlet.VariableDirichlet>`
            Class representing a Dirichlet boundary condition.

        Returns
        -------
        None

        """

        self.dirichletCondition.append(dir_class.apply(self.functionSpace))

    def _apply_neumann(self, dir_class):
        """Add a Neumann boundary condition to the problem.

        Parameters
        ----------
        dir_class : :class:`ConstantDirichlet<femtk.boundary_conditions.
                                              dirichlet.ConstantDirichlet>`
                    or
                    :class:`VariableDirichlet<femtk.boundary_conditions.
                                              dirichlet.VariableDirichlet>`
            Class representing a Dirichlet boundary condition.

        Returns
        -------
        None

        """

        if isinstance(dir_class, (ConstantNeumann)):
            self.neumannCondition['constant'].append(dir_class.apply(self.domain.bdata))
            self._neumann_domain_id['constant'].append(dir_class._id)
        elif isinstance(dir_class, (VariableNeumann, NormalNeumann)):
            self.neumannCondition['variable'].append(dir_class.apply(self.functionSpace,
                                        self.domain.bdata))
            self._neumann_domain_id['variable'].append(dir_class._id)

    def add_boundary_condition(self, bnd_cond,
                               method='topological',
                               subspace=None,
                               degree=None):
        """Add a boundary condition (Dirichlet) to the problem.

        Parameters
        ----------
        bnd_cond : :class:`ConstantDirichlet <femtk.boundary_conditions.dirichlet.ConstantDirichlet>`, :class:`VariableDirichlet <femtk.boundary_conditions.dirichlet.VariableDirichlet>`
            Class representing a boundary condition.
        method : str, optional
            Method to apply Dirichlet condition (default is 'topological').
        subspace : int or None, optional
            Index of the subspace to constrain for Dirichlet (default is None).
        degree : int or None, optional
            Determines the degree for :class:`VariableDirichlet <femtk.boundary_conditions.dirichlet.VariableDirichlet>` (default is None).

        Returns
        -------
        None
        """
        if not isinstance(bnd_cond, (list, tuple)):
            bnd_cond = [bnd_cond]

        for cond in bnd_cond:
            assert isinstance(cond, (ConstantDirichlet,
                                     VariableDirichlet,
                                     ZeroDirichlet,
                                     NormalDirichlet,
                                     fe.DirichletBC,
                                     SquarePeriodicBoundary,
                                     LinearPeriodicBoundary,
                                     ConstantNeumann,
                                     VariableNeumann,
                                     NormalNeumann))

            if isinstance(cond, (ConstantDirichlet, VariableDirichlet,
                                 ZeroDirichlet, NormalDirichlet)):
                self._apply_dirichlet(cond)
                self._dirichlet_class.append(cond)
            elif isinstance(cond, fe.DirichletBC):
                self.dirichletCondition.append(cond)
            elif isinstance(cond, (SquarePeriodicBoundary,
                                   LinearPeriodicBoundary)):
                self.set_vform(self.vform, constrain=cond)
            elif isinstance(cond, (ConstantNeumann, VariableNeumann,
                                  NormalNeumann)):
               self._apply_neumann(cond)

    # TODO: migrate this in solver ?
    def _set_solver_parameter(self, **kwargs):
        """Sets parameter for the solver.

        Parameters
        ----------
        None

        Other parameters
        ----------------
        **kwargs : type
            Description of parameter `**kwargs`.

        Returns
        -------
        None
        """
        parameters = self.solver.parameters._param
        krylov_parameter = self.solver.parameters._param['krylov_solver']
        for key in kwargs.keys():
            if (key in krylov_parameter) and (key in parameters) and isinstance(self.solver, LinearSolver):
                krylov_parameter[key] = kwargs[key]
            elif key == 'krylov_solver':
                for subkey in kwargs[key].keys():
                    if subkey in krylov_parameter:
                        krylov_parameter[subkey] = kwargs[key][subkey]
                    else:
                        logger.warn(str(subkey) + ' is not in the list of parameter for the krylov solver')
            elif key in parameters:
                parameters[key] = kwargs[key]
            elif key in krylov_parameter:
                krylov_parameter[key] = kwargs[key]
            else:
                logger.warn(str(key) + ' is not in the list of parameter')

    def solve(self, return_solver_info=False, **kwargs):
        lhs, rhs = self.vform.get_form(self.trialFunction,
                                       self.testFunction,
                                       self.solution)
        if isinstance(self.vform.rhs, Zero):

            for neumann, domain_id in zip(self.neumannCondition['constant'],
                                          self._neumann_domain_id['constant']):
                lhs -= fe.dot(neumann,
                              self.testFunction) * self.domain.ds(domain_id)
            for neumann, domain_id in zip(self.neumannCondition['variable'],
                                          self._neumann_domain_id['variable']):
                lhs -= fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)

            self.solver = NonLinearSolver()
            self._set_solver_parameter(**kwargs)
            jacobian = fe.derivative(self.vform.lhs,
                                     self.solution,
                                     self.trialFunction)
            self.solver.set_problem(self.solution, lhs,
                                    jacobian, self.dirichletCondition,
                                    self._pointsources)
        else:

            for neumann, domain_id in zip(self.neumannCondition['constant'],
                                          self._neumann_domain_id['constant']):
                rhs += fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)
            for neumann, domain_id in zip(self.neumannCondition['variable'],
                                          self._neumann_domain_id['variable']):
                rhs += fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)

            self.solver = LinearSolver()
            self._set_solver_parameter(**kwargs)
            self.solver.set_problem(self.solution, lhs,
                                    rhs, self.dirichletCondition,
                                    self._pointsources)

        if return_solver_info:
            residuals = self.solver.solve()
            return residuals
        else:
            self.solver.solve()

    def info(self):
        """Prints the descrition of the problem.

        Returns
        -------
        None

        """
        print(self._details)
