#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.vforms.elasticity
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
from abc import ABC, abstractmethod

import inspect
from bvpy import logger as log
import numpy as np
import fenics as fe

from bvpy.vforms.abstract import AbstractVform, Element
from bvpy.utils.tensor import applyElementwise

class AbstractElasticForm(AbstractVform):
    """Base class for elastic vform.

    Parameters
    ----------
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The body forces.
    plane_stress : bool
        Assumes plane stress assumption (if true, default) or plane strain assumption (if false)
    thickness : float
        Thickness of the material if considering 2D case with plane stress/strain. Default is 1.

    Attributes
    ----------
    _elements : :class:`Element<bvpy.vforms.abstract.Element>`
        Description of the Finite Element.
    """

    def __init__(self, source=[0, 0, 0], plane_stress=True, thickness=1):
        super().__init__()

        self._elements = [Element('P', 1, 'vector')]
        self.plane_stress = plane_stress

        self.set_parameters(source=source, thickness=thickness)

    def _get_lame_coefficients(self, young, poisson):
        """ Convert Young modulus and Poisson coefficient into Lamé coefficients
        """
        mu = .5 * young / (1 + poisson)

        if self._plane_stress:
            lmbda = young * poisson / (1 - poisson ** 2)
        else:
            lmbda = young * poisson / ((1 + poisson) * (1 - 2 * poisson))

        return mu, lmbda

    def grad_deformation(self, u, ref='lagrangian'):
        """
        Assuming a displacement field from the initial state to the reference state (deformed) compute its
        deformation gradient tensor according to the description used.

        Description can be either {'lagrangian', 'eulerian'}.

        Parameters
        ----------
        u : `Function<dolfin.function.function.Function>`
            The displacement field from initial state to reference state (deformed).
        ref : str, optional
            The description used, either lagrangian (default) or eulerian.

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            A tensor function representing the deformation gradient tensor
        """
        #assert isinstance(u, fe.Function), 'Input displacement field should be a <dolfin.function.function.Function> object'
        assert ref in {'lagrangian', 'eulerian'}, '`ref` can be either ''lagrangian'' or ''eulerian'''

        dim = u.geometric_dimension()
        F = fe.Identity(dim) + fe.grad(u)

        if ref == 'lagrangian':
            return F
        else:
            return fe.inv(F) # eulerian

    def green_cauchy_strain(self, F, side='right', ref='lagrangian'):
        """
        Compute the Green-Cauchy deformation tensor field from a deformation field ``F``.

        Depending on the ``side`` and ``ref``, several types of tensors can be obtained:

        +-------+--------------+---------------------------------------------------------------+---------------------------+
        | side  | description  | usual name of the deformation tensor                          | usual notation            |
        +=======+==============+===============================================================+===========================+
        | right | lagrangian   | right Cauchy–Green deformation tensor or Cauchy strain tensor | :math:`C = F^T F`         |
        +-------+--------------+---------------------------------------------------------------+---------------------------+
        | right | eulerian     | Finger strain tensor                                          | :math:`f = F^{-1} F^{-T}` |
        +-------+--------------+---------------------------------------------------------------+---------------------------+
        | left  | lagrangian   | left Cauchy–Green deformation tensor or Green strain tensor   | :math:`B = F F^T`         |
        +-------+--------------+---------------------------------------------------------------+---------------------------+
        | left  | eulerian     | Piola strain tensor or Finger tensor                          | :math:`c = F^{-T} F^{-1}` |
        +-------+--------------+---------------------------------------------------------------+---------------------------+

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field expressed in `description` referential
        side : str, optional
            The type of Green-Cauchy deformation tensor to compute. Either 'right' (default) or 'left'.
        ref : str, optional
            The description referential used, either lagrangian (default) or eulerian.

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            The Green-Cauchy deformation tensor field
        """
        assert side in {'left', 'right'}, '`side` can be either ''left'' or ''right'''
        assert ref in {'lagrangian', 'eulerian'}, '`ref` can be either ''lagrangian'' or ''eulerian'''

        if ref == 'eulerian':
            F = fe.inv(F)

        if side == 'right':
            return F.T * F
        elif side == 'left':
            return F * F.T

    @abstractmethod
    def strain(self, *args, **kwargs):
        """Computes the strain field.
        """
        pass


    def get_strain(self, u, **kwargs):
        """Project the strain field as a fenics.Function.

        Parameters
        ----------
        u : `Function<dolfin.function.function.Function>`
            The displacement field

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            A tensor function representing the strain field.

        """
        tnsr_func_space = fe.TensorFunctionSpace(u.function_space().mesh(), 'DG', 0)

        if self._requires_F:
            F = self.grad_deformation(u)
            strain_field = self.strain(F, **kwargs)
        else:
            strain_field = self.strain(u, **kwargs)

        return fe.project(strain_field, tnsr_func_space)

    @abstractmethod
    def stress(self, *args, **kwargs):
        """Computes the stress field.
        """
        pass

    def get_stress(self, u, **kwargs):
        """Project the stress field as a fenics.Function.

        Parameters
        ----------
        u : `Function<dolfin.function.function.Function>`
            The displacement field

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            A tensor function representing the stress field.

        """
        tnsr_func_space = fe.TensorFunctionSpace(u.function_space().mesh(), 'DG', 0)

        if self._requires_F:
            F = self.grad_deformation(u)
            stress_field = self.stress(F, **kwargs)
        else:
            stress_field = self.stress(u, **kwargs)

        return fe.project(stress_field, tnsr_func_space)

    @abstractmethod
    def construct_form(self, u, v, sol):
        """Construct the vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        pass

    @abstractmethod
    def set_expression(self):
        pass

    @property
    def plane_stress(self):
        """
        Get the plane stress state of the form.

        In the context of elasticity, plane stress refers to a two-dimensional state where one of the principal stresses is zero.
        This method retrieves whether the form is in a plane stress state.

        Returns
        -------
        bool
            True if plane stress is applied, otherwise False.
        """
        return self._plane_stress

    @plane_stress.setter
    def plane_stress(self, value):
        """
        Set the plane stress state of the form.

        This setter allows defining whether the elastic form should be treated with plane stress conditions.

        Parameters
        ----------
        value : bool
            True to apply plane stress, otherwise False.

        Raises
        ------
        ValueError: If the provided value is not a boolean.
        """
        if not isinstance(value, bool):
            raise ValueError(f"plane_stress should be a boolean, not {type(value)}`")
        self._plane_stress = value

    @property
    @abstractmethod
    def _requires_F(self):
        """Indicate whether the stress/strain method needs the deformation gradient F or not."""
        pass

class LinearElasticForm(AbstractElasticForm):
    r"""
    Variational Formulation for Linear Elastic Materials

    This class defines the finite element variational formulation for linear elastic materials,
    constructing a bilinear system :math:`a(u, v) = L(v)` based on the principle of linear elasticity.
    The formulation assumes a linear relationship between stress and strain as defined by Hooke's law.

    Governing Equations:
    The linear elastic problem is derived from the total potential energy. The equilibrium
    of internal and external forces is expressed in its weak form:

    .. math::

        a(u, v) = L(v),

    where:

    - :math:`a(u, v)` is the bilinear form representing the internal elastic forces.
    - :math:`L(v)` is the linear form representing the external forces.

    The bilinear form :math:`a(u, v)` is defined as:

    .. math::

        a(u, v) = \int_\Omega \sigma(u) : \varepsilon(v) \, dx,

    where:

    - :math:`\sigma(u)` is the Cauchy stress tensor
    - :math:`\varepsilon(u)` is the strain tensor, defined as :math:`\varepsilon = \frac{1}{2}(\nabla u + \nabla u^T)`.

    The linear form :math:`L(v)` represents the external body forces:

    .. math::

        L(v) = \int_\Omega \mathbf{b} \cdot v \, dx,

    where:

    - :math:`\mathbf{b}` is the body force per unit volume.

    Parameters
    ----------
    young: float
        Young modulus, if defined poisson coefficient must be defined
    poisson : float
        Poisson coefficient, if defined young modulus must be defined
    mu: float
        1st Lamé coefficient
    lmbda : float
        2st Lamé coefficient

    Other Parameters
    ----------------
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The body forces.
    plane_stress : bool
        Assumes plane stress assumption (if true, default) or plane strain assumption (if false)
    thickness : float
        Thickness of the material if considering 2D case with plane stress/strain. Default is 1.

    Attributes
    ----------
    _elements : :class:`Element<bvpy.vforms.abstract.Element>`
        Description of the Finite Element.
    """
    def __init__(self, young=1, poisson=0.3, mu=None, lmbda=None, **kwargs):

        super().__init__(**kwargs)

        assert (young is not None and poisson is not None) or (mu is not None and lmbda is not None), \
            'either young/poisson or mu/lmbda must be defined'

        if (young is not None and poisson is not None) and (mu is None):
            mu, lmbda = self._get_lame_coefficients(young, poisson)

        self.set_parameters(mu=mu, lmbda=lmbda, young=young, poisson=poisson)

    def strain(self, u):
        """
        Computes the strain field from a displacement field F.

        Parameters
        ----------
        u : `Function<dolfin.function.function.Function>`
            The displacement field

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed strain.
        """

        return 0.5 * (fe.grad(u) + fe.grad(u).T)

    def stress(self, u):
        """
        Computes the stress field from a displacement field F.

        Parameters
        ----------
        u : `Function<dolfin.function.function.Function>`
            The displacement field

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed stress.
        """
        d = u.geometric_dimension()

        lmbda = self.get_parameter('lmbda')
        mu = self.get_parameter('mu')

        return lmbda*fe.div(u) * fe.Identity(d) + 2*mu*self.strain(u)

    def construct_form(self, u, v, sol):
        """
        Construct the vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        thickness = self.get_parameter('thickness')
        source = self.get_parameter('source')

        self.lhs = thickness * fe.inner(self.stress(u), self.strain(v)) * self.dx
        self.rhs = fe.dot(source, v) * self.dx(domain=v.function_space().mesh())

    def set_expression(self):
        self._expression = "thickness*stress(u)*strain(v)*dx "
        self._expression += "= source*v*dx"

    @property
    def _requires_F(self):
        """Indicate that the stress/strain method do not need the deformation field."""
        return False  # Use the displacement field in strain/stress

class HyperElasticForm(AbstractElasticForm):
    r"""
    HyperElasticForm: Variational Formulation for Hyperelastic Materials

    This class defines the finite element variational formulation for hyperelastic materials
    based on the principle of energy minimization. It constructs a nonlinear system
    :math:`F(u, v) = 0` representing the equilibrium of internal and external forces.

    The variational form is derived from the total potential energy:

    .. math::

        \Pi = \int_\Omega W(F) \, dx - \int_\Omega \mathbf{b} \cdot \mathbf{u} \, dx,

    where:

    - :math:`W(F)` is the strain energy density function (called potential_energy below)
    - :math:`F` is the deformation gradient.
    - :math:`\mathbf{b}` is the body force.
    - :math:`\mathbf{u}` is the displacement field.

    with optional scaling by material thickness for 2D problems.

    Parameters
    ----------
    potential_energy : subclass of AbstractHyperElasticPotential
        The strain energy density function that relates the strain energy density of the material to the deformation
        field F.
    small_displacement : bool
        Consider or not (default) small displacement (infinitesimal strain theory)

    Other Parameters
    ----------------
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The body forces.
    plane_stress : bool
        Assumes plane stress assumption (if true, default) or plane strain assumption (if false)
    thickness : float
        Thickness of the material if considering 2D case with plane stress/strain. Default is 1.

    Attributes
    ----------
    _elements : :class:`Element<bvpy.vforms.abstract.Element>`
        Description of the Finite Element.
    """
    def __init__(self, potential_energy=None, small_displacement=False, **kwargs):
        super().__init__(**kwargs)

        if potential_energy is not None:
            self.potential_energy = potential_energy

        self.small_displacement = small_displacement

    def stress(self, F, stress_type='PK1'):
        """
        Computes the stress field from a deformation field F. The available stress tensor are:

           - 1st Piola–Kirchhoff stress tensor (P)
           - 2nd Piola–Kirchhoff stress tensor (S)
           - Cauchy stress tensor (sigma)

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field expressed in `description` referential

        stress_type : str, optional
            Name of the stress measure we want to compute.
            The default is 'PK2' (2nd Piola–Kirchhoff stress tensor).
            Available measures: 'PK1', 'PK2', 'cauchy'.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed stress.
        """
        assert stress_type in {'PK1', 'PK2', 'cauchy'}, 'the `stress_type` can be ''PK1'', ''PK2'' or ''cauchy'''

        J = fe.det(F)
        F = fe.variable(F) # needed to derive function according to tensor
        W = self.compute_potential_energy(F)

        if stress_type == 'PK1':
            return fe.diff(W, F)
        elif stress_type == 'PK2':
            return fe.inv(F) * fe.diff(W, F)
        elif stress_type == 'cauchy':
            return (1/ J) * fe.diff(W, F) * F.T

    def strain(self, F, strain_type='GL'):
        r"""
        Computes various strain measures.

       If ``small_displacement`` is ``True``, the strain is always computed as:

        .. math::

            0.5 \cdot (\nabla u + \nabla u^T)

        which represents the symmetric part of the displacement gradient :math:`u`.

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field expressed in `description` referential
        strain_type : str, optional
            Name of the strain measure we want to compute
            The default is 'GL' (Green-Lagrange)
            Available measures: 'GL', 'biot', 'natural' and 'almansi'.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed strain.

        """
        Id = fe.Identity(F.ufl_shape[0])
        grad_u = F - Id

        if self.small_displacement:
            return 0.5 * (grad_u + grad_u.T)

        C = self.green_cauchy_strain(F, side='right') # right cauchy-green deformation tenso

        if strain_type == 'GL':
            return 0.5 * (C - Id)

        elif strain_type == 'biot':
            return applyElementwise(lambda x: x ** 0.5, C) - Id

        elif strain_type == 'natural':
            return 0.5 * applyElementwise(lambda x: fe.ln(x), C)

        elif strain_type == 'almansi':
            return 0.5 * (Id - fe.inv(C))

        return 0.5 * (grad_u + grad_u.T + grad_u.T * grad_u)

    def compute_potential_energy(self, *args, **kwargs):
        """
        Calls the potential energy method of the set elastic potential class instance.
        """
        if self.potential_energy is None:
            raise NotImplementedError("No elastic potential has been set.")
        return self.potential_energy.potential_energy(*args, **kwargs)

    def construct_form(self, u, v, sol):
        """
        Construct the vform. The problem will always be recast as a non-linear problem F(u,v)=0.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        F = self.grad_deformation(sol)
        W = self.compute_potential_energy(F)

        thickness = self.get_parameter('thickness')
        source = self.get_parameter('source')

        # Balance energy : strain energy + body forces energy
        self.lhs = thickness * W * self.dx
        self.lhs -= fe.dot(source, sol) * self.dx
        self.lhs = fe.derivative(self.lhs, sol, v)

    def set_expression(self):
        self._expression = "psi(u)dx - Budx - Tudx "

    def _initialize_potential_energy(self, W):
        """
        Validates and updates the hyperelastic energy potential.
        """
        if not isinstance(W, AbstractHyperElasticPotential):
            raise TypeError("Model must be a subclass of AbstractHyperElasticPotential")

        validate_result = W.validate_potential_energy_method()
        if not validate_result:
            raise ValueError("Model validation failed: potential_energy method is invalid")

        self.set_parameters(**W.model_parameters)

    @property
    def potential_energy(self):
        """
        Get the elastic potential associated with this form.

        The elastic potential defines the material behavior, specifically how it stores and returns energy.
        This getter provides access to the current model being used.

        Returns
        -------
        AbstractHyperElasticPotential
            The elastic potential instance.
        """
        return self._potential_energy

    @potential_energy.setter
    def potential_energy(self, W):
        """
        Set the elastic potential for this form.

        This setter allows the specification of the material model that defines the form's hyperelastic behavior.

        Parameters
        ----------
        W : AbstractHyperElasticPotential
            The elastic potential instance to set.

        Raises
        ------
        ValueError: If the provided model is not an instance of AbstractHyperElasticPotential.
        """
        if W is not None:
            self._initialize_potential_energy(W)
        self._potential_energy = W

    @property
    def small_displacement(self):
        """
        Get the state of small displacement assumption (linear or non-linear elasticity)

        Returns
        -------
        bool
            True if small displacement is assumed, otherwise False.
        """
        return self._small_displacement

    @small_displacement.setter
    def small_displacement(self, value):
        """
        Set the small displacement assumption (linear or non-linear elasticity) for the variational form.

        Parameters
        ----------
        value : bool
            True to apply the small displacement assumption, otherwise False.

        Raises
        ------
        ValueError: If the provided value is not a boolean.
        """
        if not isinstance(value, bool):
            raise ValueError(f"small_displacement should be a boolean, not {type(value)}`")
        self._small_displacement = value

    @property
    def _requires_F(self):
        return True  # Use the deformation field in strain/stress

class AbstractHyperElasticPotential(ABC):
    """
    Abstract base class for hyperelastic material potential.

    This class provides a blueprint for creating new hyperelastic material potential. Specifically, every
    hyperelastic material model must provide a method to calculate the potential energy
    of the material given the deformation gradients.

    Methods
    -------
    potential_energy(*args, **kwargs)
        Compute the elastic potential energy for the given deformation gradients.

    validate_potential_energy_method()
        Validate that the potential_energy method is correctly defined.
    """
    def __init__(self, *args, **kwargs):
        """
        Initialize the AbstractHyperElasticModel with composition.

        Parameters
        ----------
        args : tuple
            Positional arguments to be passed to HyperElasticForm.
        kwargs : dict
            Keyword arguments to be passed to HyperElasticForm.
        """
        self._parameters = {}
        self._hyperelastic_form = HyperElasticForm(*args, **kwargs)

    @abstractmethod
    def potential_energy(self, F):
        """
        Calculate the potential energy for the given deformation gradients.

        This method must be implemented by subclasses to define the specific form
        of the potential energy for different hyperelastic material.

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field (lagrangian description)

        Returns
        -------
            The potential scalar energy of the material for the given deformation gradients.
        """
        pass

    @property
    def model_parameters(self, params=None):
        """
        Dictionary of model parameters.
        """
        return self._parameters

    @model_parameters.setter
    def model_parameters(self, params):
        """
        Set model parameters and ensure they are available for potential_energy.

        Parameters
        ----------
        params : dict
            Dictionary of parameters to set.
        """
        self._hyperelastic_form.set_parameters(**params)
        self._parameters.update({k:self._hyperelastic_form.get_parameter(k) for k in params.keys()})

    def get_model_parameters(self, param_name=None):
        """
        Get a specific model parameter.

        Parameters
        ----------
        param_name : str
            Name of the parameter to retrieve.

        Returns
        -------
        The value of the requested parameter.

        Raises
        ------
        ValueError
            If the parameter is not found.
        """
        if param_name in self._parameters:
            return self._parameters[param_name]
        else:
            raise ValueError(f"The parameter '{param_name}' is not available.")

    def validate_potential_energy_method(self):
        """
        Validate that the potential_energy method is correctly defined.

        Returns
        -------
        True if correctly defined.

        Raises
        ------
        ValueError
            If the potential_energy method does not meet the required criteria.
        """
        method = getattr(self, 'potential_energy', None)

        # Ensure the method is callable
        if not callable(method):
            raise ValueError(f"The 'potential_energy' method is not callable in {self.__class__.__name__}.")

        # Test the method
        mesh = fe.UnitSquareMesh(8, 8)
        V = fe.TensorFunctionSpace(mesh, "DG", 0)
        dummy_F = fe.Function(V)
        self.potential_energy(dummy_F)

        # All checks passed
        log.debug(f"The 'potential_energy' method in {self.__class__.__name__} is correctly defined.")

        return True

    def _get_lame_coefficients(self, *args, **kwargs):
        """
        Delegates to `HyperElasticForm._get_lame_coefficients`.
        """
        return self._hyperelastic_form._get_lame_coefficients(*args, **kwargs)

    def grad_deformation(self, *args, **kwargs):
        """
        Delegates to `HyperElasticForm.grad_deformation`.
        """
        return self._hyperelastic_form.grad_deformation(*args, **kwargs)

    def green_cauchy_strain(self, *args, **kwargs):
        """
         Delegates to `HyperElasticForm.green_cauchy_strain`.
         """
        return self._hyperelastic_form.green_cauchy_strain(*args, **kwargs)

    def stress(self, *args, **kwargs):
        """
         Delegates to `HyperElasticForm.stress`.
         """
        return self._hyperelastic_form.stress(*args, **kwargs)

    def strain(self, *args, **kwargs):
        """
         Delegates to `HyperElasticForm.strain`.
         """
        return self._hyperelastic_form.strain(*args, **kwargs)

class NeoHookeanPotential(AbstractHyperElasticPotential):
    r"""
    Generic base for the Neo-Hookean potential.

    The potential energy is written as:

    .. math::

        W(F) = \frac{\mu}{2} \left( \text{tr}(C) - \text{dim} \right) + \text{incompressible}_\text{penalty}

    where:

    - :math:`C` is the Cauchy-Green deformation tensor (:math:`C = F^T F`, with :math:`F` being the deformation gradient),
    - :math:`\text{dim}` is the spatial dimension (:math:`2` or :math:`3`),
    - :math:`\mu` is the first Lamé coefficient.

    Different incompressibility penalties can be applied. The implemented version is:

    .. math::

        \text{incompressible}_\text{penalty} = \mu \cdot \ln(J) + 0.5 \cdot \lambda \cdot (J - 1)^2

    where:

    - :math:`J = \det(F)` is the determinant of the deformation gradient,
    - :math:`\lambda` is the second Lamé coefficient.

    For large :math:`\lambda`, the material behaves as nearly-incompressible.

    Parameters
    ----------
    young: float
        Young modulus, if defined poisson coefficient must be defined
    poisson : float
        Poisson coefficient, if defined young modulus must be defined
    mu: float
        1st Lamé coefficient
    lmbda : float
        2st Lamé coefficient, if defined mu must be defined
    """
    def __init__(self, young=1, poisson=0.3, mu=None, lmbda=None):

        super().__init__()

        assert (young is not None and poisson is not None) or (mu is not None), \
            'either young/poisson or mu must be defined'

        if (young is not None and poisson is not None) and (mu is None):
            mu, lmbda = self._get_lame_coefficients(young, poisson)

        self.model_parameters = {'mu': mu, 'lmbda': lmbda, 'young': young, 'poisson': poisson}

    def potential_energy(self, F):
        """ The strain energy function for the neo-hookean model

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field (lagrangian description)

        Returns
        -------
        """
        mu = self.get_model_parameters('mu')
        lmbda = self.get_model_parameters('lmbda')

        dim = F.ufl_shape[0]
        C = self.green_cauchy_strain(F, side='right')
        Ic = fe.tr(C) # trace of the right Cauchy-Green deformation tensor

        W =  0.5 * mu * (Ic - dim) # neo-hookean model without incompressible constraint

        if lmbda is not None:
            # add incompressible constraint
            J = fe.det(F)
            W += - mu * fe.ln(J) + 0.5 * lmbda * (J - 1) ** 2

        return W

class StVenantKirchoffPotential(AbstractHyperElasticPotential):
    r"""
    Generic base for the St. Venant-Kirchhoff hyperelastic potential.

    The St. Venant-Kirchhoff model assumes a quadratic strain energy density function, expressed as:

    .. math::

        W(E) = \mu \cdot \text{tr}(E \cdot E) + 0.5 \cdot \lambda \cdot (\text{tr}(E))^2

    where:

    - :math:`E` is the Green-Lagrange strain tensor, defined as :math:`E = 0.5 \cdot (C - I)`,
      with :math:`C` being the Cauchy-Green deformation tensor and :math:`I` the identity tensor,
    - :math:`\lambda` is the second Lamé coefficient,
    - :math:`\mu` is the first Lamé coefficient.

    This model is linear in terms of the strain energy but nonlinear with respect to the deformation.

    Parameters
    ----------
    young: float
        Young modulus
    poisson : float
        Poisson coefficient
    mu: float
        1st Lamé coefficient, if defined lmbda must be defined
    lmbda : float
        2st Lamé coefficient, if defined mu must be defined
    """
    def __init__(self, young=1, poisson=0.3, mu=None, lmbda=None):

        super().__init__()

        assert (young is not None and poisson is not None) or (mu is not None and lmbda is not None), \
            'either young/poisson or mu/lmbda must be defined'

        if (young is not None and poisson is not None) and (mu is None):
            mu, lmbda = self._get_lame_coefficients(young, poisson)

        self.model_parameters = {'mu': mu, 'lmbda': lmbda, 'young': young, 'poisson': poisson}

    def potential_energy(self, F):
        """
        The strain energy function for the st-venant kirchoff potential

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field (lagrangian description)

        Returns
        -------
        """
        mu = self.get_model_parameters('mu')
        lmbda = self.get_model_parameters('lmbda')

        E = self.strain(F, strain_type='GL')

        return mu * fe.tr(E * E) + 0.5 * lmbda * fe.tr(E) * fe.tr(E)


class QuadraticGreenLagrangePotential(AbstractHyperElasticPotential):
    r"""
    Quadratic Green-Lagrange potential.

    This class defines an energy density function that is quadratic with respect to the Green-Lagrange strain tensor.
    It introduces the fourth-order elasticity tensor :math:`C`, which describes all possible linear elastic responses.

    The energy density function is defined as:

    .. math::

        W(E) = 0.5 (E : C : E)

    where:

    - :math:`E` is the Green-Lagrange strain tensor,
    - :math:`C` is the fourth-order elasticity tensor.

    **Elasticity Tensor in Voigt Notation**

    In 3D, the elasticity tensor :math:`C` is expressed in Voigt notation as:

    .. math::

        C = \begin{bmatrix}
        c_{11} & c_{12} & c_{13} & c_{14} & c_{15} & c_{16} \\
        c_{12} & c_{22} & c_{23} & c_{24} & c_{25} & c_{26} \\
        c_{13} & c_{23} & c_{33} & c_{34} & c_{35} & c_{36} \\
        c_{14} & c_{24} & c_{34} & c_{44} & c_{45} & c_{46} \\
        c_{15} & c_{25} & c_{35} & c_{45} & c_{55} & c_{56} \\
        c_{16} & c_{26} & c_{36} & c_{46} & c_{56} & c_{66}
        \end{bmatrix}

    The relationship between stress and strain is described by Hooke's law:

    .. math::

        \begin{bmatrix}
        \sigma_{xx} \\
        \sigma_{yy} \\
        \sigma_{zz} \\
        \sigma_{xy} \\
        \sigma_{xz} \\
        \sigma_{yz}
        \end{bmatrix}
        =
        \begin{bmatrix}
        c_{11} & c_{12} & c_{13} & c_{14} & c_{15} & c_{16} \\
        c_{12} & c_{22} & c_{23} & c_{24} & c_{25} & c_{26} \\
        c_{13} & c_{23} & c_{33} & c_{34} & c_{35} & c_{36} \\
        c_{14} & c_{24} & c_{34} & c_{44} & c_{45} & c_{46} \\
        c_{15} & c_{25} & c_{35} & c_{45} & c_{55} & c_{56} \\
        c_{16} & c_{26} & c_{36} & c_{46} & c_{56} & c_{66}
        \end{bmatrix}
        \cdot
        \begin{bmatrix}
        \epsilon_{xx} \\
        \epsilon_{yy} \\
        \epsilon_{zz} \\
        2 \epsilon_{xy} \\
        2 \epsilon_{xz} \\
        2 \epsilon_{yz}
        \end{bmatrix}

    In 2D (plane stress or plane strain configurations), the elasticity tensor reduces to:

    .. math::

        C = \begin{bmatrix}
        c_{11} & c_{12} & c_{13} \\
        c_{12} & c_{22} & c_{23} \\
        c_{13} & c_{23} & c_{33}
        \end{bmatrix}

    with:

    .. math::

        \begin{bmatrix}
        \sigma_{xx} \\
        \sigma_{yy} \\
        \sigma_{xy}
        \end{bmatrix}
        =
        \begin{bmatrix}
        c_{11} & c_{12} & c_{13} \\
        c_{12} & c_{22} & c_{23} \\
        c_{13} & c_{23} & c_{33}
        \end{bmatrix}
        \cdot
        \begin{bmatrix}
        \epsilon_{xx} \\
        \epsilon_{yy} \\
        2 \epsilon_{xy}
        \end{bmatrix}

    For further details, see Boresi, A. P., Schmidt, R. J., & Sidebottom, O. M. (1993). *Advanced Mechanics of Materials* (5th ed.), p. 84.

    Parameters
    ----------
    elasticity tensor : np.ndarray
        The fourth-order elasticity tensor in Voigt notation.
    """
    def __init__(self, elasticity_tensor=None, **kwargs):

        super().__init__(**kwargs)
        self.model_parameters = {'elasticity_tensor': elasticity_tensor}

    def potential_energy(self, F):
        """
        The strain energy function of a generic st-venant kirchoff is a function quadratic in the green-lagrange
        strain and that uses the elasticity tensor C

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field (lagrangian description)

        Returns
        -------
        """

        strain = self.strain(F, strain_type='GL') # green-lagrange strain tensor
        C = self.get_model_parameters('elasticity_tensor')

        strain_voigt = self.tensor2voigt(strain, isometry_weight=True) # in voigt notation
        Ce = self.voigt2tensor(fe.dot(C, strain_voigt)) # compute tensor product in voigt notation

        return 0.5 * fe.inner(Ce, strain)

    def fill_orthotropic(self, young, poisson, shear, plane_stress=True):
        r"""
        Construct the 4th order elasticity tensor :math:`C` in Voigt notation.

        This function constructs the compliance tensor :math:`K`, which is the inverse of the elasticity tensor :math:`C`.
        The compliance tensor is then inverted to recover the elasticity tensor using:

        .. math::

            C = K^{-1}

        **Definitions**

        - :math:`E`: Young's modulus,
        - :math:`v`: Poisson's coefficient,
        - :math:`G`: Shear modulus.

        **Compliance Tensor Formulation**

        For 3D orthotropic materials, the compliance tensor :math:`K` is written as:

        .. math::

            K = \begin{bmatrix}
            1/E_x & -v_{xy}/E_x & -v_{xz}/E_x & 0 & 0 & 0 \\
            -v_{xy}/E_x & 1/E_y & -v_{yz}/E_y & 0 & 0 & 0 \\
            -v_{xz}/E_x & -v_{yz}/E_y & 1/E_z & 0 & 0 & 0 \\
            0 & 0 & 0 & 1/G_{xy} & 0 & 0 \\
            0 & 0 & 0 & 0 & 1/G_{xz} & 0 \\
            0 & 0 & 0 & 0 & 0 & 1/G_{yz}
            \end{bmatrix}

        For 2D configurations, the compliance tensor changes depending on the stress/strain assumption:

        1. **Plane Stress Condition**:

        .. math::

            K = \begin{bmatrix}
            1/E_x & -v_{xy}/E_x & 0 \\
            -v_{xy}/E_x & 1/E_y & 0 \\
            0 & 0 & 1/G_{xy}
            \end{bmatrix}

        2. **Plane Strain Condition**:

        .. math::

            K = \begin{bmatrix}
            1/E_x - v_{xz}^2/E_z & -v_{xy}/E_x - v_{xz}v_{yz}/E_z & 0 \\
            -v_{xy}/E_x - v_{xz}v_{yz}/E_z & 1/E_y - v_{yz}^2/E_z & 0 \\
            0 & 0 & 1/G_{xy}
            \end{bmatrix}

        For more details, see Boresi, A. P., Schmidt, R. J., & Sidebottom, O. M. (1993). *Advanced Mechanics of Materials* (5th ed.), p94.

        Parameters
        ----------
        young : np.array or list
            Young modulus E_i along the 3 axis [Ex, Ey, Ez]. Only [Ex, Ey] in 2D if plane_stress=True.
        poisson : np.array or list or float
            Poisson coefficient v_ij [v_xy, v_xz, v_yz]. Only [v_xy] in 2D if plane_stress=True
            Contraction in direction j when an extension is applied in direction i
        shear : np.array or list or float
            Shear modulus G_ij [G_xy, G_xz, G_yz]. Only [G_xy] in 2D.
            Shear modulus in direction j on the plane whose normal is in direction i
        plane_stress : bool, optional
            Assumes plane stress assumption (if true, default) or plane strain assumption (if false)

        Returns
        -------
        np.ndarray
            The 4th order elasticity tensor in voigt notation.
            Shape of the tensor is 3x3 (6x6) in 2D (resp. 3D)
        """
        assert all(isinstance(ele, (int, float)) for ele in young), 'young modulus should be a list of float(s)'
        if isinstance(poisson, (int, float)):
            poisson = [poisson]
        else:
            assert all(isinstance(ele, (int, float)) for ele in poisson), 'poisson coefficient should be float(s)'

        if isinstance(shear, (int, float)):
            shear = [shear]
        else:
            assert all(isinstance(ele, (int, float)) for ele in shear), 'shear modulus should be float(s)'

        # - Rename
        E, v, G = young, poisson, shear

        if (len(E) == 3) & (len(v) == 3) & (len(shear) == 3):
            # - 3D case
            K = np.array([[1 / E[0], -v[0] / E[0], -v[1] / E[0], 0, 0, 0],
                          [-v[0] / E[0], 1 / E[1], -v[2] / E[1], 0, 0, 0],
                          [-v[1] / E[0], -v[2] / E[1], 1 / E[2], 0, 0, 0],
                          [0, 0, 0, 1 / G[0], 0, 0],
                          [0, 0, 0, 0, 1 / G[1], 0],
                          [0, 0, 0, 0, 0, 1 / G[2]]])

        elif (len(E) == 3) & (len(v) == 3) & (len(G) == 1) & ~plane_stress:
            # - 2D case with plane stress conditions
            K = np.array([[1 / E[0] - v[1] ** 2 / E[2]    , -v[0] / E[0] - v[1] * v[2] / E[2], 0],
                          [-v[0] / E[0] - v[1] * v[2] / E[2], 1 / E[1] - v[2] / E[2]     , 0],
                          [0             , 0           , 1 / G[0]]])
        elif (len(E) == 2) & (len(v) == 1) & (len(G) == 1) & plane_stress:
            # - 2D case with plane strain conditions
            K = np.array([[1 / E[0]    , -v[0] / E[0], 0],
                          [-v[0] / E[0], 1 / E[1]  , 0],
                          [0     , 0      , 1 / G[0]]])
        else:
            log.error('Some input argument have inconsistent shape')

        # - Inverse the compliance tensor to get the elasticity tensor
        elasticity_tensor = np.linalg.inv(K)

        self.model_parameters = {'elasticity_tensor': elasticity_tensor}

    def fill_isotropic(self, young, poisson, dim=3, plane_stress=True):
        r"""
        Construct the 4th order elasticity tensor :math:`C` in Voigt notation.

        This function first constructs the compliance tensor :math:`K`, which is the inverse of the elasticity tensor :math:`C`.
        The elasticity tensor is then recovered using:

        .. math::

            C = K^{-1}

        **Definitions**

        - :math:`E`: Young's modulus,
        - :math:`v`: Poisson's ratio.

        **Compliance Tensor Formulation**

        Using symmetry, the compliance tensor in 3D can be written as:

        .. math::

            K = \frac{1}{E} \cdot
            \begin{bmatrix}
            1 & -v & -v & 0 & 0 & 0 \\
            -v & 1 & -v & 0 & 0 & 0 \\
            -v & -v & 1 & 0 & 0 & 0 \\
            0 & 0 & 0 & 2(1+v) & 0 & 0 \\
            0 & 0 & 0 & 0 & 2(1+v) & 0 \\
            0 & 0 & 0 & 0 & 0 & 2(1+v)
            \end{bmatrix}

        For 2D configurations, the compliance tensor changes based on the assumptions:

        1. **Plane Stress Condition**:

        .. math::

            K = \frac{1}{E} \cdot
            \begin{bmatrix}
            1 & -v & 0 \\
            -v & 1 & 0 \\
            0 & 0 & 2(1+v)
            \end{bmatrix}

        2. **Plane Strain Condition**:

        The compliance tensor can be written directly for the elasticity tensor in this condition:

        .. math::

            C = \alpha \cdot
            \begin{bmatrix}
            1 - v & v & 0 \\
            v & 1 - v & 0 \\
            0 & 0 & \frac{1 - 2v}{2}
            \end{bmatrix}

        where:

        .. math::

            \alpha = \frac{E}{(1+v)(1-2v)}

        For more details, see Boresi, A. P., Schmidt, R. J., & Sidebottom, O. M. (1993). *Advanced Mechanics of Materials* (5th ed.), p86.

        Parameters
        ----------
        young : float
            Young modulus E
        poisson : float
            Poisson coefficient v
        dim: int, optional
            The dimension of the space, either 2D or 3D (default)
        plane_stress : bool, optional
            Assumes plane stress assumption (if true, default) or plane strain assumption (if false)

        Returns
        -------
        np.ndarray
            The 4th order elasticity tensor in voigt notation.
            Shape of the tensor is 3x3 (6x6) in 2D (resp. 3D)
        """
        assert isinstance(young, (int, float)) and isinstance(poisson, (int, float)), 'Input parameters (E,v) should be float'

        # Rename for clarity
        E, v = young, poisson

        if dim == 3:
            K = (1 / E) * np.array([[1, -v, -v, 0, 0, 0],
                                    [-v, 1, -v, 0, 0, 0],
                                    [-v, -v, 1, 0, 0, 0],
                                    [0, 0, 0, 2 * (1 + v), 0, 0],
                                    [0, 0, 0, 0, 2 * (1 + v), 0],
                                    [0, 0, 0, 0, 0, 2 * (1 + v)]])
            elasticity_tensor =  np.linalg.inv(K)  # inverse the matrix to get the elasticity tensor
        elif (dim == 2) & plane_stress:
            K = (1 / E) * np.array([[1, -v, 0],
                                    [-v, 1, 0],
                                    [0, 0, 2 * (1 + v)]])
            elasticity_tensor =  np.linalg.inv(K)  # inverse the matrix to get the elasticity tensor
        elif (dim == 2) & ~plane_stress:
            alpha = E / ((1 + v) * (1 - 2 * v))
            elasticity_tensor =  alpha * np.array([[1 - v, v, 0],
                                                  [v, 1 - v, 0],
                                                  [0, 0, (1 - 2 * v) / 2]])
        else:
            log.error('Some input argument are not well-defined (dim={2, 3}, plane_stress={false, true})')

        self.model_parameters = {'elasticity_tensor': elasticity_tensor}

    @staticmethod
    def tensor2voigt(tensor, isometry_weight=False):
        r"""
        Convert a 2nd-order symmetric tensor :math:`T` to its Voigt notation.

        This function converts a symmetric 2nd-order tensor, assuming symmetry (:math:`T_{ij} = T_{ji}`), into its Voigt notation.

        The input tensor can be either 2D (:math:`2 \times 2`) or 3D (:math:`3 \times 3`).

        **Voigt Notation Transformation**

        - For a 3D symmetric tensor of size :math:`3 \times 3`:

        .. math::

            T_{Voigt} = [T_{xx}, T_{yy}, T_{zz}, T_{yz}, T_{xz}, T_{xy}]

        - For a 2D symmetric tensor of size :math:`2 \times 2`:

        .. math::

            T_{Voigt} = [T_{xx}, T_{yy}, T_{xy}]

        Parameters
        ----------
        tensor : np.ndarray
            The symmetric tensor to be converted. Must be either of size :math:`3 \times 3` (3D) or :math:`2 \times 2` (2D).
        isometry_weight : bool, optional
            Specifies whether to apply a weight for recovering isometry mapping (e.g., in cases like Hooke's law).
            Default is `False`.

        Returns
        -------
        np.ndarray
            The symmetric tensor in Voigt notation.
        """
        if isometry_weight:
            w = 2
        else:
            w = 1

        if tensor.ufl_shape == (3, 3):  # 3D case
            voigt_tensor = fe.as_vector(
                [tensor[0, 0], tensor[1, 1], tensor[2, 2], w * tensor[1, 2], w * tensor[0, 2], w * tensor[0, 1]])
        elif tensor.ufl_shape == (2, 2):  # 2D case
            voigt_tensor = fe.as_vector([tensor[0, 0], tensor[1, 1], w * tensor[0, 1]])
        else:
            raise ValueError("Invalid tensor shape. Tensor must be either 3x3 or 2x2.")
        return voigt_tensor

    @staticmethod
    def voigt2tensor(voigt_tensor, isometry_weight=False):
        r"""
        Convert a Voigt notation vector to a 2D or 3D 2nd-order symmetric tensor.

        This function reconstructs a symmetric 2nd-order tensor :math:`T`, assuming symmetry (:math:`T_{ij} = T_{ji}`),
        from its Voigt notation representation. The input can represent either a 2D (size :math:`1 \times 3`)
        or 3D (size :math:`1 \times 6`) symmetric tensor.

        **Voigt to Tensor Transformation**

        - For a 2D symmetric tensor, the mapping from Voigt to the tensor is:

        .. math::

            T = \begin{bmatrix}
            T_{xx} & T_{xy} \\
            T_{xy} & T_{yy}
            \end{bmatrix}

        - For a 3D symmetric tensor, the mapping from Voigt to the tensor is:

        .. math::

            T = \begin{bmatrix}
            T_{xx} & T_{xy} & T_{xz} \\
            T_{xy} & T_{yy} & T_{yz} \\
            T_{xz} & T_{yz} & T_{zz}
            \end{bmatrix}

        Parameters
        ----------
        voigt_tensor :  np.ndarray
            The symmetric tensor in Voigt notation. For 2D, the input has size `[1x3]`. For 3D, the input has size `[1x6]`.
        isometry_weight : bool, optional
            If a weight need to be apply to recover isometry mapping (ex: if Hooke's Law). Default is `False`.

        Returns
        -------
        np.ndarray
            The reconstructed symmetric 2nd-order tensor.
        """
        if isometry_weight:
            w = 0.5
        else:
            w = 1

        if voigt_tensor.ufl_shape == (6,):  # 3D case
            tensor = fe.as_tensor([[voigt_tensor[0], voigt_tensor[5]/w, voigt_tensor[4]/w],
                                   [voigt_tensor[5]/w, voigt_tensor[1], voigt_tensor[3]/w],
                                   [voigt_tensor[4]/w, voigt_tensor[3]/w, voigt_tensor[2]]])
        elif voigt_tensor.ufl_shape == (3,):  # 2D case
            tensor = fe.as_tensor([[voigt_tensor[0], voigt_tensor[2]/w],
                                   [voigt_tensor[2]/w, voigt_tensor[1]]])
        else:
            raise ValueError("Invalid Voigt vector length. Length must be either 6 for 3D or 3 for 2D.")
        return tensor