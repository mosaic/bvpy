#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.templates.vforms.poisson
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
from bvpy.vforms.abstract import AbstractVform, Element
from bvpy.utils.pre_processing import create_expression


__all__ = ['PoissonForm']


class PoissonForm(AbstractVform):
    """Poisson vform.

    Parameters
    ----------
    source : int, float or :class:`Expression<dolfin.cpp.function.Expression>`
        Source term of the problem.

    """
    def __init__(self, source=0):
        super().__init__()
        self._elements = [Element('P', 1, 'scalar')]

        self.set_parameters(source=source)

    def construct_form(self, u, v, sol):
        self.lhs = fe.inner(fe.grad(u), fe.grad(v))*self.dx
        self.rhs = self._parameters['source']*v*self.dx

    def set_expression(self):
        self._expression = "grad(u)*grad(v)*dx"
        self._expression += " = source'*v*dx"
