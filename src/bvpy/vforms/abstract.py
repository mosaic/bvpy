#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
import numpy as np
import ufl

from abc import ABC, abstractmethod
from ufl.classes import Zero
from bvpy import logger as log

from collections import namedtuple
from types import FunctionType

from bvpy.utils.pre_processing import create_expression

Element = namedtuple('Element', 'family degree dim')


class AbstractVform(ABC):
    """Defines common attributes and methods for variational forms.

    Attributes
    ----------
    _elements : list of :class:`Element<bvpy.vforms.abstract.Element>`
        The list of all the various types of finite element we are going
        to use in the vform.
    lhs : :class:`Form<ufl.form.Form>`
        The bilinear form of the variational formulation of the considered
        differential problem.
    rhs : :class:`Form<ufl.form.Form>`
        The linear form of the variational formulation of the considered
        differential problem.
    ds : :class:`Measure<ufl.measure.Measure>`
        A measure defined on the boundary of the integration domain.
    dx : :class:`Measure<ufl.measure.Measure>`
        A measure defined on the integration domain.
    _pointsources : list of
        Description of attribute `_pointsources`.

    """

    def __init__(self):
        """Generates a vform instance.

        """
        self._expression = 'Undisclosed by the developer'
        self._elements = []
        self._parameters = {}

        self._lhs = Zero()
        self._rhs = Zero()

        self._ds = fe.ds
        self._dx = fe.dx

        self._pointsources = []

    def __repr__(self):
        """Returns a concise description of the considered vform.

        Returns
        -------
        str
            A compact printable expression of the vform.

        """
        return '<'+str(self.vform_name)+', at '+str(hex(id(self)))+'>'

    def __str__(self):
        """Returns a detailed description of the considered vform.

        Returns
        -------
        str
            An extended printable expression of the vform.

        """
        return self._details

    def add_point_source(self, coordinates, val):
        self._pointsources.append(dict(point=fe.Point(coordinates), value=val))

    def set_element_order(self, degree, ind=0):
        """Sets the order of the implemented finite elements.

        Parameters
        ----------
        degree : int
            The new degree value we want to set.
        ind : int, optional
            The index of the element we want to change (the default is 0).

        """
        self._elements[ind] = self._elements[ind]._replace(degree=degree)

    def set_element_family(self, family, ind=0):
        """Sets the family of the used finite elements.

        Parameters
        ----------
        family : str
            The name of the family type of finite element we want to consider.
        ind : int, optional
            The index of the element we want to change (the default is 0).

        Notes
        -----
        The FE family name can be chosen amongst all possibilities covered by
        FEniCS see the doc for the FunctionSpace class
        (:class:`FunctionSpace<dolfin.functions.functionspace.FunctionSpace>`)
        to get an exhaustive list. But the most common should be:
        * "CG" or "P" for Continuous Lagrange
        * "DG" for Discontinuous Lagrange
        """
        supported_FE_families = ["ARG", "AW", "BDFM", "BDM", "B", "CR", "DG",
                                 "DRT", "HER", "CG", "MTW", "MOR", "N1curl",
                                 "N2curl", "P", "Q", "RT", "R"]
        if family in supported_FE_families:
            self._elements[ind] = self._elements[ind]._replace(family=family)
        else:
            print(f"WARNING, AbstractDomain - set_element_family: {family}\
                    is not a supported type of finite element.")

    def set_parameters(self, **kwargs):
        """Sets a new value for a given parameters.

        Parameters
        ----------
        dict

        Other parameters
        ----------------
        int or float or fenics.Expression
            The value or function that defines the parameter we want to change.

        Returns
        -------
        None

        """
        for name, val in kwargs.items():
            if isinstance(val, (int, float, list, tuple, np.ndarray)):
                self._parameters[name] = fe.Constant(val, name=name)
            elif isinstance(val, (str)):
                deg = max([elmt.degree for elmt in self._elements])
                self._parameters[name] = create_expression(val, degree=deg)
            elif isinstance(val, (fe.Expression, ufl.core.expr.Expr)):
                self._parameters[name] = val
            elif isinstance(val, FunctionType):
                self._parameters[name] = val
            elif val is None:
                self._parameters[name] = val
            else:

                print("WARNING"
                      + " - AbstractVform class, set_parameters method:"
                      + f" type {type(val)} is not handled.")

        self.set_expression()

    def get_parameter(self, param_name):
        """
        Get a specific parameter.

        Parameters
        ----------
        param_name : str
            Name of the parameter to retrieve.

        Returns
        -------
        The value of the requested parameter.

        Raises
        ------
        ValueError
            If the parameter is not found.
        """
        if param_name in self._parameters:
            return self._parameters[param_name]
        else:
            raise ValueError(f"The parameter '{param_name}' is not available.")

    @abstractmethod
    def construct_form(self, u, v, sol):
        """Abstract method that constructs the Vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        pass

    def get_form(self, u, v, sol):
        """Returns the lhs and the rhs elements of the Vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        Returns
        -------
        tuple of two :class:`Form<ufl.form.Form>`
            Respectively the bilinear and the linear forms of
            the variational formulation.

        """
        self.construct_form(u, v, sol)
        return (self.lhs, self.rhs)

    @abstractmethod
    def set_expression(self, *args):
        pass

    def info(self):
        """Prints a detailed descrition of the Vform.

        """
        print(self._details)

    def _set_measures(self, dx, ds):
        """Sets the integration elements (measures) on and around the domain.

        Parameters
        ----------
        dx : :class:`Measure<ufl.measure.Measure>`
            A measure defined on the integration domain.
        ds : :class:`Measure<ufl.measure.Measure>`
            A measure defined on the boundary of the integration domain.

        """
        self.dx = dx
        self.ds = ds

    @property
    def parameters(self):
        """Return the parameters of the vform.

        Returns:
            dict: The parameters of the vform
        """
        return self._parameters

    @property
    def vform_name(self):
        """str: Name of the variational form class.

        """
        return self.__class__.__name__

    @property
    def elements(self):
        """list of :class:`Element<bvpy.vforms.abstract.Element>`: list of the
           types of Finite Elements we consider to model this problem.
        """
        return list(self._elements)

    @property
    def degree(self):
        """int or list of int: The degree(s) of the element(s) used within
           this vform.

        """
        if len(self._elements) == 1:
            return self._elements[0].degree
        else:
            return [elmt.degree for elmt in self._elements]

    @property
    def ds(self):
        """
        Get the measure of the exterior facet integral.

        Returns
        -------
        :class:`Measure<ufl.measure.Measure>`
            A measure defined on the exterior boundary of the integration domain.
        """
        return self._ds

    @ds.setter
    def ds(self, ds_value):
        """
        Set the measure of the exterior facet integral.

        Parameters
        ----------
        ds_value : `Measure<ufl.measure.Measure>`
            A measure defined on the exterior boundary of the integration domain.
        """
        if not isinstance(ds_value, ufl.measure.Measure):
            raise ValueError(f"ds should be an instance of `Measure<ufl.measure.Measure>`")
        self._ds = ds_value

    @property
    def dx(self):
        """
        Get the measure of the cell integral.

        Returns
        -------
        :class:`Measure<ufl.measure.Measure>`
            A measure defined on the cell of the interior of the integration domain.
        """
        return self._dx

    @dx.setter
    def dx(self, dx_value):
        """
        Set the measure of the cell integral.

        Parameters
        ----------
        dx_value : :class:`Measure<ufl.measure.Measure>`
            A measure defined on the cell of the interior of the integration domain.
        """
        if not isinstance(dx_value, ufl.measure.Measure):
            raise ValueError(f"dx should be an instance of `Measure<ufl.measure.Measure>`")
        self._dx = dx_value

    @property
    def lhs(self):
        """
        Get the left-hand side of the variational form.

        Returns
        -------
        :class:`Form<ufl.form.Form>`
            The left-hand side of the variational form.
        """
        return self._lhs

    @lhs.setter
    def lhs(self, lhs_value):
        """
        Set the left-hand side of the variational form.

        Parameters
        ----------
        lhs_value : :class:`Form<ufl.form.Form>`
            The left-hand side of the variational form.
        """
        if not isinstance(lhs_value, ufl.form.Form):
            raise ValueError(f"lhs should be an instance of `Form<ufl.form.Form>`")
        self._lhs = lhs_value

    @property
    def rhs(self):
        """
        Get the right-hand side of the variational form.

        Returns
        -------
        :class:`Form<ufl.form.Form>`
            The right-hand side of the variational form.
        """
        return self._rhs

    @rhs.setter
    def rhs(self, rhs_value):
        """
        Set the right-hand side of the variational form.

        Parameters
        ----------
        rhs_value : :class:`Form<ufl.form.Form>`
            The right-hand side of the variational form.
        """
        if not isinstance(rhs_value, ufl.form.Form):
            raise ValueError(f"rhs should be an instance of `Form<ufl.form.Form>`")
        self._rhs = rhs_value

    @property
    def _details(self):
        """str: A detailed printable description of the domain.

        """

        description = '''Finite Elements
        ---------------
        '''
        for elmt in self._elements:
            description += f'    * Family: {elmt.family}\n'
            description += f'    * Order: {elmt.degree}\n'
            description += f'    * dimensionality: {elmt.dim}\n'

        description += f'''\n Variational form
        -----------------
            * Name: {self.vform_name}
            * Expression: {self._expression}
            * Parameters:
        '''
        for name, value in self._parameters.items():
            if isinstance(value, fe.Constant):
                description += f"           - {name}: {value.values()}\n"
            elif isinstance(value, fe.Expression):
                val = value._cppcode
                val = val.replace("x[0]", "x")
                val = val.replace("x[1]", "y")
                val = val.replace("x[2]", "z")
                description += f"           - {name}: {val}\n"
            elif isinstance(value, fe.UserExpression):
                description += f"           - {name}: {value.values}\n"
            elif value is None:
                description += f"           - {name}: {value}\n"
            else:
                description += f"           - {name}: unknown value...\n"
        return description

    def __setattr__(self, key, value):
        """
        Make a meta-method to update easily parameters of the vform.

        This method overrides the default behavior of setting an attribute by incorporating several checks:
          1. If the attribute name starts with an underscore ('_'), it is considered a hidden attribute and set directly.
          2. If the attribute already exists in the instance's dictionary (`__dict__`) or in the class's dictionary (`__class__.__dict__`),
             it is set directly using the superclass's `__setattr__` method. This includes attributes with custom setters and getters.
          3. If the attribute corresponds to a key in the `_parameters` dictionary, it uses the `set_parameters` method to update the parameter.
          4. If the attribute cannot be matched by the above criteria, an `AttributeError` is raised indicating
             that the parameter does not exist in the model and no setter has been defined.

        If a parameter `young` exists in the self._parameters. It can be updated with
            self.young = value
        after the instantiation of the vform (instead of self._parameters['young'] = value).

        Parameters
        ----------
        key : str
            The name of the attribute to set.
        value
            The value to set for the attribute.

        Raises
        ------
        AttributeError: If the attribute does not exist in the model parameters or class attributes, and no setter has been defined.
        """
        # Check for hidden attributes (starting with _)
        if key.startswith('_'):
            super().__setattr__(key, value)
        # Allow regular attribute assignments for existing attributes
        elif key in self.__dict__ or hasattr(self.__class__, key):
            super().__setattr__(key, value)
        # Check if the key exists in _parameters and set it
        elif key in self._parameters:
            self.set_parameters(**{key: value})
        else:
            raise AttributeError(
                f"Parameter '{key}' does not exist in the model parameters and no attribute setter has been defined.")