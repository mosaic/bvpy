#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.vforms.plasticity
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
from abc import ABC, abstractmethod

import numpy as np
import fenics as fe

from scipy.integrate import quad_vec

from bvpy import logger
from bvpy.vforms.elasticity import HyperElasticForm
from bvpy.utils.post_processing import SolutionExplorer

class MorphoElasticForm(HyperElasticForm):
    r"""
    Variational Formulation for morphoelastic problem

    This class extends the hyperelastic formulation by incorporating the **multiplicative decomposition**
    of the deformation gradient, which models irreversible growth followed by an elastic response. The total
    deformation gradient :math:`F` is decomposed into:

    .. math::

        F = F_e \cdot F_g,

    where:

    - :math:`F` is the total deformation gradient.
    - :math:`F_e` is the elastic deformation gradient.
    - :math:`F_g` is the plastic (growth) deformation gradient, modeling irreversible growth.

    The elastic response is determined solely by :math:`F_e`, while :math:`F_g` accounts for permanent
    deformation due to growth.

    Governing Equations:
    The problem is formulated based on the minimization of the total potential energy, expressed in terms
    of :math:`F_e`. The potential energy is defined as:

    .. math::

        \Pi = \int_\Omega W(F_e) \, dx - \int_\Omega \mathbf{b} \cdot \mathbf{u} \, dx,

    where:

    - :math:`W(F_e)` is the strain energy density function (energy potential), dependent only on the elastic deformation gradient.
    - :math:`\mathbf{b}` is the body force per unit volume.
    - :math:`\mathbf{u}` is the displacement field.

    The variational form balances:

    - **Internal strain energy**: :math:`\int_\Omega W(F_e) \, dx`.
    - **External work**: :math:`\int_\Omega \mathbf{b} \cdot \mathbf{u} \, dx`.

    The variational form :math:`F(u, v)` is derived as:

    .. math::

        F(u, v) = \int_\Omega \frac{\partial W(F_e)}{\partial F_e} : \nabla v \, dx - \int_\Omega \mathbf{b} \cdot v \, dx,

    with :math:`F_e` calculated as:

    .. math::

        F_e = F \cdot F_g^{-1}.

    Here, :math:`F` is the total deformation gradient (from the displacement field) and :math:`F_g` is
    the given growth tensor.

    Other Parameters
    ----------------
    small_displacement : bool
        Consider or not (default) small displacement (infinitesimal strain theory)
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The body forces.
    plane_stress : bool
        Assumes plane stress assumption (if true, default) or plane strain assumption (if false)
    thickness : float
        Thickness of the material if considering 2D case with plane stress/strain. Default is 1.

    Attributes
    ----------
    _elements : :class:`Element<bvpy.vforms.abstract.Element>`
        Description of the Finite Element.
    """
    def __init__(self, potential_energy, F_g=np.diag(np.ones(3)), **kwargs):
        super().__init__(potential_energy=potential_energy, **kwargs)

        self.F_g = F_g

    def F_e(self, F, F_g=None):
        r"""
        Implements the multiplicative decomposition of the deformation gradient:

        .. math::

            F = F_e \cdot F_g

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation tensor
        F_g : `Function<dolfin.function.function.Function>`, optional
            If not provided, the growth tensor F_g  of the vform is used.

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            The elastic deformation tensor
        """
        F_g = self.F_g if F_g is None else F_g

        return F * fe.inv(F_g)

    def construct_form(self, u, v, sol):
        r"""
        Construct the vform. The problem will always be recast as a non-linear problem:

        .. math::

            F(u, v) = 0

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        F = self.grad_deformation(sol, ref='lagrangian')
        F_e = self.F_e(F)

        W = self.compute_potential_energy(F_e) # use the elastic model in elastic_form

        thickness = self.get_parameter('thickness')
        source = self.get_parameter('source')

        self.lhs = fe.det(self.F_g) * thickness * W * self.dx
        self.lhs -= fe.dot(source, sol) * self.dx

        self.lhs = fe.derivative(self.lhs, sol, v)

    def stress(self, F, method='elastic', stress_type='PK1', F_g=None):
        """
        Computes stress measures.

        See :class:`HyperElasticForm<bvpy.vform.HyperElasticForm>` for the stress type measures

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field
        method : str, optional
            The type of stress to return. Either "elastic" (default), "plastic" or "total".
        stress_type : str, optional
            Name of the stress measure we want to compute.
            The default is 'PK2' (2nd Piola–Kirchhoff stress tensor).
            Available measures: 'PK1', 'PK2', 'cauchy'.
        F_g : `Function<dolfin.function.function.Function>`, optional
            If not provided, the growth tensor F_g  of the vform is used.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed stress.

        """
        if F_g is None:
            F_g = self.F_g

        if method == 'elastic':
            method_F = self.F_e(F, F_g=F_g)
        elif method == 'plastic':
            method_F = F_g
        elif method == 'total':
            method_F = F
        else:
            raise ValueError(f"The stress can be either 'elastic', 'plastic' or 'total', not {method}")

        return super().stress(method_F, stress_type=stress_type)

    def strain(self, F, method='elastic', strain_type='GL', F_g=None):
        """
        Computes strain measures.

        See :class:`HyperElasticForm<bvpy.vform.HyperElasticForm>` for the strain type measures

        Parameters
        ----------
        F : `Function<dolfin.function.function.Function>`
            The deformation field
        method : str, optional
            The type of strain to return. Either "elastic" (default), "plastic" or "total".
        strain_type : str, optional
            Name of the strain measure we want to compute
            The default is 'GL' (Green-Lagrange)
            Available measures: 'GL', 'biot', 'natural' and 'almansi'.
        F_g : `Function<dolfin.function.function.Function>`, optional
            If not provided, the growth tensor F_g  of the vform is used.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed strain.

        """
        if F_g is None:
            F_g = self.F_g

        if method == 'elastic':
            method_F = self.F_e(F, F_g=F_g)
        elif method == 'plastic':
            method_F = F_g
        elif method == 'total':
            method_F = F
        else:
            raise ValueError(f"The strain can be either 'elastic', 'plastic' or 'total', not {method}")

        return super().strain(method_F, strain_type=strain_type)

    def set_expression(self):
        self._expression = "psi(u)dx - Budx - Tudx "

    @property
    def F_g(self):
        """
        Gets the growth tensor F_g (plastic part of the deformation tensor) associated with this morphological form.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The growth tensor F_g.
        """
        return self._F_g

    @F_g.setter
    def F_g(self, value):
        """
        Modifies the growth tensor representing plastic part of the deformation tensor.

        Parameter
        ---------
        value : :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The growth tensor to set as F_g.
        """
        self.set_parameters(F_g=value)
        self._F_g = self.get_parameter('F_g')


class AbstractGrowth(ABC):
    """
    Generic class for growth scheme.

    Parameters
    ----------
    plastic_vform : :class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
        The associated plastic form.

    Attributes
    ----------
    plastic_form : :class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
        The associated plastic form.
    growth_incr : :class:`Function<dolfin.cpp.function.Function>`
        The growth increment
    prev_solution : :class:`Function<dolfin.cpp.function.Function>`
        The previous displacement field
    prev_growth : :class:`Function<dolfin.cpp.function.Function>`
        The previous growth field
    dt : int or float
        Integration time-step.
    t : int or float
        The current time-point
    type : string
        Type of growth scheme
    """

    def __init__(self, plastic_vform=None):

        self._plastic_vform = None
        if plastic_vform is not None:
            self.plastic_vform = plastic_vform

        self.growth_incr = None
        self._prev_solution = None
        self._prev_growth = None
        self._type = None
        self._dt = None
        self._t = 0

    @abstractmethod
    def get_growth_increment(self):
        """
        Abstract method that compute the increment of growth to add to the previous growth

        Parameters
        ----------
        """
        pass

    def apply_growth(self, u, v, sol):
        """
        Apply the growth law to the solution, considering each subdomain's growth class.

        Parameters
        ----------
        u : fe.TrialFunction
            The trial function in the variational form.
        v : fe.TestFunction
            The test function in the variational form.
        sol : fe.Function
            The solution function to which growth will be applied.

        Returns
        -------
        tuple : (lhs, rhs)
            The left-hand side and right-hand side of the variational problem after applying growth increment.
        """
        assert isinstance(self.plastic_vform,
                          MorphoElasticForm), 'The variational form should be a MorphoElasticForm class'

        # Set previous states (growth, displacement) if needed
        self.prev_solution = sol  # previous solution

        # Compute the growth increment
        self.get_growth_increment()

        # Update the previous growth with the growth increment
        Id = fe.Identity(sol.geometric_dimension())
        tnsr_func_space = fe.TensorFunctionSpace(self.prev_solution.function_space().mesh(), "DG", 0)
        self.prev_growth = fe.project(self.prev_growth * (Id + self.growth_incr), tnsr_func_space)

        # - Set the growth in the plastic form
        self.plastic_vform.F_g = self.prev_growth

        # - Get the form
        lhs, rhs = self.plastic_vform.get_form(u, v, sol)

        return lhs, rhs

    def update_time(self, dt):
        self.dt = dt
        self.t += dt

    def get_growth(self, only_increment=False):
        """
        Return the current growth field or the increment of growth

        Parameters
        ----------
        only_increment : bool, optional
            Return the current growth field or the increment of growth, by default the current growth field.

        Returns
        -------
        :class:`Function<dolfin.cpp.function.Function>`
        """
        if self.prev_solution is None:
            logger.error('A previous solution is required !')

        growth = None
        if only_increment:
            if self.growth_incr is None:
                logger.error('A growth increment is required, use `get_growth_increment` method to compute it')
            else:
                growth = self.growth_incr
        else:
            if self.prev_growth is None:
                logger.error('A previous growth is required !')
            else:
                growth = self.prev_growth

        tnsr_func_space = fe.TensorFunctionSpace(self.prev_solution.function_space().mesh(), "DG", 0)
        return fe.project(growth, tnsr_func_space)

    @property
    def plastic_vform(self):
        return self._plastic_vform

    @plastic_vform.setter
    def plastic_vform(self, plastic_vform):
        assert isinstance(plastic_vform, MorphoElasticForm), 'The variational form should be a MorphoElasticForm class'
        self._plastic_vform = plastic_vform

    @property
    def prev_growth(self):
        return self._prev_growth

    @prev_growth.setter
    def prev_growth(self, previous):
        if not isinstance(previous, fe.Function):
            if self.prev_solution is None:
                logger.warning("Because no previous solution is available, it was not possible to force the "
                               "conversion of the previous growth to fenics Function.")
            else:
                tnsr_func_space = fe.TensorFunctionSpace(self.prev_solution.function_space().mesh(), "DG", 0)
                previous = fe.project(previous, tnsr_func_space)

        self._prev_growth = previous

    @property
    def prev_solution(self):
        return self._prev_solution

    @prev_solution.setter
    def prev_solution(self, previous):
        if not isinstance(previous, fe.Function):
            logger.error("Input previous solution should be a fenics Function")
        self._prev_solution = previous

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        self._type = value

    @property
    def dt(self):
        return self._dt

    @dt.setter
    def dt(self, value):
        self._dt = value

    @property
    def t(self):
        return self._t

    @t.setter
    def t(self, value):
        self._t = value


class ConstantGrowth(AbstractGrowth):
    """
    Class for constant growth scheme

    Parameters
    ----------
    dg : np.ndarray
        The constant growth rate tensor per time unit

    Attributes
    ----------
    plastic_vform : subclass of :class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
    previous_growth : :class:`Function<dolfin.cpp.function.Function>`
    dt : int or float
        Integration time-step.
    _type : string
        Type of growth scheme
    """

    def __init__(self, dg):
        super().__init__()
        self.type = 'constant'
        self.dg = dg

    def get_growth_increment(self):
        self.growth_incr = fe.Constant(self.dg * self.dt)


class TimeDependentGrowth(AbstractGrowth):
    """
    Class for managing a time-dependent growth scheme.

    This class allows the integration of a growth law that can dynamically vary with time.
    It calculates the growth increment by numerically integrating the growth tensor over
    the specified time interval.

    Parameters
    ----------
    growth_func : callable
        Function that computes the growth rate tensor as a function of time and additional parameters.
    args : tuple
        Additional positional arguments passed to `growth_func`.
    kwargs : dict
        Additional named arguments passed to `growth_func`.

    Examples
    --------
    >>> from bvpy.vforms.plasticity import MorphoElasticForm
    >>> from bvpy.vforms.plasticity import TimeDependentGrowth
    >>> import numpy as np
    >>> # Define a time-dependent growth law with parameters a and b
    >>> def specific_growth_func(t, a, b):
            dg = np.zeros((3, 3))
            dg[0, 0] = b * np.sin(t * np.pi) + a
            return dg
    >>> vform = MorphoElasticForm()  # Assuming the existence of such a class
    >>> a = 1.0
    >>> b = 0.5
    >>> growth_model = TimeDependentGrowth(vform, specific_growth_func, a, b)
    >>> growth_model.update_time(0.5) # Set dt = 0.5 and update t = 0.5
    >>> growth_model.get_growth_increment()
    >>> print(growth_model.growth_incr)
    """

    def __init__(self, growth_func, *args, **kwargs):
        super().__init__()
        self.type = 'time-dependent'
        self.growth_func = growth_func
        self.args = args  # if needed for the computation of dg
        self.kwargs = kwargs  # if needed for the computation of dg

    def get_growth_increment(self):
        # Integrate the growth rate function on [t, t+dt]
        dg_dt, err = quad_vec(self.growth_func, self.t, self.t + self.dt, args=self.args, **self.kwargs)
        self.growth_incr = fe.Constant(dg_dt)


class StrainDependentGrowth(AbstractGrowth):
    """
    Class for managing a strain-dependent growth scheme.
    """

    def __init__(self, extensibility=1, target_strain=None):
        super().__init__()
        self.type = 'strain-dependent'
        self.extensibility = extensibility
        self.target_strain = target_strain

    def get_growth_increment(self):
        assert isinstance(self.plastic_vform,
                          MorphoElasticForm), 'The variational form should be a MorphoElasticForm class'

        # Compute the elastic part of the strain from previous solution
        strain = self.plastic_vform.get_strain(self.prev_solution, method='elastic')

        # Compute the increment of growth assuming an extensibility parameter
        if self.target_strain is None:
            self.growth_incr = fe.Constant(self.extensibility) * strain * fe.Constant(self.dt)
        else:
            # - Convert the current strain in numpy array
            strain_obj = SolutionExplorer(strain)
            strain_np = strain_obj.get_dofs_values()  # do it on the DOFs !

            # - Extract the eigenvalues & eigenvectors of the current strain to filter the eigenvalues according
            #   to the threshold target value
            eigenvals, eigenvecs = np.linalg.eig(strain_np)
            # filtered the eigenvals of the strain diagonal matrix
            eigenvals_filtered = np.where(eigenvals > self.target_strain, eigenvals - self.target_strain, 0)
            # reconstruct the strain matrix in current referential
            strain_np_filtered = np.array([evecs @ np.diag(evals) @ evecs.T for evecs, evals in zip(eigenvecs,
                                                                                                    eigenvals_filtered)])
            # - Convert the filtered strain in fenics function object
            tnsr_func_space = fe.TensorFunctionSpace(self.prev_solution.function_space().mesh(), 'DG', 0)
            strain_filtered = fe.Function(tnsr_func_space)
            strain_filtered.vector().set_local(strain_np_filtered.flatten())

            # - Compute the increment of growth
            self.growth_incr = fe.Constant(self.extensibility) * strain_filtered * fe.Constant(self.dt)


class HeterogeneousGrowth(AbstractGrowth):
    """
    A class to handle different growth models across subdomains in the mesh.

    Parameters
    ----------
    cdata : fe.MeshFunctionSizet
        A MeshFunction marking the subdomains of the mesh.
    growth_classes : dict
        A dictionary of growth classes for each subdomain.

    Attributes
    ----------
    plastic_vform : subclass of :class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
    growth_incr : :class:`Function<dolfin.cpp.function.Function>`
        The growth increment
    prev_solution : :class:`Function<dolfin.cpp.function.Function>`
        The previous displacement field
    prev_growth : :class:`Function<dolfin.cpp.function.Function>`
        The previous growth field
    dt : int or float
        Integration time-step.
    t : int or float
        The current time-point
    _type : string
        Type of growth scheme

    """

    def __init__(self, cdata, growth_classes):
        """
        Initializes the HeterogeneousGrowth class.

        Parameters
        ----------
        cdata : fe.MeshFunctionSizet
            A MeshFunction marking the subdomains of the mesh.
        growth_classes : dict
            A dictionary of growth classes for each subdomain.
        """
        super().__init__()  # dummy vform

        self.cdata = cdata
        self.growth_classes = growth_classes

    def get_growth_increment(self):
        """
        Compute the growth increment for each subdomain
        """
        subdomain_increments = {}
        mesh = self._prev_solution.function_space().mesh()
        tnsr_func_space = fe.TensorFunctionSpace(mesh, 'DG', 0)

        for subdomain_id, growth_class in self.growth_classes.items():
            # Compute the growth increment for the subdomain
            growth_class.get_growth_increment()
            incr = growth_class.growth_incr

            # Project the constant onto the function space
            subdomain_increments[subdomain_id] = fe.project(incr, tnsr_func_space)

        missing_cdata = set(self.cdata.array()) - set(subdomain_increments)

        if missing_cdata:
            # logger.warning(f'Some subdomains do not have any growth laws ({missing_cdata}), null growth will be set')
            # if some index are missing, replace by a null increment
            subdomain_increments = {**subdomain_increments,
                                    **{idx: fe.Function(tnsr_func_space) for idx in missing_cdata}}

        # Initialize an empty tensor for the overall growth increment
        growth_incr = fe.Function(tnsr_func_space)

        # Loop over the cells of the mesh and apply the precomputed growth increments
        for cell in fe.cells(mesh):
            subdomain_id = int(self.cdata[cell.index()])
            cell_growth_incr = subdomain_increments[subdomain_id]

            # Get the DOFs for the current cell in the tensor function space
            cell_dofs = tnsr_func_space.dofmap().cell_dofs(cell.index())

            # Assign each component of the tensor for the current cell
            for i, dof in enumerate(cell_dofs):
                growth_incr.vector()[dof] = cell_growth_incr.vector()[dof]

        self.growth_incr = growth_incr

    def _propagate_property(self, prop_name, value):
        """
        Generic method to propagate property updates to all growth classes.

        Parameters
        ----------
        prop_name : str
            The name of the property to update.
        value : any
            The value to set for the given property.
        """
        for growth_class in self.growth_classes.values():
            setattr(growth_class, prop_name, value)

    @property
    def plastic_vform(self):
        return self._plastic_vform

    @plastic_vform.setter
    def plastic_vform(self, plastic_vform):
        assert isinstance(plastic_vform, MorphoElasticForm), 'The variational form should be a MorphoElasticForm class'
        self._plastic_vform = plastic_vform
        self._propagate_property('plastic_vform', plastic_vform)

    @property
    def prev_solution(self):
        return self._prev_solution

    @prev_solution.setter
    def prev_solution(self, previous):
        self._prev_solution = previous
        self._propagate_property('prev_solution', previous)

    @property
    def prev_growth(self):
        return self._prev_growth

    @prev_growth.setter
    def prev_growth(self, previous):
        self._prev_growth = previous
        self._propagate_property('prev_growth', previous)

    @property
    def dt(self):
        return self._dt

    @dt.setter
    def dt(self, value):
        self._dt = value
        self._propagate_property('dt', value)

    @property
    def t(self):
        return self._t

    @t.setter
    def t(self, value):
        self._t = value
        self._propagate_property('t', value)

    @property
    def type(self):
        return {lab: gcl.type for lab, gcl in self.growth_classes.items()}