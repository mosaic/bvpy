# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.templates.custom_gmsh
#
#      file is an adaptation of customDomain class
#      File contributor(s)
#            Adrien Heymans <adrien.heymans@slu.se>
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
import fenics as fe

from bvpy.domains.abstract import AbstractDomain, BuiltInModel

__all__ = ['CustomDomainGmsh']


class CustomDomainGmsh(AbstractDomain, BuiltInModel):
    """A Domain subclass to create bvpy meshes from existing file meshes
    """

    def __init__(self, fname=None, **kwargs):
        """Initialize the custom domain.

        Parameters
        ----------
        fname : str
            Location of the file to read to get the mesh.

        Other parameters
        ----------------
        dim : float
            Dimension of the embedding space to consider.
        """
        super(CustomDomainGmsh, self).__init__(**kwargs)

        if fname is not None:
            self.read_gmsh(fname)

    def read_gmsh(self, fname):
        """Read a gmsh mesh recorded on disk.

        Parameters
        ----------
        fname : str
            The location of the mesh file to convert.
        """
        if self.mesh is not None:
            self.model.mesh.clear()

        self.convert_gmsh_to_fenics(fname)

        self.ds = fe.Measure('ds', domain=self.mesh, subdomain_data=self.bdata)
        self.dx = fe.Measure('dx', domain=self.mesh, subdomain_data=self.cdata)

    def geometry(self):
        """Adds a custom structure to the Gmsh factory.

        WARNING: For custom domains generated from existing triangular mesh,
        The Gmsh step is not required and therefore the corresponding domains
        will not contain any `geometry`, the corresponding method is therefore
        useless. It remains in the code at a token of heritage.

        """
        pass