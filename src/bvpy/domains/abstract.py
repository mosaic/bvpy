#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.domains.abstract
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import os
import gmsh
import sys

from bvpy import logger as log
import meshio as mio
from abc import ABC, abstractmethod
import tempfile

import fenics as fe
import numpy as np

# Available meshing algorithm from gmsh (experimental algorithms are removed)
# See https://gmsh.info/doc/texinfo/gmsh.html#Choosing-the-right-unstructured-algorithm
# to have more information about what algo fits your need. Briefy :
# - complex curved surfaces (2D) --> MeshAdapt
# - high element quality (2D/3D) --> Frontal-Delaunay
# - very large meshes --> Delaunay (2D) / HXT (3D)
AVAILABLE_MESHING_ALGO_2D = {'MeshAdapt': 1, 'Automatic': 2, 'Delaunay': 5, 'Frontal-Delaunay': 6}
AVAILABLE_MESHING_ALGO_3D = {'Delaunay': 1, 'Frontal-Delaunay': 4, 'HXT': 10}

# Available cell type for the mesh (hybrid mesh are not allowed)
AVAILABLE_CELL_TYPE = {'tetra': 3, 'triangle': 2, 'line': 1, 'vertex': 0} # cell_type : topological_dimension
ELEMENT_GMSH_TYPES = {'line': 1, 'triangle': 2, 'tetra': 4}  # Gmsh code for a given element type

def _generate_default_boundary_data(mesh: fe.Mesh) -> fe.MeshFunction:
    """Sets the boundary of a mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<Dolfin.cpp.mesh.Mesh>`
        The mesh on which we want to define boundaries.

    Returns
    -------
    :class:`MeshFunction<Dolfin.cpp.mesh.MeshFunction>`
        A fenics MeshFunction that marks (with the index 0)
        the boundary of the considered mesh.

    """
    mf = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    mf.set_all(sys.maxsize)
    subdomain = fe.CompiledSubDomain('on_boundary')
    subdomain.mark(mf, 0)
    return mf


def _generate_default_cell_data(mesh: fe.Mesh) -> fe.MeshFunction:
    """Sets the body (i.e. inner part) of a mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<Dolfin.cpp.mesh.Mesh>`
        The mesh on which we want to mark the body.

    Returns
    -------
    :class:`MeshFunction<Dolfin.cpp.mesh.MeshFunction>`
        A fenics MeshFunction that marks (with the index 0 ?)
        the boundary of the considered mesh.
    """
    mf = fe.MeshFunction("size_t", mesh, mesh.topology().dim())
    mf.set_all(0)
    return mf


class AbstractDomain(ABC):
    """The most general class that defines an integration domain.

    Attributes
    ----------
    cdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunctionSizet>`
        Description of attribute `cdata`.
    bdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunctionSizet>`
        Description of attribute `bdata`.
    dx : :class:`Measure<ufl.measure.Measure>`
        An integral measure within the domain of interest.
    ds : :class:`Measure<ufl.measure.Measure>`
        An integral measure on the border of the domain of interest.
    mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
        A mesh representation (topology and geometry) of the considered domain.
    model : :class:`Model<gmsh.model>`
        A purely geometrical description of the domain.
    domain_type : str
        Name of the geometrical model of the domain.
    details : str
        A printable description of the domain main characteristics.

    """
    _model = gmsh.model
    _domain_instances = 0

    def __init__(self,
                 cell_type: str = 'triangle',
                 cell_size=1,
                 verbosity: bool = False,
                 algorithm: str = 'Delaunay',
                 dim: int = 3,
                 clear: bool = False):
        """Generates an instance of the AbstractDomain class.

        Parameters
        ----------
        cell_type : str
            Type of element used to tile the domain.
            (the default is 'triangle').
        cell_size : float, [float, float]
            Characteristic size of the tiling elements (the default is 1). You can relax this constraint by defining
            cell_size as [min_cell_size, max_cell_size].
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements.
            (the default is 'Delaunay').

        Note
        ----
        The following cell types can be used to tile the considered domain:
            * 'line' (1D element)
            * 'triangle' (2D)
            * 'tetra' (3D).
            
        Accepted meshing algorithms depends on the cell_type dimension:
            * 2D ('triangle') : 'Delaunay', 'Frontal-Delaunay', 'MeshAdapt', 'Automatic'
            * 3D ('tetra') : 'Delaunay', 'Frontal-Delaunay', 'HXT'
        """

        super().__init__(cell_type=cell_type, cell_size=cell_size,
                         verbosity=verbosity, algorithm=algorithm)

        AbstractDomain._domain_instances += 1
        if AbstractDomain._domain_instances == 1:
            gmsh.initialize()

        if clear:
            gmsh.clear()

        self._characteristic_size = None # TODO : do we need to init these values here ?
        self._cell_type = cell_type
        self._cell_size = cell_size
        self._algorithm = algorithm

        self._mesh_dir = tempfile.TemporaryDirectory()
        self._dir_name = self._mesh_dir.name

        self.set_cell_type(cell_type)
        self.set_verbosity(verbosity)
        self.set_cell_size(cell_size)
        self.set_meshing_algorithm(algorithm)
        self.set_dim(dim)

        self.dim = dim
        self.mesh = None
        self.cdata = None
        self.bdata = None
        self.sub_domain_names = None # specify explicit names for labels in cdata (MeshFunctionSizet)
        self.sub_boundary_names = None # specify explicit names for labels in bdata (MeshFunctionSizet)


        self.dx = fe.dx
        self.ds = fe.ds

        self.surfaces = dict()
        self.volumes = dict()

    def __del__(self):
        """Clears the temporary file containing the domain.

        """
        self._mesh_dir.cleanup()
        AbstractDomain._domain_instances -= 1
        if AbstractDomain._domain_instances == 0:
            gmsh.finalize()

    def __repr__(self):
        """Returns a short description of the considered domain.

        Returns
        -------
        str
            A printable short expression of the domain.

        """
        return '<'+str(self.domain_type)+', at '+str(hex(id(self)))+'>'

    def __str__(self):
        """Returns a detailed description of the considered domain.

        Returns
        -------
        str
            A printable detailed description of the domain.

        """
        return self._details

    @property
    def domain_type(self):
        """str: Name of the geometrical model of the domain.

        """
        return self.__class__.__name__

    @property
    def model(self):
        """:class:`Model<gmsh.model>`: The (gmsh) geometrical model used
                                       to define the domain.

        """
        return AbstractDomain._model

    @property
    def _details(self):
        """str: A detailed printable description of the domain.

        """

        description = f'''Domain
------
    * Shape: {self.domain_type}
    * Dimension: {self.dim}\n'''

        if self._characteristic_size is not None:
            for name, val in self._characteristic_size.items():
                description += f'    * {name}: {val:.2e}\n'

        if self.mesh is not None:
            description += '\n' + f'''Meshing
-------
        * Algorithm: {self._algorithm}
        * Cell type: {self._cell_type}
        * Number: {len(self.mesh.cells())}
        * Resolution (i.e. prescribed element size): {self._cell_size:.2e}
        * Actual size (min, max): ({self.mesh.hmin():.2e},\
        {self.mesh.hmax():.2e})
    '''
            cells_quality = np.array([c.radius_ratio()
                                      for c in fe.cells(self.mesh)])
            mn_c_qual = cells_quality.mean()
            std_c_qual = cells_quality.std()

            description += "    * Cells quality:"
            description += f" {mn_c_qual:.2e} +/- {std_c_qual:.2e} \n"

        else:
            description = "Mesh not generate yet."
            description += " Use self.discretize() to to so."

        return description

    def synchronize(self):
        """synchronizes the CAD model with the gmsh model.

        """
        self.factory.synchronize()

    @abstractmethod
    def geometry(self, *args, **kwargs):
        """Computes the geometry of the domain.

        Parameters
        ----------
        None

        Returns
        -------
        None

        Note
        ----
        This abstract method must be defined within daughter class
        depending on the specific need.

        """
        pass

    def run_gui(self):
        """Generates a quick gmsh gui with the current model in it.

        """
        gmsh.fltk.run()

    def set_dim(self, dim: int):
        """Sets the dimension of the embedded space

        Parameters
        ----------
        dim : int
            Dimension of the embedded space ?

        """
        if (dim < AVAILABLE_CELL_TYPE[self._cell_type]) or (type(dim) is not int):
            raise ValueError(f'Dimension of the embedded space need to be equal or larger than the dimension'
                             f' of the finite element. Here, you are using "{self._cell_type}" meaning'
                             f' that dim >= {AVAILABLE_CELL_TYPE[self._cell_type]}.')

        self.dim = dim

    def set_cell_size(self, cell_size):
        """Sets the characteristic size of the domain elements.

        Parameters
        ----------
        cell_size : float, [float, float]
            The cell_size value we want to use.

        """
        if isinstance(cell_size, (float, int)):
            gmsh.option.setNumber("Mesh.CharacteristicLengthMin", cell_size)
            gmsh.option.setNumber("Mesh.CharacteristicLengthMax", cell_size)
        elif (isinstance(cell_size, list) and (len(cell_size) == 2)
              and all(isinstance(ele, (int, float)) for ele in cell_size)):
            gmsh.option.setNumber("Mesh.CharacteristicLengthMin", cell_size[0])
            gmsh.option.setNumber("Mesh.CharacteristicLengthMax", cell_size[1])
        else:
            raise ValueError('cell_size can be a number or a list of two numbers')

        self._cell_size = cell_size
    def set_cell_type(self, cell_type: str):
        """Sets the type of finite element (triangle, tetra) you want to use for your mesh

        Parameters
        ----------
        cell_type: str
            The cell_type you want to use

        """
        if cell_type not in AVAILABLE_CELL_TYPE:
            raise ValueError(f'Invalid cell_type, should be one of {AVAILABLE_CELL_TYPE}')

        self._cell_type = cell_type

    def set_cell_size_from_curvature(self, n_elements_per_circle: int):

        if type(n_elements_per_circle) is not int:
            raise ValueError('n_elements should be an integer')

        gmsh.option.setNumber("Mesh.CharacteristicLengthFromCurvature", n_elements_per_circle)

    def set_meshing_algorithm(self, algorithm: str):
        """Sets the algorithm to use to discretize the domain elements.

        Parameters
        ----------
        algorithm : str
            Name of the tiling algorithm to use.

        """
        if AVAILABLE_CELL_TYPE[self._cell_type] == 2: # topological_dimension = 2
            if algorithm not in AVAILABLE_MESHING_ALGO_2D.keys():
                raise ValueError(f'Invalid meshing algorithm, can be {list(AVAILABLE_MESHING_ALGO_2D)}')

            gmsh.option.setNumber("Mesh.Algorithm", AVAILABLE_MESHING_ALGO_2D[algorithm])

        elif AVAILABLE_CELL_TYPE[self._cell_type] == 3: # topological_dimension = 3
            if algorithm not in AVAILABLE_MESHING_ALGO_3D.keys():
                raise ValueError(f'Invalid meshing algorithm, can be {list(AVAILABLE_MESHING_ALGO_3D)}')

            gmsh.option.setNumber("Mesh.Algorithm", AVAILABLE_MESHING_ALGO_3D[algorithm])

        self._algorithm = algorithm


    def set_verbosity(self, verbosity: bool):
        """Sets the verbosity of the considered domain.

        Parameters
        ----------
        verbosity : bool

        """
        if not isinstance(verbosity, bool):
            raise ValueError('verbosity should be a boolean {True, False}')

        if verbosity:
            gmsh.option.setNumber("General.Verbosity", 5)
        else:
            gmsh.option.setNumber("General.Verbosity", 0)

    def discretize(self):
        """Generates a fenics mesh representing the domain.

        Yields
        ------
        mesh : :class:`Mesh <dolfin.cpp.mesh.Mesh>`
            A fenics mesh corresponding to the domain.
        bdata : :class:`MeshFunctionSizet <dolfin.cpp.mesh.MeshFunction>`
            A MeshFunction that labels the borders (i.e., outermost facets)
            of the mesh of the domain.
        cdata : :class:`MeshFunctionSizet <dolfin.cpp.mesh.MeshFunction>`
            A MeshFunction that labels the inner elements (i.e., cells)
            of the mesh of the domain.
        ds : :class:`Measure <ufl.measure.Measure>`
            An integral measure on the border of the domain of interest.
        dx : :class:`Measure <ufl.measure.Measure>`
            An integral measure within the domain of interest.

        Notes
        -----
        - The method also generates a temporary gmsh mesh
          and stores it temporarily on disk.
        - MeshFunctionSizet: `Sizet` means that the output of
          the function is a positive integer.
        """
        if self.mesh is not None:
            self.model.mesh.clear()

        self.generate()
        self.convert_gmsh_to_fenics()

        self.ds = fe.Measure('ds', domain=self.mesh, subdomain_data=self.bdata)
        self.dx = fe.Measure('dx', domain=self.mesh, subdomain_data=self.cdata)

    def rotate(self, point, axe, angle):
        dimTags = [(2, val) for val in self.surfaces.values()] + [(3, val) for val in self.volumes.values()]
        self.factory.rotate(dimTags, *point, *axe, angle)
        self.synchronize()

    def move(self, displacement, return_cdata=True):
        """ Create a FeniCS mesh copy and move it using a displacement field

        Parameters
        ----------
        displacement : fe.Fonction
            The displacement field
        return_cdata : bool, optional
            If the cdata need to be returned. Default is True.

        Returns
        -------
        mesh : :class:`Mesh <dolfin.cpp.mesh.Mesh>`
            A fenics mesh corresponding to the domain deformed
        bdata : :class:`MeshFunctionSizet <dolfin.cpp.mesh.MeshFunction>`
            A MeshFunction that labels the domains


        """
        new_mesh = fe.Mesh(self.mesh)
        dim = self.mesh.geometric_dimension()

        if return_cdata:
            new_cdata = fe.MeshFunction("size_t", new_mesh, dim)
            new_cdata.set_values(self.cdata.array())

        fe.ALE.move(new_mesh, displacement)

        if return_cdata:
            return new_mesh, new_cdata
        else:
            return new_mesh

    def generate(self):
        """ Generate the Gmsh mesh from the current model and write it in a temporary file.
        """
        # - Get the finite element topological dimension
        ele_dim = AVAILABLE_CELL_TYPE[self._cell_type]
        log.debug(f"Convert a mesh with topological element of dimension {ele_dim}")

        self.model.mesh.generate(ele_dim)
        gmsh.write(self._dir_name + '/mesh.msh')

    def convert_gmsh_to_fenics(self, fname=None):
        """ Converts a temporary gmsh mesh into a fenics (dolphin) one.

        Other Parameters
        ----------------
        fname : str, optional
            The path to the gmsh mesh. Default is the automatic path generated by `generate()` method.

        Returns
        -------
        mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
            Fenics mesh corresponding to the domain.
        bdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            MeshFunction that labels the borders (i.e. outermost facets)
            of the mesh of the domain.
        cdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            MeshFunction that labels the inner elements (i.e. cells)
            of the mesh of the domain.

        """
        if fname is None:
            fname = self._dir_name + '/mesh.msh'
            cell_type = self._cell_type # we know the cell_type in advance
        elif os.path.isfile(fname):
            cell_type = None
            if not fname.endswith('.msh'):
                raise ValueError('The input filename should be a gmsh file with extension ".msh"')
        else:
            raise ValueError('The input filename should be a gmsh file with extension ".msh"')

        # - Read the gmsh file with meshio
        mesh_io = mio.read(fname, file_format='gmsh')

        # - Assert that the entities are allowed
        unknown_entites = set(mesh_io.cells_dict.keys()) - set(AVAILABLE_CELL_TYPE.keys())

        if unknown_entites:
            raise RuntimeError(f'Input mesh contains entities that are not handled : {unknown_entites}')

        # - Guess the dimension of the mesh if no model was provided
        if cell_type is None:
            if 'tetra' in mesh_io.cells_dict.keys():
                log.debug('Input mesh is a 3D tetra mesh')
                cell_type = 'tetra'
            elif 'triangle' in mesh_io.cells_dict.keys():
                log.debug('Input mesh is a 2D triangular mesh')
                cell_type = 'triangle'
            elif 'line' in mesh_io.cells_dict.keys():
                log.debug('Input mesh is a 1D line mesh')
                cell_type = 'line'
            else:
                raise RuntimeError(f'No entities were detected after the meshio conversion in the current input mesh!')

            ele_dim = AVAILABLE_CELL_TYPE[cell_type]
        else:
            ele_dim = AVAILABLE_CELL_TYPE[cell_type]
            element_type, element_tags, _ = self.model.mesh.getElements(ele_dim)
            n_entities = len(element_tags[np.where(element_type == ELEMENT_GMSH_TYPES[cell_type])[0][0]])

            if cell_type not in mesh_io.cells_dict:
                raise RuntimeError(f'None elements of the required topological dimension (element_dim = {ele_dim} for '
                                   f'{cell_type}) have been found during the conversion from Gmsh to MeshIo.'
                                   f'\nKnown issue 1: no Entities of the required topological dimension ({ele_dim}) '
                                   f'are defined in your Gmsh mesh. Use YourAbstractObject.factory.getEntities(dim={ele_dim}) '
                                   f'to check if it is the case.\nKnown issue 2: some PhysicalGroups have been set for '
                                   f'Entities of topological dimension < {ele_dim}. If PhysicalGroups are defined, be sure '
                                   f'to define them for all the Entities at topological dimension = {ele_dim}. If not, '
                                   f'all the unmarked Entities will be ignored.\nKnown issue 3: ???')
            elif len(mesh_io.cells_dict[cell_type]) != n_entities:
                raise Warning(f'Some elements have been lost during the conversion of the gmsh mesh, one possible '
                              f'reason is the partial labelling of entities at topological dimension {ele_dim}. Make '
                              f'sure that you label all the entities ! Unlabelled entities at dim {ele_dim} will be'
                              f' ignored...')

            mesh_io.points = mesh_io.points[:, :self.dim]

        mesh = fe.Mesh()

        # - Extract physical properties (and possible name) for the domain
        if ('gmsh:physical' in mesh_io.cell_data_dict.keys()
                and cell_type in mesh_io.cell_data_dict["gmsh:physical"].keys()):
            log.debug(f"Found labels at the dimension {ele_dim}")

            # Notes:
            # 'gmsh: physical' lists the tags given to group of entities of same dimension. Here, the dimension will
            #                  be defined by ele_dim (from the _cell_type).
            #                  There tags can be defined by addPhysicalGroup(ele_dim, ele_tags, tagOfGroup) in gmsh.
            #                  It is possible to set a name for a given PhysicalGroup using setPhysicalName in gmsh.
            #                  Gmsh does not allowed intersection of PhysicalGroup, the first tag for a given entity
            #                  will be the one you will find here.
            #                  Ex: if elements [0, 1] are tagged with 4,
            #                      then 1 is tagged with 2,
            #                      you will see ele. 1 with tag 4 here.
            # Important warning : In the case where some entities have a PhysicialGroup, if it exists entities without
            #                     any PhysicialGroup, they will be ignored !
            #                     TO BE CLEAR : you will loose part of your mesh !
            # Conclusion : using PhysicalGroup, the resulting mesh will always contains PhysicialGroups that form a
            #              partition of the mesh (at the given topological dimension)
            #              However, it is possible that some of the PhysicialGroup do not have a PhysicalName, we will
            #              deal with this situation by setting a default name.

            labels = mesh_io.cell_data_dict["gmsh:physical"][cell_type]

            # - Catch PhysicalName associated with PhysicalGroup
            # - reformat field_data as {tag : name} and filter PhysicalName (`ele_dim` topological dimension)
            if mesh_io.field_data:
                field_data = {lab_dim[0]: lab_name
                              for lab_name, lab_dim in mesh_io.field_data.items()
                              if lab_dim[1] == (ele_dim)}  # tag: name
                log.debug(f"domains : field data => {field_data} found")
            else:
                field_data = {i: f'sub_domain_{i}' for i in np.unique(labels)}
                log.debug(f"domains : default field data => {field_data}")

            if set(labels) != set(field_data):
                log.warning('Domain : the number of PhysicalGroup does not correspond to the number of PhysicalName !')
                log.warning('Domain : some of your PhysicalGroup will be automatically named...')

                # - set automatic names for the other labels
                field_data.update({lab: f'sub_domain_{i}' for i, lab in enumerate(set(labels) - set(field_data))})

            mio.write(self._dir_name + '/' + 'mesh.xdmf',
                      mio.Mesh(points=mesh_io.points,
                               cells={cell_type: mesh_io.cells_dict[cell_type]},
                               cell_data={f"{cell_type}_labels": [labels]}))

            with fe.XDMFFile(self._dir_name + '/' + 'mesh.xdmf') as f:
                f.read(mesh)

            mvc = fe.MeshValueCollection("size_t", mesh, ele_dim)
            with fe.XDMFFile(self._dir_name + '/' + 'mesh.xdmf') as infile:
                infile.read(mvc, f'{cell_type}_labels')
            cdata = fe.cpp.mesh.MeshFunctionSizet(mesh, mvc)

            self.mesh = mesh
            self.cdata = cdata
            self.sub_domain_names = field_data

        else:
            log.debug(f"No labels found at the dimension {ele_dim}, use default.")

            mio.write(self._dir_name + '/' + 'mesh.xdmf',
                      mio.Mesh(points=mesh_io.points,
                               cells={cell_type: mesh_io.cells_dict[cell_type]}))

            with fe.XDMFFile(self._dir_name + '/' + 'mesh.xdmf') as f:
                f.read(mesh)

            self.mesh = mesh
            self.cdata = _generate_default_cell_data(mesh)
            self.sub_domain_names = {0: 'domain'} # default naming for the domain

        # - Extract physical properties (and possible names) for the boundaries (ele_dim - 1)
        bnd_cell_type = [cell_type for  cell_type, d in AVAILABLE_CELL_TYPE.items() if d == (ele_dim - 1)][0]

        if ('gmsh:physical' in mesh_io.cell_data_dict.keys()
                and bnd_cell_type in mesh_io.cell_data_dict["gmsh:physical"].keys()):

            labels = mesh_io.cell_data_dict["gmsh:physical"][bnd_cell_type]

            # - Catch PhysicalName associated with PhysicalGroup
            # - reformat field_data as {tag : name} and filter PhysicalName (`ele_dim` - 1 topological dimension)
            if mesh_io.field_data:
                field_data = {lab_dim[0]: lab_name
                              for lab_name, lab_dim in mesh_io.field_data.items()
                              if lab_dim[1] == (ele_dim - 1)}  # tag: name
                log.debug(f"boundary : field data => {field_data} found")
            else:
                field_data = {i: f'sub_boundary_{i}' for i in np.unique(labels)}
                log.debug(f"boundary : default field data => {field_data}")

            if set(labels) != set(field_data):
                log.warning('Boundary: the number of PhysicalGroup does not correspond to the number of PhysicalName !')
                log.warning('Boundary: some of your PhysicalGroup will be automatically named...')

                # - set automatic names for the other labels
                field_data.update({lab: f'sub_boundary_{i}' for i, lab in enumerate(set(labels)-set(field_data))})

            bnd_mesh = mio.Mesh(points=mesh_io.points,
                                 cells={bnd_cell_type:
                                          mesh_io.cells_dict[bnd_cell_type]},
                                 cell_data={f"boundary": [labels]})
            mio.xdmf.write(self._dir_name + '/mesh_boundary.xdmf', bnd_mesh)

            mvc = fe.MeshValueCollection("size_t", mesh, ele_dim - 1)
            with fe.XDMFFile(self._dir_name + '/mesh_boundary.xdmf') as infile:
                infile.read(mvc, 'boundary')
            bdata = fe.cpp.mesh.MeshFunctionSizet(mesh, mvc)

            self.bdata = bdata
            self.sub_boundary_names = field_data

        else:
            self.bdata = _generate_default_boundary_data(mesh)
            self.sub_boundary_names = {0: 'boundary'}

    def info(self):
        """Prints the descrition of the domain.

        Returns
        -------
        None

        """
        print(self._details)


class BuiltInModel(object):
    """A model based on the Gmsh `geo` factory.

    Attributes
    ----------
    factory : :class:`occ<gmsh.model.geo>`
        A gmsh model `geo` factory to discretize mesh from geometry.

    """

    def __init__(self, *args, **kwargs):
        """Sets the attribute factory to a specific type of gmsh factory.

        Returns
        -------
        factory : :class:`occ<gmsh.model.geo>`
            A gmsh model `geo` factory to discretize mesh from geometry.

        """
        super().__init__()
        self.factory = gmsh.model.geo


class OccModel(object):
    """A model based on the Gmsh `Occ` factory.

    Attributes
    ----------
    factory : :class:`occ<gmsh.model.occ>`
        A gmsh model `occ` factory to discretize mesh from geometry.

    """

    def __init__(self, *args, **kwargs):
        """Sets the attribute factory to a specific type of gmsh factory.

        Returns
        -------
        factory : :class:`occ<gmsh.model.occ>`
            A gmsh model `occ` factory to discretize mesh from geometry.

        """
        super().__init__()
        self.factory = gmsh.model.occ

    def __sub__(self, other):
        """Cutting the left hand side Domain with the right hand side domain.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Returns th result of the cut operation

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.cut(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.cut(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def __add__(self, other):
        """Adding the left hand side Domain with the right hand side domain.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Returns th result of the addition operation

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.fuse(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.fuse(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def __and__(self, other):
        """Compute the intersection of the two domains.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Return the result of the intersection.

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.intersect(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.intersect(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def compute_new_size(self, other, csg):
        """Computes a characteristic size for the new structure.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)
            The domain to combine with the considered one.
        csg : `CSGDomain<bvpy.domains.abstract.CSGDomain>`
            The combined domain that must be updated.

        Returns
        -------
        None

        """

        all_sizes = list(self._characteristic_size.values())\
            + list(other._characteristic_size.values())
        csg._characteristic_size = {'width': max(all_sizes)}


class CSGDomain(AbstractDomain, OccModel):
    def __init__(self, *args, **kwargs):
        """The resulting class of an add/sub or intersection of Domains.

        """
        super(CSGDomain, self).__init__(**kwargs)

    def geometry(self, *args, **kwargs):
        pass
