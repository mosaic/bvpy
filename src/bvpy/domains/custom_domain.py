#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.templates.custom_polygonal
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import gmsh
import fenics as fe
import meshio as mio
import numpy as np

from bvpy.domains.abstract import (AbstractDomain, BuiltInModel,
                                   _generate_default_cell_data,
                                   _generate_default_boundary_data)

__all__ = ['CustomDomain']


class CustomDomain(AbstractDomain, BuiltInModel):
    """A Domain subclass to describe custom surfaces.

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    """

    def __init__(self, points=[], cells=[], **kwargs):
        """Generates a cellularized domain structure.

        Parameters
        ----------
        path : str
            Location of the file to read to get the mesh.

        Other parameters
        ----------------
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        """

        super(CustomDomain, self).__init__(**kwargs)

        self._characteristic_size = {}

        points = np.array(points)
        cells = np.array(cells)
        self.mesh = fe.Mesh()

        editor = fe.MeshEditor()
        if self._cell_type == 'line':
            top = 1
        elif self._cell_type == 'triangle':
            top = 2
        elif self._cell_type == 'tetra':
            top = 3
        editor.open(self.mesh, self._cell_type, top, self.dim)
        editor.init_vertices(len(points))
        editor.init_cells(len(cells))

        [editor.add_vertex(ind, point[:self.dim]) for ind, point in enumerate(points)]
        [editor.add_cell(ind, cell) for ind, cell in enumerate(cells)]

        editor.close()

        self.cdata = _generate_default_cell_data(self.mesh)
        self.bdata = _generate_default_boundary_data(self.mesh)

        self.ds = fe.Measure('ds', domain=self.mesh, subdomain_data=self.bdata)
        self.dx = fe.Measure('dx', domain=self.mesh, subdomain_data=self.cdata)

    def geometry(self):
        """Adds a custom structure to the Gmsh factory.

        WARNING: For custom domains generated from existing triangular mesh,
        The Gmsh step is not required and therefore the corresponding domains
        will not contain any `geometry`, the corresponding method is therefore
        useless. It remains in the code at a token of heritage.

        """
        pass

    @staticmethod
    def read(path):
        """Convert a mesh recorded on disk into a fenics one.

        Parameters
        ----------
        path : str
            The location of the mesh file to convert.

        Notes
        -----
        Gmsh is not involved in the domain production workflow.

        """
        mesh_io = mio.read(path)
        _, dim = mesh_io.points.shape

        points = mesh_io.points[:, :dim]
        cells = mesh_io.cells_dict["triangle"]

        return CustomDomain(points, cells)
