#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       femtk.utils.geometry
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
import numpy as np

from bvpy.domains.abstract import AbstractDomain


class Init_orientation_3D(fe.UserExpression):
    """Short summary.

    Attributes
    ----------
    mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
        Mesh to work with.
    barycenter : :class:`array<numpy.ndarray>` of floats
        The (3D) position vector of the barycenter of all the mesh vertices.
    interior : list of floats
        A 3D vector (x, y, z) to orient properly the mesh.

    """

    def __init__(self, mesh, interior=None, **kwargs):
        """Generates an orientation on a given mesh.

        Parameters
        ----------
        mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
            Mesh to work with.
        interior : list of floats
            A 3D vector (x, y, z) to orient properly the mesh.

        Other parameters
        ----------------
        Kwargs admissible by the generator of mother class:
        :class:`Expression<dolfin.functions.expression.Expression>`

        """
        if interior is None:
            self.barycenter = mesh.coordinates().mean(axis=0)
        else:
            self.barycenter = interior
        super().__init__(**kwargs)

    def eval(self, value, x):
        """Computes the relative position with respect to the barycenter.

        Parameters
        ----------
        value : :class:`array<numpy.array>` of float
            the position we want to normalize.
        x : list of float
            The position vector to re-center.

        Notes
        -----
        This method is used by fenics and is not explicitly used within
        the bvpy code but is nonetheless needed.

        """
        value[0] = x[0] - self.barycenter[0]
        value[1] = x[1] - self.barycenter[1]
        value[2] = x[2] - self.barycenter[2]

    def value_shape(self):
        """Returns the geometrical dimesion of the structure (always 3).

        Returns
        -------
        tuple of int
            This always equals 3.

        """
        return (3,)


def normal(domain, scale=1, interior=None, degree=1, name='normal', on='vertices'):
    """Computes the normal vector field to a given domain.

    Parameters
    ----------
    domain : subclass of
             :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`
        The meshed surface on which we want to compute the normal field.
    scale : float, optional
        The norm of the vector field (the default is 1).
    interior : list(float), optional
        A 3D vector to set the mesh orientation (the default is None).
    degree : int, optional
        The interpolation degree to use to compute the vector space
        on the border (the default is 1).
    name : str, optional
        the label we want to attach to the normal field.
    on : str, optional
        where to compute the normal, either cells (triangles) or vertices
        (the default is 'vertices').

    Returns
    -------
    :class:`Function<dolfin.cpp.function.Function>`
        The computed normal field.

    Notes
    -----
        * To get the normals computed on 
    """
    if isinstance(domain, fe.Mesh):
        mesh = domain
    elif issubclass(type(domain), AbstractDomain):
        if not domain.mesh:
            domain.discretize()
        mesh = domain.mesh
    
    if on == 'cells':
        V = fe.VectorFunctionSpace(mesh, 'DG', 0, dim=3)
    else:
        V = fe.VectorFunctionSpace(mesh, 'P', degree, dim=3)
    
    u = fe.Function(V)
    ux = fe.Function(V.sub(0).collapse())
    uy = fe.Function(V.sub(1).collapse())
    uz = fe.Function(V.sub(2).collapse())
    
    mesh.init_cell_orientations(Init_orientation_3D(mesh, interior))
    orientations = np.transpose(mesh.cell_orientations())
    orientations = np.where(orientations == 1, -1, 1)
    
    mesh.init(0, 2)
    
    if on == 'cells':
        vf = np.array([orientations[c.index()] * c.cell_normal().array() 
                           for c in fe.cells(mesh)])
        
        ux.vector().set_local(vf[:, 0])
        uy.vector().set_local(vf[:, 1])
        uz.vector().set_local(vf[:, 2])
    
    elif on == 'vertices':
        vf = np.zeros([mesh.num_entities(0), 3])
        
        V_scal = fe.FunctionSpace(mesh, 'CG', 1)
        d2v_scal = fe.dof_to_vertex_map(V_scal)

        for v in fe.vertices(mesh):
            f_vertex = np.zeros(3)
            for c in fe.cells(v):
                orientation = orientations[c.index()]
                f_vertex += orientation * c.cell_normal().array()
            vf[v.index()] = scale*f_vertex/np.linalg.norm(f_vertex)

        ux.vector().set_local(vf[d2v_scal, 0])
        uy.vector().set_local(vf[d2v_scal, 1])
        uz.vector().set_local(vf[d2v_scal, 2])

    fe.assign(u.sub(0), ux)
    fe.assign(u.sub(1), uy)
    fe.assign(u.sub(2), uz)

    u.rename(name, 'label')

    return u


def get_boundary_facets(mesh):
    """Selects the boundary elements of a given mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
        The mesh from which we want to get the boundary elements.

    Returns
    -------
    facets : list of :class:`facets<dolfin.cpp.mesh.facets>`
        the list of the outermost elements of the considered mesh.

    Notes
    -----
    The returned elements corresponds to mesh elements of topological
    dimension (n-1) for a mesh of topological dimension n.

    """
    mesh.init()
    facets = list()

    for fct in fe.facets(mesh):
        if len(list(fe.cells(fct))) == 1:
            facets.append((fct))

    return facets


def get_boundary_vertex(mesh):
    """Gets the outermost vertices of a mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
        The mesh from which we want to get the boundary vertices.


    Returns
    -------
    list of :class:`Vertex<dofin.cpp.mesh.Vertex>`
        The list of the outermost vertices of the considered mesh.

    """
    bnd_facets = get_boundary_facets(mesh)
    bnd_vertex = list()

    for fct in bnd_facets:
        [bnd_vertex.append(v.index()) for v in fe.vertices(fct)]

    bnd_vertex = list(set(bnd_vertex))  # Unique
    bnd_vertex = [fe.Vertex(mesh, i) for i in bnd_vertex]

    return bnd_vertex


def boundary_normal(domain, scale=1, name='boundary_normal'):
    """Computes along the boundary the normals within the tangent plane.

    Parameters
    ----------
    domain : subclass of :class:`Domain<bvpy.domains.AbstracDomain>`
        The meshed surface on which we want to compute the normal field.
    scale : float, optional
        The norm of the vector field (the default is 1).
    name : str, optional
        the label we want to attach to the normal field.

    Returns
    -------
    :class:`Function<dolfin.cpp.function.Function>`
        The computed normal field.

    """
    if isinstance(domain, fe.Mesh):
        mesh = domain
        dim = len(domain.coordinates()[0])
    elif issubclass(type(domain), AbstractDomain):
        mesh = domain.mesh
        dim = domain.dim
    else:
        raise ValueError("domain must be a mesh or a subclass of AbstractDomain")

    mesh.init()
    bnd_facets = get_boundary_facets(mesh)
    facets_index = [f.index() for f in bnd_facets]
    facet_normals = np.zeros((mesh.num_entities(1), 3))
    normals = np.zeros((mesh.num_entities(0), 3))

    #for fct in bnd_facets:
    #    cell = [c for c in fe.cells(fct)][0]
    #    vertex = [v for v in fe.vertices(fct)][0]
    #    normal_cell = cell.cell_normal().array()
    #    midpoint_facet = fct.midpoint().array()
    #    midpoint_vertex = vertex.midpoint().array()
    #    vec = midpoint_vertex-midpoint_facet
    #    vec = vec/np.linalg.norm(vec)
    #    n = np.cross(normal_cell, vec)
    #    facet_normals[fct.index()] = n

    for fct in bnd_facets:
        if fct.dim() == dim - 1:  # Manifold without curvature
            facet_normals[fct.index()] = fct.normal().array()
        elif fct.dim() == 1 and dim == 3:  # 2-manifold embedded in a 3-dimension space
            cell = [c for c in fe.cells(fct)][0]
            vertex = [v for v in fe.vertices(fct)][0]
            normal_cell = cell.cell_normal().array()
            midpoint_facet = fct.midpoint().array()
            midpoint_vertex = vertex.midpoint().array()
            midpoint_cell = cell.midpoint().array()
            inner_vct = midpoint_facet - midpoint_cell
            vec = midpoint_vertex - midpoint_facet
            vec = vec / np.linalg.norm(vec)
            n = np.cross(normal_cell, vec)
            n *= np.sign(np.dot(inner_vct, n))
            facet_normals[fct.index()] = n
        else:
            raise NotImplementedError("Normal boundary not implemented for this type of mesh")

    for fct in bnd_facets:
        for vtx in fe.vertices(fct):
            norm = np.array([facet_normals[f.index()]
                             for f in fe.facets(vtx)
                             if f.index() in facets_index])
            norm = np.sum(norm, axis=0)
            norm = norm / np.linalg.norm(norm)
            normals[vtx.index()] = norm

    normals = scale * normals
    V = fe.VectorFunctionSpace(mesh, 'P', 1)
    u = fe.Function(V)
    d2v = fe.dof_to_vertex_map(V)
    dim = mesh.geometric_dimension()
    u.vector().set_local(normals[:,:dim].flatten()[d2v])

    u.rename(name, 'label')

    return u
