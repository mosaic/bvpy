#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       femtk.boundary_conditions.neumann
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
from bvpy.domains.geometry import boundary_normal
from bvpy.utils.pre_processing import create_expression
from .boundary import Boundary


class NeumannInterface(object):
    """Base class for Neumann boundary conditions.

    Parameters
    ----------
    val : float, int, list, tuple or str
        The value of the Neumann condtion.
    boundary : :class:`Boundary<femtk.boundary_conditions.boundary.Boundary>`
        Class definig the boundary of the boundary condition.
    ind : int
        The index of the condtion (optionnal, must be unique among all
        the Neumann conditions)

    Attributes
    ----------
    _id : int
        Id of the condition.
    _boundary : :class:`Boundary<femtk.boundary_conditions.boundary.Boundary>`
        Class definig the boundary of the boundary condition.
    _val : float, int, list, tuple or str
        The value of the Neumann condtion.
    _counter : int
        The counter of the number of instanciated Neumann class

    """
    _counter = 0

    def __init__(self, val, boundary, ind=None):
        if ind is None:
            NeumannInterface._counter += 1
            self._id = NeumannInterface._counter
        else:
            self._id = ind

        if isinstance(boundary, Boundary):
            self._boundary = boundary
        else:
            self._boundary = Boundary(boundary)

        self._val = val

    def apply(self):
        raise NotImplementedError

    def markMeshFunction(self, mf):
        self._boundary.markMeshFunction(self._id, meshfunc=mf)

    def __repr__(self):
        return "<" + self.__class__.__name__ + " object, location: " +\
               str(self._boundary._string) + ", value: " + str(self._val) + ">"


class ConstantNeumann(NeumannInterface):
    """Short summary.

    Parameters
    ----------
    val : float, int, list, tuple
        Value of the Neumann condition.
    boundary : :class:`Boundary<femtk.boundary_conditions.boundary.Boundary>`
        Class definig the boundary of the boundary condition.
    ind : int
        The index of the condtion (optionnal, must be unique among all
        the Neumann conditions)

    """

    def __init__(self, val, boundary, ind=None):
        assert isinstance(val, (float, int, list, tuple))
        super().__init__(val, boundary, ind)

    def apply(self, mf=None):
        if mf is not None:
            self.markMeshFunction(mf)
        return fe.Constant(self._val)


class VariableNeumann(NeumannInterface):
    """Short summary.

    Parameters
    ----------
    val : str
        The string compiled in C++.
    boundary : :class:`Boundary<femtk.boundary_conditions.boundary.Boundary>`
        Class definig the boundary of the boundary condition.
    ind : int
        The index of the condition (optionnal, must be unique among all
        the Neumann conditions)

    """

    def __init__(self, val, boundary, ind=None, degree=None):
        assert isinstance(val, str)
        super().__init__(val, boundary, ind)
        self._degree = degree

    def apply(self, functionspace, mf=None):
        if mf is not None:
            self.markMeshFunction(mf)
        return create_expression(self._val, functionspace, degree=self._degree)


def neumann(val, boundary, degree=None, ind=None):
    if isinstance(val, (float, int, list, tuple)):
        return ConstantNeumann(val, boundary, ind=ind)
    elif isinstance(val, str):
        return VariableNeumann(val, boundary, ind=ind, degree=degree)


class NormalNeumann(NeumannInterface):

    def __init__(self, val=1, boundary='all',
                 method='topological', ind=None, degree=None):
        """Generator of the NormalNeumann class.

        Parameters
        ----------
        val : float optional
            amplitude of the normal vector field (the default is 1).
        boundary : :class:`Domain<bvpy.boundary_conditions.domains.Domain>`,
            Optional (default is 'all'). defines where the condition
            must be applied.
        method : str
            Optional (default is 'topological'). Name of the method to use.
            Should be in ['topological', 'geometrical', 'pointwise'].
        ind : int or None
            Optional (default is None). The index of the condition.
            Must be unique among all the Neumann conditions.
        degree : int or None
            Optional (default is None).

        Returns
        -------
        :class:`VariableNeumann<bvpy.boundary_conditions.neumann.VariableNeumann>`
            A constrain applied to the function space
            in witch the solution will be looked for.

        """
        assert isinstance(val, (int, float))

        super().__init__(val, boundary, ind)
        self._degree = degree

        if isinstance(boundary, Boundary):
            self._boundary = boundary
        else:
            self._boundary = Boundary(boundary)

        self._val = val
        self._method = method

    def __repr__(self):
        """Gives a representation of a NormalNeumann instance.

        Returns
        -------
        str
            a printable description of the NormalNeumann instance.

        """
        return "<NormalNeumann object, location: "\
               + str(self._boundary._string) + ", value: " + str(self._val)+">"

    def apply(self, functionSpace, mf=None):
        if mf is not None:
            self.markMeshFunction(mf)
        return boundary_normal(functionSpace.mesh(), scale=self._val)
