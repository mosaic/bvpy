#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.boundary_conditions.boundary
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
from sympy import And, Function, Or, Symbol, Not
from sympy.parsing.sympy_parser import parse_expr

from bvpy import MPI_COMM_WORLD, logger

_XYZ = {'x': Symbol('x[0]'),
        'y': Symbol('x[1]'),
        'z': Symbol('x[2]'),
        'all': Symbol('on_boundary'),
        'near': Function('near')}


class Boundary(object):
    """Defines the boundary of a domain.

    Parameters
    ----------
    expr : str
        An expression used to compute the boundary.


    Attributes
    ----------
    _string : str
        The sting passed to the class
    _expr : str
        The parsed string for fenics.
    _domain : SubDomain
        The subdomain fenics defining the boundary.
    _kwargs : dict
        Optional argument.

    Examples
    --------
    >>> Boundary("near(x+a, 2)", a=0) & Boundary("all")

    """
    def __init__(self, expr, **kwargs):
        """Constructor of the Boundary class.

        Parameters
        ----------
        expr : str
            An expression used to compute the boundary.

        Yields
        -------
        Boundary
            A boundary instance.

        """
        self._string = expr
        self._expr = parse_expr(expr, local_dict=_XYZ, evaluate=False)
        self._domain = fe.CompiledSubDomain(str(self._expr), **kwargs,
                                            mpi_comm=MPI_COMM_WORLD)
        self._kwargs = kwargs

    def __call__(self):
        """Fetches the fenics object defining the boundary.

        Returns
        -------
        Domain
            Description of returned object.

        """
        return self._domain

    def __or__(self, other):
        """Intersects two boundary domains together.

        Parameters
        ----------
        other : type
            Description of parameter `other`.

        Returns
        -------
        type
            Description of returned object.

        """
        new = Boundary('near(x, 0)')
        s1 = Symbol(str(self._expr))
        s2 = Symbol(str(other._expr))
        new._expr = Or(s1, s2)
        new._string = self._string+' | '+other._string
        new._kwargs = {**self._kwargs, **other._kwargs}
        str_expr = str(new._expr).replace('&', '&&')\
                                 .replace('|', '||')\
                                 .replace('~', '!')
        new._domain = fe.CompiledSubDomain(str_expr, **new._kwargs,
                                           mpi_comm=MPI_COMM_WORLD)
        return new

    def __and__(self, other):
        """Adds two boundary domains together.

        Parameters
        ----------
        other : type
            Description of parameter `other`.

        Returns
        -------
        type
            Description of returned object.

        """
        new = Boundary('near(x, 0)')
        s1 = Symbol(str(self._expr))
        s2 = Symbol(str(other._expr))
        new._expr = And(s1, s2)
        new._string = self._string+' & '+other._string
        new._kwargs = {**self._kwargs, **other._kwargs}
        str_expr = str(new._expr).replace('&', '&&')\
                                 .replace('|', '||')\
                                 .replace('~', '!')
        logger.debug('expression in and: %s ', str_expr)
        new._domain = fe.CompiledSubDomain(str_expr, **new._kwargs,
                                           mpi_comm=MPI_COMM_WORLD)

        return new

    def __invert__(self):
        """Inverts a boundary domain.

        Parameters
        ----------
        other : type
            Description of parameter `other`.
        Returns
        -------
        type
            Description of returned object.
        """
        new = Boundary('near(x, 0)')
        s1 = Symbol(str(self._expr))
        new._expr = Not(s1)
        new._string = '~ ('+self._string+')'
        new._kwargs = self._kwargs
        str_expr = str(new._expr).replace('&', '&&')\
                                 .replace('|', '||')\
                                 .replace('~', '!')
        logger.debug('expression in and: %s ', str_expr)
        new._domain = fe.CompiledSubDomain(str_expr, **new._kwargs,
                                           mpi_comm=MPI_COMM_WORLD)

        return new

    def markMeshFunction(self, valueTrue,
                         mesh=None, valueFalse=0, meshfunc=None,
                         dim_topology=0, type='double'):
        """Short summary.

        Parameters
        ----------
        valueTrue : type
            Description of parameter `valueTrue`.
        mesh : type
            Description of parameter `mesh` (the default is None).
        valueFalse : type
            Description of parameter `valueFalse` (the default is 0).
        meshfunc : type
            Description of parameter `meshfunc` (the default is None).
        dim_topology : type
            Description of parameter `dim_topology` (the default is 0).
        type : type
            Description of parameter `type` (the default is 'double').

        Returns
        -------
        type
            Description of returned object.

        """
        if meshfunc is not None:
            self._domain.mark(meshfunc, valueTrue) # this mark the subdomain meshfunc using the condition
        else:
            meshfunc = fe.MeshFunction(type, mesh, dim_topology)
            meshfunc.set_all(valueFalse)
            self._domain.mark(meshfunc, valueTrue)

        return meshfunc
