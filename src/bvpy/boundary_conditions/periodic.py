#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.boundary_conditions.periodic
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#          Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------


import fenics as fe


class SquarePeriodicBoundary(fe.SubDomain):
    """Defines periodic boundary conditions on a rectangular domain.

    Attributes
    ----------
    x1 : int, float
        Abscissa of the lower left corder of the considered rectangle
        (the default is 0).
    y1 : int, float
        Ordinate of the lower left corder of the considered rectangle
        (the default is 0).
    x2 : int, float
        Abscissa of the upper right corder of the considered rectangle
        (the default is 1).
    y2 : int, float
        Ordinate of the upper right corder of the considered rectangle
        (the default is 1).

    """

    def __init__(self, *args, x=0, y=0, length=1, width=1, **kwargs):
        """Instanciates periodic boundary conditions.

        Parameters
        ----------
        x : int, float
            Abscissa of the lower left corder of the rectangle to consider
            (the default is 0).
        y : int, float
            Ordinate of the lower left corder of the rectangle to consider
            (the default is 0).
        length : int, float
            size of the rectangle along the abscisse direction
            (the default is 1).
        width : int, float
            size of the rectangle along the ordinate direction
            (the default is 1).

        Other parameters
        ----------------
        *args : type
            Description of parameter `*args`.
        **kwargs : type
            Description of parameter `**kwargs`.

        Returns
        -------
        None

        """
        super().__init__(*args, **kwargs)

        self.x1 = x
        self.y1 = y
        self.x2 = length + x
        self.y2 = width + y

    def inside(self, x, on_boundary):
        """Short summary.

        Parameters
        ----------
        x : list, numpy.ndarray
            position vector to test.
        on_boundary :
            :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
            The boundary of the considered domain.

        Returns
        -------
        Bool
            True if the position vector is inside the domain, False otherwise.

        """
        return (fe.near(x[0], self.x1)
                or fe.near(x[1], self.y1))\
               and (not ((fe.near(x[0], self.x1)
                          and fe.near(x[1], self.y2))
                         or (fe.near(x[0], self.x2)
                         and fe.near(x[1], self.y1))))\
               and on_boundary

    def map(self, x, y):
        """Connects two points from different sides of the domain.

        Parameters
        ----------
        x : list, numpy.ndarray()
            first position vector to consider.
        y : list, numpy.ndarray()
            second position vector to consider.

        Returns
        -------
        None.

        """
        if fe.near(x[0], self.x2) and fe.near(x[1], self.y2):
            y[0] = x[0] - self.x2
            y[1] = x[1] - self.y2
        elif fe.near(x[0], self.x2):
            y[0] = x[0] - self.x2
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - self.y2


class LinearPeriodicBoundary(fe.SubDomain):
    """Connects the two end of a rectangular mesh along a particular direction.

    Attributes
    ----------
    x1 : int, float
        Abscissa of the lower left corder of the considered rectangle
        (the default is 0).
    y1 : int, float
        Ordinate of the lower left corder of the considered rectangle
        (the default is 0).
    x2 : int, float
        Abscissa of the upper right corder of the considered rectangle
        (the default is 1).
    y2 : int, float
        Ordinate of the upper right corder of the considered rectangle
        (the default is 1).

    """

    def __init__(self, *args, xmin=0, xmax=1, direction="x", **kwargs):
        """Instanciates periodic boundary conditions.

        Parameters
        ----------
        xmin : int, float
            Optional (the default is 0).
            Abscissa (ordinate) of the left (bottom) border of the mesh.
        xmax : int, float
            Optional (the default is 1).
            Abscissa (ordinate) of the right (top) border of the mesh.
        direction : str
            Optional (the default is "x").
            Direction to close. Should either be "x" or "y"

        Other parameters
        ----------------
        *args : type
            Description of parameter `*args`.
        **kwargs : type
            Description of parameter `**kwargs`.

        Returns
        -------
        None

        """
        super().__init__(*args, **kwargs)

        assert direction in ["x", "y"]

        self.xmin = xmin
        self.xmax = xmax
        self.distance = xmax - xmin
        self.direction = direction


    def inside(self, x, on_boundary):
        """Short summary.

        Parameters
        ----------
        x : list, numpy.ndarray
            position vector to test.
        on_boundary :
            :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
            The boundary of the considered domain.

        Returns
        -------
        Bool
            True if the position vector is inside the domain, False otherwise.

        """
        if self.direction == "x":
            return (fe.near(x[0], self.xmin) and on_boundary)
        else:
            return (fe.near(x[1], self.xmin) and on_boundary)


    def map(self, x, y):
        """Connects two points from different sides of the domain.

        Parameters
        ----------
        x : list, numpy.ndarray()
            first position vector to consider.
        y : list, numpy.ndarray()
            second position vector to consider.

        Returns
        -------
        None.

        """
        if self.direction == "x":
            y[0] = x[0] - self.distance
            y[1] = x[1]

        else:
            y[0] = x[0]
            y[1] = x[1] - self.distance
