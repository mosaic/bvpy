#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
import numpy as np
from sympy import Function, Symbol
from sympy.parsing.sympy_parser import parse_expr

from types import FunctionType
from bvpy import logger

def _stringify_expression_from_string(source):
    """
    Converts a string expression into a structure of tuples and strings.
    The function parses mathematical expressions with parentheses or square brackets and converts
    them into tuples of strings, preserving the expression format.

    This function will help to write dolfin.functions.expression.Expression cppcode more easily.

    Parameters
    ----------
    source : str
        The input expression as a string. The expression may contain tuples, mathematical
        expressions, and can use either parentheses '()' or square brackets '[]'.

    Returns
    -------
    str or tuple
        The converted expression, either as a string or a tuple of strings (possibly nested).

    Raises
    ------
    ValueError: If the number of opening and closing parentheses does not match.

    Examples
    --------
    >>> from bvpy.utils.pre_processing import _stringify_expression_from_string
    ### EXAMPLE 1: Deal with scalar expressions
    >>> source = "x"
    >>> expr = _stringify_expression_from_string(source)
    >>> print(f"Input {source} --> Output {expr}")
    ### EXAMPLE 2: Deal with vector expressions
    >>> source = "(x, y)"
    >>> expr = _stringify_expression_from_string(source)
    >>> print(f"Input {source} --> Output {expr}")
    >>> source = "[cos(y), sin(x)+y]"
    >>> expr = _stringify_expression_from_string(source)
    >>> print(f"Input {source} --> Output {expr}")
    ### EXAMPLE 3: Deal with tensor expressions
    >>> source = "((x, 2*x),(y, 4+2*x))"
    >>> expr = _stringify_expression_from_string(source)
    >>> print(f"Input {source} --> Output {expr}")
    ### EXAMPLE 4: Detect if parentheses are closed (or opened)
    >>> source = "(x, y"
    >>> _stringify_expression_from_string(source)
    """
    # Replace square brackets with parentheses
    expr = source.replace('[', '(').replace(']', ')')

    # Check if parentheses are balanced
    if expr.count('(') != expr.count(')'):
        raise ValueError("Mismatched number of opening and closing parentheses.")

    def parse_expression(s):
        """Recursively parses expressions and converts them into tuples of strings."""
        result, current, level = [], [], 0
        for char in s:
            if char == ',' and level == 0:
                result.append(''.join(current).strip())  # Add current expression
                current = []
            else:
                if char == '(': level += 1
                if char == ')': level -= 1
                current.append(char)
        result.append(''.join(current).strip())  # Add the last element

        # Recursively process nested tuples or return the string element
        return tuple(
            parse_expression(item[1:-1]) if item.startswith('(') and item.endswith(')') else item for item in
            result)

    # If the expression is wrapped in parentheses, parse it, otherwise return the expression as is
    return parse_expression(expr[1:-1]) if expr.startswith("(") and expr.endswith(")") else expr


def _expression_from_string(source, functionspace, degree, **kwargs):
    # TODO: deal with expression with vector/tensor !
    _XYZ = {'x': Symbol('x[0]'),
            'y': Symbol('x[1]'),
            'z': Symbol('x[2]'),
            'border': Symbol('on_boundary'),
            'near': Function('near')}

    if isinstance(source, (tuple, list)):
        expr = []
        for src in source:
            expr.append(str(parse_expr(src, local_dict=_XYZ, evaluate=False)))
    else:
        expr = str(parse_expr(source, local_dict=_XYZ, evaluate=False))

    if degree is not None:
        expr = fe.Expression(expr, degree=degree, **kwargs)
    else:
        expr = fe.Expression(expr, element=functionspace.ufl_element(), **kwargs)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr


def _expression_from_list(source, functionspace, degree):
    source = np.asarray(source)
    dim = source[0].shape
    if len(source.shape) == 1:
        source = np.array([[i] for i in source])

    class UserExpr(fe.UserExpression):
        def eval_cell(self, value, x, ufc_cell):
            val = source[ufc_cell.index]
            for i, v in enumerate(val):
                value[i] = v

        def value_shape(self):
            return dim

    if degree is None:
        expr = UserExpr()
    else:
        expr = UserExpr(degree=degree)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr

def _expression_from_function(source, functionspace, degree):
    x = [0, 0, 0]
    dim = np.array(source(x)).shape

    if dim == ():

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                val = [source(x)]
                for i, v in enumerate(val):
                    value[i] = v

            def value_shape(self):
                return dim

    elif len(dim) == 1:

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                val = source(x)
                for i, v in enumerate(val):
                    value[i] = v

            def value_shape(self):
                return dim

    elif len(dim) == 2:

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                flat_list = [item for sublist in source(x) for item in sublist]
                for i, v in enumerate(flat_list):
                    value[i] = v
                # val = np.asarray(source(x))
                # for i, arr in enumerate(val):
                #     for j, v in enumerate(arr):
                #         value[i][j] = v

            def value_shape(self):
                return dim

    if degree is None:
        expr = UserExpr()
    else:
        expr = UserExpr(degree=degree)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr


def create_expression(source, functionspace=None, degree=None, **kwargs):
    if isinstance(source, str):
        return _expression_from_string(source, functionspace, degree, **kwargs)
    elif isinstance(source, FunctionType):
        return _expression_from_function(source, functionspace, degree)
    elif isinstance(source, (list, tuple)):
        if any([isinstance(i, str) for i in source]):
            return _expression_from_string(source,
                                           functionspace,
                                           degree,
                                           **kwargs)
        else:
            return _expression_from_list(source, functionspace, degree)
    else:
        logger.error(str(source)+' is not a string, tuple, list or a function')


class HeterogeneousParameter(fe.UserExpression):
    """
    A class to define heterogeneous parameters within a FEniCS domain,
    supporting scalars, vectors, and tensors up to 4th order.

    Attributes
    ----------
    cdata : fe.MeshFunction
        A MeshFunction marking the subdomains of the mesh.
    values : dict
        A dictionary of values for each subdomain label.
        Can contain scalars, vectors, or tensors up to 4th order.

    Methods
    -------
    check_value_shape()
        Checks and sets the shape of the values.
    eval_cell(values, x, ufc_cell)
        Assigns values to the cells of the mesh.
    value_shape()
        Returns the shape of the data (scalar, vector, or tensor).

    Notes
    -----
    This class handles the flattening of scalar, vector, and tensor data up to 4th order for assignment to mesh cells.
    Flattening is performed in NumPy's 'C' order (row-major order), meaning that the last index changes the fastest.

    It is crucial for users to understand how the input data is flattened:

    - Scalars: No flattening is needed. E.g., a scalar value 10 remains 10.

    - Vectors: Flattened straightforwardly.
      E.g., a vector [a, b, c] is flattened to [a, b, c].

    - 2D Tensors (Matrices): Flattened row-wise.
      E.g., a tensor [[a, b], [c, d]] is flattened to [a, b, c, d].

    - 3D Tensors: Flattened in order of the last dimension first.
      E.g., a tensor [[[a, b], [c, d]], [[e, f], [g, h]]] is flattened to [a, b, c, d, e, f, g, h].

    - 4D Tensors: Similarly, flattened considering the last dimension first.
      E.g., a tensor of shape (2, 2, 2, 2) with sequential elements from 1 to 16
      will be flattened as [1, 2, 3, 4, ..., 16].

    Ensure that the input data is organized considering this flattening method. This understanding is crucial when
    defining values for each subdomain to ensure they are correctly assigned in the FEniCS framework.
    """

    def __init__(self, cdata, values, **kwargs):
        """ Checks and sets the shape of the values. """
        sample_value = next(iter(values.values()))
        if np.isscalar(sample_value):
            self._value_shape = ()
        else:
            self._value_shape = np.shape(sample_value)

        super().__init__(**kwargs)
        self.cdata = cdata
        self.values = values

    def eval_cell(self, values, x, ufc_cell):
        """ Assigns values to the cells of the mesh. """
        idx = ufc_cell.index
        cell_value = self.values[self.cdata[idx]]

        if self._value_shape == ():
            values[0] = cell_value
        else:
            # Flatten the tensor/vector data to assign to 'values'
            flattened_values = np.ravel(cell_value)
            for i in range(len(flattened_values)):
                values[i] = flattened_values[i]

    def value_shape(self):
        """ Returns the shape of the data (scalar, vector, or tensor). """
        return self._value_shape
