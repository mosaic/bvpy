#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.utils.visu_pyvista
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
import numpy as np
import logging

import pyvista as pv
from pyvista import CellType

from bvpy.bvp import BVP
from bvpy.domains.abstract import AbstractDomain
from bvpy.utils.post_processing import SolutionExplorer

def _visu_feMesh(mesh: fe.Mesh, plotter: pv.Plotter = None, show_plot: bool = True, show_ruler: bool = True,
                 **kwargs) -> pv.Plotter:
    """ This function will display a FeniCS Mesh.

    Parameters
    ----------
    mesh
        The FeniCS Mesh
    plotter
        A pyvista plotter object. If None (default), a plotter is created.
    show_plot
        Show (default) or not the returned mesh.
        If using _plot_mesh as subplot, set show = false until you have built all your subplots.
    show_ruler
        Show (default) or not the ruler.

    Other Parameters
    ----------------
    return_actor
        Return the actor corresponding to the mesh (with the plotter)
    kwargs
        Any parameters that can be passed to pv.Plotter.add_mesh.
        See https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.add_mesh.html

    Returns
    -------
    plotter
        A pyvista plotter object with the added FeniCS mesh

    """

    assert isinstance(mesh, fe.Mesh)

    if plotter is None:
        plotter = pv.Plotter()

    assert isinstance(plotter, pv.Plotter)

    # - Get the coordinates of the vertices and indices of the cells
    points = _check_is3D(mesh.coordinates())
    cells = mesh.cells()

    # - Format the cells for pyvista : [[nIndices, ind0, ind1, ..., indN], [nIndices, ind0, ind1, ..., indN], ...]
    cells = np.hstack([cells.shape[1] * np.ones((cells.shape[0], 1), dtype=int), cells])

    # - Specify the cell type (so pyvista knows how to create the faces from the vertices)
    if mesh.cell_name() == 'interval':
        cell_type = CellType.LINE
    elif mesh.cell_name() == 'triangle':
        cell_type = CellType.TRIANGLE
    elif mesh.cell_name() == 'tetrahedron':
        cell_type = CellType.TETRA
    else:
        logging.error('unknown cell type for FeniCS Mesh')
        return 0

    # - Create an unstructured grid
    mesh = pv.UnstructuredGrid(cells.ravel(),
                               np.full(cells.shape[0], cell_type, dtype=np.uint8),
                               points)

    # - Add the unstructured grid to the pv.Plotter
    plotter.add_mesh(mesh, **kwargs) # add a specific name in case we need to find this actor later

    # - Add ruler
    if show_ruler:
        x_ruler = plotter.add_ruler(pointa=[mesh.bounds[0], mesh.bounds[2] - 0.1, 0.0],
                         pointb=[mesh.bounds[1], mesh.bounds[2] - 0.1, 0.0],
                         title="X Axis", number_labels=5)
        x_ruler.SetAdjustLabels(0)
        y_ruler = plotter.add_ruler(pointa=[mesh.bounds[0] - 0.1, mesh.bounds[3], 0.0],
                         pointb=[mesh.bounds[0] - 0.1, mesh.bounds[2], 0.0],
                         flip_range=True, title="Y Axis")
        y_ruler.SetAdjustLabels(0)
        if (mesh.bounds[5] - mesh.bounds[4]) != 0:
            z_ruler = plotter.add_ruler(pointa=[mesh.bounds[0] - 0.1, 0.0, mesh.bounds[4]],
                             pointb=[mesh.bounds[0] - 0.1, 0.0, mesh.bounds[5]],
                             flip_range=True, title="Z Axis")
            z_ruler.SetAdjustLabels(0)

    if show_plot:
        plotter.show()

    return plotter

def _visu_feFunction(mesh_fcn: fe.Function, plotter: pv.Plotter = None, show_plot: bool = True,
                     cmap: str = 'inferno', val_range = 'auto', scalar_bar_title: str = '', show_mesh: bool = True,
                     show_glyph: bool = True, scale: float = 1, show_scalar_bar: bool = True, **kwargs) -> pv.Plotter:
    """ This function will display a FeniCS Function.
        The Function can either describes a scalar or a vector field. Vector field will be displayed as arrows at the
        mesh vertices with their associated norms.

    Parameters
    ----------
    mesh
        The FeniCS Function
    plotter
        A pyvista plotter object. If None (default), a plotter is created.
    show_plot
        Show (default) or not the returned mesh.
        If using _plot_fcn_mesh in subplot, set show = false until you have built all your subplots.
    cmap
        The colormap to use to display the function field.
    val_range : list or str
        The range of values displayed by the scalar bar. If 'auto' (default), will be the [min_values, max_values].
    scalar_bar_title
        The displayed name for the function (scalar bar)
    show_mesh
        If the mesh need to be displayed (default) or not
    show_glyph
        If the glyph (vector field) need to be displayed (default) or not
    scale
        Scale the displayed vector field by a constant (default = 1)
    show_scalar_bar
        Display (default) or not the scalar bar

    Other Parameters
    ----------------
    kwargs
        Any parameters that can be passed to pv.Plotter.add_mesh.
        See https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.add_mesh.html

    Returns
    -------
    plotter
        A pyvista plotter object with the added FeniCS Function


    Notes
    -----
    It is planned to extend this function to display tensor field. Wait for it !

    """

    assert isinstance(mesh_fcn, fe.Function)

    # - If the mesh should not be displayed, change the style of the plot
    if not show_mesh:
        style = 'points'
        render_points_as_spheres = True
        kwargs['opacity'] = 0.0
    else:
        style = 'surface'
        render_points_as_spheres = False

    # - Get the Function field
    if len(mesh_fcn.ufl_shape) == 0: # scalar field
        # - Get scalar values at the vertices
        scalars = mesh_fcn.compute_vertex_values()

        if not scalar_bar_title:
            scalar_bar_title = 'Value (scalar field)'

    elif len(mesh_fcn.ufl_shape) == 1: # vector field
        # - Get norm of the vector at the vertices (scalar value)
        V = mesh_fcn.function_space().split()[0].collapse()
        norm = fe.project(fe.sqrt(fe.inner(mesh_fcn, mesh_fcn)), V)
        scalars = norm.compute_vertex_values()
        #scalars = norm.vector().get_local()

        if not scalar_bar_title:
            scalar_bar_title = 'Amplitude (vector field)'

    else:
        raise ValueError('Tensor field visualization is not yet available...')

    if val_range == 'auto':
        val_range = [scalars.min(), scalars.max()]

    # - Plot the mesh of the FeniCS Function
    scalar_bar_args = kwargs.pop('scalar_bar_args', {})
    show_ruler = kwargs.pop('show_ruler', True)
    plotter = _visu_feMesh(mesh=mesh_fcn.function_space().mesh(), plotter=plotter, show_plot=False,
                           cmap=cmap, style=style, scalars=scalars, show_scalar_bar=False, clim=val_range,
                           show_ruler=show_ruler,
                           render_points_as_spheres=render_points_as_spheres, **kwargs)

    # - Add scalar bar
    if show_scalar_bar:
        scalar_bar_title = _check_scalar_bar_title(plotter, scalar_bar_title)
        plotter.add_scalar_bar(title=scalar_bar_title, mapper=plotter.mapper, **scalar_bar_args)

    if (len(mesh_fcn.ufl_shape) == 1) and show_glyph:
        mesh = plotter.mesh # find the created mesh

        # - Set vectors in the mesh
        func = SolutionExplorer(mesh_fcn)
        mesh.point_data.set_vectors(_check_is3D(func.to_vertex_values()), 'vector')
        mesh.set_active_vectors('vector')

        # - Plot vectors
        arrows_grid = mesh.glyph(factor=scale)
        plotter.add_mesh(arrows_grid, cmap=cmap, show_scalar_bar=False, clim=val_range)

    if show_plot:
        plotter.show()

    return plotter

def _visu_feFunctionSizet(mesh_szt: fe.cpp.mesh.MeshFunctionSizet, dict_values: dict = None, plotter: pv.Plotter = None,
                          show_plot: bool = True, cmap: str = 'Paired', scalar_bar_title: str = '',
                          show_scalar_bar: bool = True, **kwargs) -> pv.Plotter:
    """ This function will display a FeniCS MeshFunctionSizet.
        In FeniCS, MeshFunctionSizet can be used for marking subdomains or boolean markers for mesh refinement.

    Parameters
    ----------
    mesh_szt
        The FeniCS MeshFunctionSizet
    dict_values
        A dictionnary used to map the label in MeshFunctionSizet into any other type of data (numeric or string).
    plotter
        A pyvista plotter object. If None (default), a plotter is created.
    show_plot
        Show (default) or not the returned mesh.
        If using _plot_mesh_sizet in subplot, set show = false until you have built all your subplots.
    cmap
        The colormap to use to display the function field.
    scalar_bar_title
        The displayed name for the sizet (scalar bar)
    show_scalar_bar
        Display (default) or not the scalar bar

    Other Parameters
    ----------------
    kwargs
        Any parameters that can be passed to pv.Plotter.add_mesh.
        See https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.add_mesh.html

    Returns
    -------
    plotter
        A pyvista plotter object with the added FeniCS MeshFunctionSizet

    """

    assert isinstance(mesh_szt, fe.cpp.mesh.MeshFunctionSizet)

    # - Get the array of sizet data
    if dict_values is None:
        scalars = mesh_szt.array()
    else:
        scalars = np.array([dict_values[ele] for ele in mesh_szt.array()])

    # # - Extract the mesh or the boundary mesh
    # CELLTYPE2DIM = {'interval': 1, 'triangle': 2, 'tetrahedron': 3}
    #
    # ufl_cell = str(mesh_szt.mesh().ufl_cell())
    #
    # if ufl_cell not in CELLTYPE2DIM:
    #     return ValueError(f"Unknown cell type. Valid type are {list(CELLTYPE2DIM.keys())}")
    #
    # mesh_dim = CELLTYPE2DIM[ufl_cell]
    # szt_dim = mesh_szt.dim()
    #
    # if mesh_dim == szt_dim:
    #     mesh = mesh_szt.mesh()
    # elif mesh_dim - 1 == szt_dim:
    #     mesh = fe.BoundaryMesh(mesh_szt.mesh(), 'exterior')
    # else:
    #     return ValueError(f"Unknown topological dimension (not the mesh, nor its boundaries)")

    # - Plot the mesh of the FeniCS MeshFunctionSizet
    mesh = mesh_szt.mesh()
    scalar_bar_args = kwargs.pop('scalar_bar_args', {})
    show_ruler = kwargs.pop('show_ruler', True)
    plotter = _visu_feMesh(mesh=mesh, plotter=plotter, show_plot=False, show_ruler=show_ruler,
                           cmap=cmap, scalars=scalars.astype('str'), show_scalar_bar=False,
                           categories=True, **kwargs) # note that we use categorical labels here

    # - Add scalar bar
    if show_scalar_bar:
        scalar_bar_title = _check_scalar_bar_title(plotter, scalar_bar_title)
        label_font_size = scalar_bar_args.pop('label_font_size', 12)
        plotter.add_scalar_bar(title=scalar_bar_title, mapper=plotter.mapper,
                               n_labels=0, label_font_size=label_font_size, **scalar_bar_args) # set n_labels to 0 to show only categorical labels

    if show_plot:
        plotter.show()

    return plotter

def _visu_feDiriclet(diri, val_range = 'auto', cmap: str = 'inferno', plotter: pv.Plotter = None,
                     show_plot: bool = True, scalar_bar_title: str = '', show_mesh: bool = True,
                     scale: float = 1, show_scalar_bar: bool = True, **kwargs) -> pv.Plotter:
    """ This function will convert a FeniCS DirichletBC into a Pyvista PoinSet.
        The DirichletBC can be either scalar or vector values (if normal vector at the frontier of the domain).

    Parameters
    ----------
    diri : fe.DiricletBC or list of fe.DiricletBC
        The FeniCS DirichletBC
    val_range : list or str
        The range of values displayed by the scalar bar. If 'auto' (default), will be the [min_values, max_values].
    plotter
        A pyvista plotter object. If None (default), a plotter is created.
    show_plot
        Show (default) or not the returned mesh.
        If using _plot_bc_diriclet in subplot, set show = false until you have built all your subplots.
    cmap
        The colormap to use to display the function field.
    scalar_bar_title
        The displayed name for the function (scalar bar)
    show_mesh
        If the mesh need to be displayed (default) or not
    scale
        Scale the displayed vector field by a constant (default = 1)
    show_scalar_bar
        Display (default) or not the scalar bar

    Other Parameters
    ----------------
    kwargs
        Any parameters that can be passed to pv.Plotter.add_mesh.
        See https://docs.pyvista.org/version/stable/api/plotting/_autosummary/pyvista.Plotter.add_mesh.html

    Returns
    -------
    plotter
        A pyvista plotter object with the added FeniCS Function


    Notes
    -----
    TODO: Could be extend to other boundary conditions.

    """
    assert isinstance(diri, fe.DirichletBC) or np.all([isinstance(ele, fe.DirichletBC) for ele in diri])

    if not isinstance(diri, list):
        diri = [diri]

    # - Display the mesh if needed
    if show_mesh:
        show_ruler = kwargs.pop('show_ruler', True)
        plotter = _visu_feMesh(diri[0].function_space().mesh(), plotter=plotter, show_plot=False,
                               show_ruler=show_ruler, style='wireframe')
    elif plotter is None:
        plotter = pv.Plotter()  # create a plotter if needed

    assert isinstance(plotter, pv.Plotter)

    # - Define a sub-routine to extract data to plot from DiricletBC object
    def _get_diri_data(diri_obj):
        # - Plot the boundary conditions
        if len(diri_obj.function_space().component()) > 0:
            # - subspace
            dofs = diri_obj.function_space().collapse()[0].tabulate_dof_coordinates()
        else:
            dofs = diri_obj.function_space().tabulate_dof_coordinates() # get the position of the bc data
        _, idx, count = np.unique(dofs, axis=0, return_index=True, return_counts=True) # check if duplicates (i.e. if vector)

        L = count[0] # number of times the same localisation is used ie. if scalar (L=1) or vector (L>1 & L<4, in R^3)

        if L == 1:  # scalar values
            values = diri_obj.get_boundary_values()

            if len(diri_obj.function_space().component()) > 0:
                coords_diri = np.array([dofs[int(i / 2)] for i in values.keys()])
            else:
                coords_diri = np.array([dofs[i] for i in values.keys()])
            scalars = np.array(list(values.values()))

            return pv.PointSet(_check_is3D(coords_diri)), scalars

        elif (L > 1) and (L < 4):  # vector values
            # - reorganize the values
            idx_v = np.array(list(diri_obj.get_boundary_values().keys()))  # indices of the vertices (for coordinates)
            val_v = np.array(list(diri_obj.get_boundary_values().values()))  # values at the vertices (bc conditions)
            k = np.argsort(idx_v)

            # sort and reshape the values at the vertices (ncol = n-dimension vector)
            sorted_val_dof = val_v[k].reshape(int(len(val_v) / L), L)

            # extract only one dofs coordinates for each boundary value vector value (each dofs coordinates appear N times
            # for n-dimension vector)
            sorted_dof_coords = dofs[np.sort(list(set(idx) & set(idx_v)))]

            # - plot bc conditions as points (with norm of vector)
            scalars = np.linalg.norm(sorted_val_dof, axis=1)

            # - create arrows glyph
            poly = pv.PointSet(_check_is3D(sorted_dof_coords)).cast_to_polydata()  # need to convert to poydata to handle arrows properly
            poly.point_data.set_vectors(_check_is3D(sorted_val_dof), name='bc_conditions')
            poly.set_active_vectors('bc_conditions')
            arrows = poly.glyph(factor=scale)

            return pv.PointSet(_check_is3D(sorted_dof_coords)), scalars, arrows

        else:
            raise ValueError('Unknown boundary conditions (maybe tensor ?)')

    pv_vista_data = []
    for diri_obj in diri:
        try:
            # - Call the sub-routines and assign outputs
            outputs = _get_diri_data(diri_obj)
            pv_vista_data.append(outputs)
        except:
            raise Warning('Some boundary conditions can not be displayed, this can be due to usage of'
                          ' subspace for dirichlet conditions.')

    # - Define the val_range
    if val_range == 'auto':
        all_scalars = np.concatenate([ele[1] for ele in pv_vista_data]) # grab all the scalars
        val_range = [all_scalars.min(), all_scalars.max()]

    # - Now we have the val_range, show the plot
    isFirst = True
    for data in pv_vista_data:
        # - Add the PointSet
        plotter.add_mesh(data[0], scalars=data[1], cmap=cmap, show_scalar_bar=False,
                         render_points_as_spheres=True, clim=val_range, **kwargs)

        # - Add scalar bar (before adding arrows)
        if show_scalar_bar & isFirst:
            if not scalar_bar_title:
                if len(data) == 2:
                    scalar_bar_title = 'Value (scalar field)'
                else:
                    scalar_bar_title = 'Amplitude (vector field)'

            scalar_bar_title = _check_scalar_bar_title(plotter, scalar_bar_title)
            plotter.add_scalar_bar(title=scalar_bar_title, mapper=plotter.mapper, label_font_size=12)
            isFirst = False # avoid to replot scalar bar twice or more

        # - Add arrows if vector field
        if len(data) > 2:
            plotter.add_mesh(data[2], cmap=cmap, show_scalar_bar=False, clim=val_range, **kwargs)

    if show_plot:
        plotter.show()

    return plotter

def _check_is3D(data_array: np.ndarray) -> np.ndarray:
    """ Assert that the data_array is a [N x 3] np.ndarray, else fill the missing columns

    Parameters
    ----------
    data_array
        The input array [N x M] where M corresponds to the spatial component (X, Y, Z) of each vectors

    Returns
    -------
        The output array [N x 3]

    """
    # - Assert that the input array is a numpy array
    if isinstance(data_array, list):
        data_array = np.array(data_array)

    assert isinstance(data_array, np.ndarray) and len(data_array.shape) == 2, 'input array should be a 2D numpy array'

    # - Assert that the input array has 3 columns or less
    assert (0 < data_array.shape[1]) and (data_array.shape[1] < 4), 'input array need to have 1, 2 or 3 dimensions'

    # - If input array has less than 3 columns, fill with column of 0
    if data_array.shape[1] != 3:
        return np.hstack([data_array, np.zeros((data_array.shape[0], 3-data_array.shape[1]))])
    else:
        return data_array

def _check_scalar_bar_title(plotter: pv.Plotter , scalar_bar_title: str) -> str:
    """ Check scalar bar title prior adding the scalar bar to the pyvista plotter.
    # - Assert that no other scalar bar actor are already stored in the plotter with the same title !
    #   "The problem is that scalars for a scalar bar are stored in a dict, with the scalar bar title as the key.
    #   When you don't set a scalar bar title yourself, the default of '' is used, see Plotter.add_scalar_bar().
    #   Since both datasets share the same empty string as key, the latter overwrites the former."
    # => see : https://stackoverflow.com/questions/70007247/multi-window-plot-with-pyvista-are-wrongly-sharing-the-colorlevels

    Parameters
    ----------
    plotter
    scalar_bar_title

    Returns
    -------
    """

    # change the title by adding trailing space before or after the name (will keep the title centered)
    i = 0
    while scalar_bar_title in plotter.scalar_bars:
        if i % 2:
            scalar_bar_title = scalar_bar_title + " "
        else:
            scalar_bar_title = " " + scalar_bar_title
        i += 1

    return scalar_bar_title

def visualize(obj, visu_type:str = 'mesh', **kwargs) -> pv.Plotter:
    """ This function can display various FeniCS objects:
            - fe.Mesh               -> usually the domain geometry
            - fe.Function           -> any fields resulting from simulation (scalar or vector)
            - fe.MeshFunctionSizet  -> subdomains that have been labelled
            - fe.DiricletBC         -> dirichlet boundary conditions

        Note that it is only possible to display triangular or tetrahedrical domains.

    Parameters
    ----------
    obj
        Either a BVP object, an AbstractDomain or a FeniCS object to display
        Fenics object can be fe.Mesh, fe.Function, fe.MeshFunctionSizet or fe.DiricletBC
    visu_type : str
        Defines the information to display if `obj` is a BVP or AbstractDomain object.
        Can be either {'mesh, 'domain', 'boundary', 'solution', 'dirichlet'} or a string corresponding to a
        parameter defined in the vforms.

    Other Parameters
    ----------------
    cmap : str
        The colormap used to display fe.Function or fe.DiricletBC (scalar or vector).
    val_range : list of [float, float]
        Define the range of values used by the colormap. If None, val_range = [min_values, max_values] (default)
    plotter : pv.Plotter
        A pyvista plotter object. If None (default), a plotter is created. Can be used to create subplots.
    title: str
        If provided (default is empty), add this string of characters as title
    scalar_bar_title: str
        If provided, add this string of characters as title of the scalar bar
    scale : float
        Multiply the displayed vector field by this constant (default = 1)
    background : (int, int, int)
        The background of the pyvista plotter. Default is (242, 242, 242)
    window_size
        The size of the pyvista plotter window. Default is [1000, 600].
    show_edges : bool
        Show (default) or not the edges of the displayed mesh
    show_grid : bool
        Show or not (default) the widget grid
    show_scalar_bar : bool
        Show (default) or not the scalar bar
    show_ruler : bool
        Show (default) or not the ruler
    show_plot : bool
        Show (default) or not the returned mesh.
        If using pv_plot() in subplot, set show = false until you have built all your subplots.
    dict_values : dict
        Dictionnary used in feFunctionSizet to convert labels to names.
    plotter_kwargs
        Any parameters that can be passed to pv.Plotter
        See pyvista.Plotter
    actor_kwargs
        Any parameters that can be passed to pv.Plotter.add_mesh
        See pyvista.Plotter.add_mesh

    Returns
    -------
    plotter
        A pyvista plotter object

    Notes
    -----
    It is planned to extend this function to display tensor field. Wait for it !

    """
    # - Get a plotter
    plotter = kwargs.get('plotter', pv.Plotter(window_size=kwargs.get('window_size', [800, 400]),
                                               **kwargs.get('plotter_kwargs', {})))
    assert isinstance(plotter, pv.Plotter)

    title = kwargs.get('title', '')

    # - According to the input type & plot_type, choose the appropriate sub-functions
    if isinstance(obj, BVP):
        if visu_type == 'mesh':
            plotter = _visu_feMesh(obj.domain.mesh, show_plot=False, plotter=plotter,
                                   show_edges=kwargs.get('show_edges', True),
                                   show_ruler=kwargs.get('show_ruler', True),
                                   **kwargs.get('actor_kwargs', {}))

        elif visu_type == 'domain':
            plotter = _visu_feFunctionSizet(obj.domain.cdata, show_plot=False, plotter=plotter,
                                            dict_values=obj.domain.sub_domain_names,
                                            cmap=kwargs.get('cmap', 'Paired'),
                                            scalar_bar_title=kwargs.get('scalar_bar_title', 'Sub-domains'),
                                            show_edges=kwargs.get('show_edges', True),
                                            show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                            show_ruler=kwargs.get('show_ruler', True),
                                            **kwargs.get('actor_kwargs', {}))

        elif visu_type == 'boundary':
            raise ValueError('Not available yet!')
        elif visu_type == 'solution':
            plotter = _visu_feFunction(obj.solution, plotter=plotter, show_plot=False,
                                       val_range=kwargs.get('val_range', 'auto'), cmap=kwargs.get('cmap', 'inferno'),
                                       scale=kwargs.get('scale', 1), show_mesh=kwargs.get('show_mesh', True),
                                       scalar_bar_title=kwargs.get('scalar_bar_title', ''),
                                       show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                       show_ruler=kwargs.get('show_ruler', True),
                                       **kwargs.get('actor_kwargs', {}))

        elif visu_type == 'dirichlet':
            plotter = _visu_feDiriclet(obj.dirichletCondition, show_plot=False, plotter=plotter,
                                       val_range=kwargs.get('val_range', 'auto'),
                                       cmap=kwargs.get('cmap', 'inferno'),
                                       scalar_bar_title=kwargs.get('scalar_bar_title', ''),
                                       show_mesh=kwargs.get('show_mesh', True), scale=kwargs.get('scale', 1),
                                       show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                       show_ruler=kwargs.get('show_ruler', True),
                                       **kwargs.get('actor_kwargs', {}))

        elif visu_type in obj.vform._parameters:
            # try to find a property in the vform that fit the plot_type
            coefficients = obj.vform._parameters[visu_type]
            if isinstance(coefficients.values, dict):
                plotter = _visu_feFunctionSizet(obj.domain.cdata, dict_values=coefficients.values,
                                                plotter=plotter, show_plot=False,
                                                cmap=kwargs.get('cmap', 'Paired'),
                                                scalar_bar_title=kwargs.get('scalar_bar_title', 'Values'),
                                                show_edges=kwargs.get('show_edges', True),
                                                show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                                show_ruler=kwargs.get('show_ruler', True),
                                                **kwargs.get('actor_kwargs', {}))
            elif isinstance(coefficients.values(), np.ndarray) & len(coefficients.values()) == 1:
                dict_values = {sub_domain: coefficients.values()[0] for sub_domain in obj.domain.sub_domain_names.keys()}
                plotter = _visu_feFunctionSizet(obj.domain.cdata, dict_values=dict_values,
                                                plotter=plotter, show_plot=False,
                                                cmap=kwargs.get('cmap', 'Paired'),
                                                scalar_bar_title=kwargs.get('scalar_bar_title', 'Values'),
                                                show_edges=kwargs.get('show_edges', True),
                                                show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                                show_ruler=kwargs.get('show_ruler', True),
                                                **kwargs.get('actor_kwargs', {}))
            else:
                raise RuntimeError('Unknown type of input data')
        else:
            raise ValueError('Unknown plot_type input')

        # - Add a default title if needed
        if not title:
            title = visu_type.title()

    elif isinstance(obj, fe.Mesh):
        plotter = _visu_feMesh(obj, plotter=plotter, show_plot=False,
                               show_edges=kwargs.get('show_edges', True),
                               show_ruler=kwargs.get('show_ruler', True),
                               **kwargs.get('actor_kwargs', {}))

        if not title:
            title = 'Mesh'

    elif isinstance(obj, AbstractDomain) and (visu_type == 'mesh'):
        if obj.mesh is None:
            obj.discretize()

        plotter = _visu_feMesh(obj.mesh, plotter=plotter, show_plot=False,
                               show_edges=kwargs.get('show_edges', True),
                               show_ruler=kwargs.get('show_ruler', True),
                               **kwargs.get('actor_kwargs', {}))

        if not title:
            title = 'Mesh'

    elif isinstance(obj, AbstractDomain) and (visu_type == 'domain'):
        plotter = _visu_feFunctionSizet(obj.cdata, dict_values=obj.sub_domain_names,
                                        show_plot=False, plotter=plotter,
                                        show_edges=kwargs.get('show_edges', True),
                                        cmap=kwargs.get('cmap', 'Paired'),
                                        scalar_bar_title=kwargs.get('scalar_bar_title', 'Sub-domains'),
                                        show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                        show_ruler=kwargs.get('show_ruler', True),
                                        **kwargs.get('actor_kwargs', {}))

        if not title:
            title = 'Domain'

    elif isinstance(obj, AbstractDomain) and (visu_type == 'boundary'):
        raise ValueError('Not available yet!')

    elif isinstance(obj, fe.Function):
        plotter = _visu_feFunction(obj, plotter=plotter, show_plot=False,
                                   val_range=kwargs.get('val_range', 'auto'), cmap=kwargs.get('cmap', 'inferno'),
                                   scale=kwargs.get('scale', 1), show_mesh=kwargs.get('show_mesh', True),
                                   scalar_bar_title=kwargs.get('scalar_bar_title', ''),
                                   show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                   show_ruler=kwargs.get('show_ruler', True),
                                   **kwargs.get('actor_kwargs', {}))

    elif isinstance(obj, fe.cpp.mesh.MeshFunctionSizet):
        plotter = _visu_feFunctionSizet(obj, dict_values=kwargs.get('dict_values', None),
                                        show_plot=False, plotter=plotter,
                                        show_edges=kwargs.get('show_edges', True),
                                        cmap=kwargs.get('cmap', 'Paired'),
                                        scalar_bar_title=kwargs.get('scalar_bar_title', 'Sub-domains'),
                                        show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                        show_ruler=kwargs.get('show_ruler', True),
                                        **kwargs.get('actor_kwargs', {}))

    elif isinstance(obj, fe.DirichletBC) or np.all([isinstance(ele, fe.DirichletBC) for ele in obj]):
        plotter = _visu_feDiriclet(obj, plotter=plotter, show_plot=False,
                                   val_range=kwargs.get('val_range', 'auto'),
                                   cmap=kwargs.get('cmap', 'inferno'), scale=kwargs.get('scale', 1),
                                   scalar_bar_title=kwargs.get('scalar_bar_title', ''),
                                   show_scalar_bar=kwargs.get('show_scalar_bar', True),
                                   show_ruler=kwargs.get('show_ruler', True),
                                   show_mesh=kwargs.get('show_mesh', True), **kwargs.get('actor_kwargs', {}))

        if not title:
            title = 'Dirichlet'

    else:
        return 0

    # - Add a title if needed
    if title:
        plotter.add_title(title, font_size=14)

    # - Set the background
    plotter.set_background(kwargs.get('background', (242, 242, 242)))

    # - Add grid ?
    show_grid = kwargs.get('show_grid', False)
    if show_grid:
        _ = plotter.show_grid()

    # - Display plot if needed
    show_plot = kwargs.get('show_plot', True)
    if show_plot:
        plotter.show()

    return plotter