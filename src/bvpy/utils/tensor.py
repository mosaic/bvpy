#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Olivier Ali <olviier.ali@inria.fr>
#
#       File contributor(s):
#           Olivier Ali <olviier.ali@inria.fr>
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olviier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import numpy as np
import numpy.linalg as lng
import fenics as fe


def applyElementwise(f, T):
    """Apply a function to each component of tensor T.

    Parameters
    ----------
    f : function applied to tensor

    T : UFL Tensor

    Returns
    -------
    UFL Tensor

    """

    shape = T.ufl_shape

    t_array = []
    for i in range(shape[0]):
        t_j = []
        for j in range(shape[1]):
            t_j.append(f(T[i, j]))
        t_array.append(t_j)

    return fe.as_tensor(t_array)


def amplitude(tensor):
    """Measures the amplitude of a tensor.

    We choose to define the amplitude of a tensor as its Frobenius norm divided
    by the square root of its dimension.
    So the identity tensor has always a unit amplitude.

    Parameters
    ----------
    tensor : numpy.nDarray

    Returns
    -------
    float

    """
    dim, _ = tensor.shape

    assert(dim == _)

    return lng.norm(tensor) / np.sqrt(dim)


def sph(tensor):
    """Extract the spherical part of a symmetric tensor.

    Parameters
    ----------
    tensor : numpy.nDarray

    Returns
    -------
    numpy.nDarray
    """

    dim, _ = tensor.shape

    assert(dim == _)

    return np.trace(tensor) * np.eye(dim) / dim


def dev(tensor):
    """Extracts the deviatoric part of a symmetric tensor.

    Parameters
    ----------
    tensor : numpy.nDarray

    Returns
    -------
    numpy.nDarray
    """
    dim, _ = tensor.shape

    assert(dim == _)

    return tensor - sph(tensor)


def intensity(tensor):
    """Measures the intensity of a tensor.

    We chose to define the intensity of a symmetric tensor as the amplitude of
    its spherical part.

    Parameters
    ----------
    tensor : numpy.nDarray

    Returns
    -------
    float

    """
    dim, _ = tensor.shape

    assert(dim == _)

    return amplitude(sph(tensor))


def anisotropy(tensor):
    """Measures the anisotropy degree of a tensor.

    We chose to define the anistropy of a symmetric tensor as the ratio
    between the amplitudes of its deviatoric and spherical parts.

    Parameters
    ----------
    tensor : numpy.nDarray

    Returns
    -------
    float

    """
    dim, _ = tensor.shape

    assert(dim == _)

    try:
        return float(amplitude(dev(tensor))) / float(amplitude(sph(tensor)))

    except ZeroDivisionError:
        print("WARNING: tensor has a null amplitude.")

        return False
