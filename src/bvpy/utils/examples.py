#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.utils.examples
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

from bvpy.domains import Cylinder
from bvpy.boundary_conditions import dirichlet
from bvpy.vforms.elasticity import LinearElasticForm
from bvpy.utils.visu import plot
from bvpy.utils.visu_pyvista import visualize
from bvpy.bvp import BVP

def cantilever_beam(visualizer: str='pyvista'):
    """ Example of a cantilever beam simulation.

    This function models a cantilever beam as a cylindrical domain with clamped boundary conditions at one end and
    a gravitational load applied  along the negative Z-axis. The problem is solved using a variational formulation
    for linear elasticity, and the solution is returned as a visualization of the displacement field.

    Parameters
    ----------
    visualizer : str, optional
        The type of visualizer to use for visualization purposes. Acceptable values are 'pyvista' (default) or
        'plotly'.

    Returns
    -------
    None
        This function visualizes the displacement field of the solved boundary value problem
        either through a Pyvista Plotter or as a Plotly plot depending on the given `visualizer`
        argument.

    Notes
    -----
    The simulation steps include:
    1. Define the domain of the cantilever beam as a cylinder:
        - Length of the cylinder: dx = 2 (unit length).
        - Radius: r = 0.2.
        - Meshing: tetrahedral cells with a size of 0.1.
        - Algorithm: Frontal-Delaunay meshing.
        - The domain is then discretized.

    2. Define boundary conditions:
        - Dirichlet boundary conditions are applied to the end of the cylinder at x=0:
          All translations (displacements) in X, Y, and Z are fixed.

    3. Define the variational formulation:
        - Linear Elasticity Formulation.
        - Material Parameters:
          - Young's modulus = 1.
          - Poisson's ratio = 0.3.
        - Body force:
          - Gravitational acceleration: g = 0.016.
          - Density: ρ = 0.1.
          - Source term: [0, 0, -ρ * g].

    4. Create and solve the boundary-value problem:
        - Combine domain, boundary conditions, and variational form into a BVP instance.
        - Solve the BVP for the cantilever beam.

    5. Visualize the displacement field at the mechanical equilibrium state

    Examples
    --------
    >>> from bvpy.utils.examples import cantilever_beam
    >>> cantilever_beam()
    """
    assert visualizer in ['pyvista', 'plotly'], "Acceptable values are 'pyvista' (default) or 'plotly'."

    # - Create a cylinder and set up some parameters for the mesh construction
    cylinder = Cylinder(x=0, dx=2, dz=0, r=0.2, cell_type='tetra',
                        cell_size=.1, algorithm='Frontal-Delaunay', clear=True)
    cylinder.discretize()  # discretize (from Gmsh -> FeniCS)

    # - Create boundary conditions : clamp the beam at x=0
    bc = [dirichlet(val=[0, 0, 0], boundary="near(x, 0, 1e-2)")]

    # - Create the variational form
    g = 0.016
    rho = .1
    vform = LinearElasticForm(young=1,
                              poisson=0.3,
                              source=[0, 0, -rho * g])

    # - Create the boundary-value problem and solve it
    bvp = BVP(domain=cylinder, vform=vform, bc=bc)
    bvp.solve()

    # - Visualize the resulting displacement field
    if visualizer == 'pyvista':
        visualize(bvp, 'solution')
    elif visualizer == 'plotly':
        visualize(bvp.solution)
