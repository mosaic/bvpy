#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.gbvp
#
#       File author(s):
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import h5py
import logging
import fenics as fe
from collections import OrderedDict

import numpy as np
from ufl.classes import Zero

from bvpy import BVP, MPI_COMM_WORLD, logger

from bvpy.solvers.linear import LinearSolver
from bvpy.solvers.nonlinear import NonLinearSolver
from bvpy.solvers.time_integration import Time
from bvpy.utils.progress_bar import ProgressBar

class GBVP(BVP):
    """
    Class implementing GBVP (Growth-Boundary Value Problem).

    Parameters
    ----------
    domain : subclass of :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`
        Integration domain, described by both a geometry and a mesh.
        (the default is None).
    vform : class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
        The variation form describing the material behavior.
    growth_scheme : subclass of :class:`AbstractGrowth<bvpy.vforms.plasticity.AbstractGrowth>`
        Define the growth law associated with the material behavior.
    bc : list or :class:`ConstantDirichlet<bvpy.boundary_conditions.dirichlet.ConstantDirichlet>`, :class:`VariableDirichlet<bvpy.boundary_conditions.dirichlet.VariableDirichlet>`, :class:`ConstantNeumann<bvpy.boundary_conditions.neumann.ConstantNeumann>`, :class:`VariableNeumann<bvpy.boundary_conditions.neumann.VariableNeumann>`
        Boundary conditions.
    initial_state : type
        Description of parameter `initial_state`.

    Attributes
    ----------
    time : type
        Description of attribute `time`.
    solution : type
        Description of attribute `solution`.
    _progressbar : type
        Description of attribute `_progressbar`.
    _vform : subclass of :class:`MorphoElasticForm<bvpy.vforms.plasticity.MorphoElasticForm>`
        The variation form describing the material behavior.
    solution_steps : type
        Description of attribute `solution_steps`.
    set_initial_solution : type
        Description of attribute `set_initial_solution`.
    growth_scheme : subclass of :class:`AbstractGrowth<bvpy.vforms.plasticity.AbstractGrowth>`
        Define the growth law associated with the material behavior.
    """

    def __init__(self, domain=None, vform=None, bc=None, growth_scheme=None, initial_state=None, initial_growth=None):
        super().__init__(domain=domain, vform=vform, bc=bc)

        self.time = Time()
        self._progressbar = ProgressBar()
        self.solution_steps = OrderedDict()
        self.growth_steps = OrderedDict()

        if initial_state is not None:
            self.set_initial_solution(initial_state)

        self.growth_scheme = growth_scheme
        self.growth_scheme.plastic_vform = self.vform  # attached the plastic vform to the growth scheme
        self.growth_scheme.prev_solution = self.solution

        if initial_growth is None:
            self.growth_scheme.prev_growth = self.vform.F_g
        else:
            self.growth_scheme.prev_growth = initial_growth

    @property
    def _details(self):
        """Sums up the characteristics of the problem.

        Returns
        -------
        str
            A printable description of the problem.

        """

        description = super()._details
        description += '\nGrowth scheme\n-----------------------\n'
        description += f'    * Type: {self.growth_scheme._type}'

        return description

    def run(self, tmin, tmax, dt,
            filename=None, store_steps=False, **kwargs):
        self.set_time(tmin)
        self.growth_scheme.dt = dt

        current_solution = self.solution.copy(deepcopy=True)
        current_growth = self.growth_scheme.get_growth().copy(deepcopy=True)

        if store_steps:
            self.solution_steps[self.time.current] = current_solution
            self.growth_steps[self.time.current] = current_growth

        if filename is not None:
            with fe.HDF5File(MPI_COMM_WORLD, filename, 'w') as file:
                file.write(self.domain.mesh, "mesh")
                #file.write(self.domain.cdata, "cell_labels")
                #file.write(self.domain.bdata, "boundary_labels")

                file.write(current_solution, f'solution_t{self.time.current}')
                file.write(current_growth, f'growth_t{self.time.current}')

            # Add parameters related to the domain : sub_domain / sub_boundary
            # if MPI_COMM_WORLD.rank == 0:  # h5py does not support parallal mode, use only the main process
            #     with h5py.File(filename, 'a') as file:
            #         sub_domain_names = file.create_group("sub_domain_names")
            #         for label, domain in self.domain.sub_domain_names.items():
            #             dtype = h5py.string_dtype(encoding="utf-8")
            #             sub_domain_names.create_dataset(str(label), data=domain, dtype=dtype)
            #
            #         sub_boundary_names = file.create_group("sub_boundary_names")
            #         for label, bound in self.domain.sub_boundary_names.items():
            #             dtype = h5py.string_dtype(encoding="utf-8")
            #             sub_boundary_names.create_dataset(str(label), data=bound, dtype=dtype)

        logger.setLevel(logging.WARNING)
        self._progressbar.set_range(tmin, tmax)

        while tmax - self.time.current >= dt:
            self.step(dt, **kwargs)

            current_solution = self.solution.copy(deepcopy=True)
            current_growth = self.growth_scheme.get_growth().copy(deepcopy=True)

            if store_steps:
                self.solution_steps[self.time.current] = current_solution
                self.growth_steps[self.time.current] = current_growth

            if filename is not None:
                with fe.HDF5File(MPI_COMM_WORLD, filename, 'a') as file:
                    file.write(current_solution, f'solution_t{self.time.current}')
                    file.write(current_growth, f'growth_t{self.time.current}')

            self._progressbar.update(self.time.current)
            self._progressbar.show()

    def set_initial_solution(self, solution):
        self.solution.interpolate(solution)

    def set_time(self, time):
        self.time.current = time

    def solve(self, lhs, rhs, **kwargs):
        if isinstance(rhs, Zero):
            for neumann, domain_id in zip(self.neumannCondition['constant'],
                                          self._neumann_domain_id['constant']):
                lhs -= fe.dot(neumann,
                              self.testFunction) * self.domain.ds(domain_id)
            for neumann, domain_id in zip(self.neumannCondition['variable'],
                                          self._neumann_domain_id['variable']):
                lhs -= fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)

            self.solver = NonLinearSolver()
            self._set_solver_parameter(**kwargs)
            jacobian = fe.derivative(lhs, self.solution, self.trialFunction)
            self.solver.set_problem(self.solution, lhs,
                                    jacobian, self.dirichletCondition,
                                    self._pointsources)
        else:

            for neumann, domain_id in zip(self.neumannCondition['constant'],
                                          self._neumann_domain_id['constant']):
                rhs += fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)
            for neumann, domain_id in zip(self.neumannCondition['variable'],
                                          self._neumann_domain_id['variable']):
                rhs += fe.dot(neumann,
                              self.testFunction)*self.domain.ds(domain_id)

            self.solver = LinearSolver()
            self._set_solver_parameter(**kwargs)
            self.solver.set_problem(self.solution, lhs,
                                    rhs, self.dirichletCondition,
                                    self._pointsources)

        self.solver.solve()

    def step(self, dt, **kwargs):
        # - Update the vform according to the growth scheme
        lhs, rhs = self.growth_scheme.apply_growth(self.trialFunction,
                                                   self.testFunction,
                                                   self.solution)

        # - Solve
        self.solve(lhs, rhs, **kwargs)

        # - Update time
        self.time.step(self.growth_scheme.dt)
        self.growth_scheme.update_time(dt)