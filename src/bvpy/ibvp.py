#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.ibvp
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import logging
import fenics as fe
from collections import OrderedDict
from ufl.classes import Zero

from bvpy import BVP, MPI_COMM_WORLD, logger

from bvpy.solvers.linear import LinearSolver
from bvpy.solvers.nonlinear import NonLinearSolver
from bvpy.solvers.time_integration import Time, ImplicitEuler
from bvpy.utils.progress_bar import ProgressBar


class IBVP(BVP):
    """Class implementing IBVP (Initial-Boundary Value Problem).

    Parameters
    ----------
    domain : subclass of :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`
        Integration domain, described by both a geometry and a mesh.
        (the default is None).
    vform : subclass of :class:`AbstractVform<bvpy.vform.abstract.AbstractVform>`
        Variational form to solve. Encompass the type(s) of Finite Elements
        to consider (the default is None).
    bc : list or :class:`ConstantDirichlet<bvpy.boundary_conditions.dirichlet.ConstantDirichlet>`, :class:`VariableDirichlet<bvpy.boundary_conditions.dirichlet.VariableDirichlet>`, :class:`ConstantNeumann<bvpy.boundary_conditions.neumann.ConstantNeumann>`, :class:`VariableNeumann<bvpy.boundary_conditions.neumann.VariableNeumann>`
        Boundary conditions.
    initial_state : type
        Description of parameter `initial_state`.
    scheme : type
        Boundary conditions in a list

    Attributes
    ----------
    time : type
        Description of attribute `time`.
    previous_solution : type
        Description of attribute `previous_solution`.
    solution : type
        Description of attribute `solution`.
    _progressbar : type
        Description of attribute `_progressbar`.
    solution_steps : type
        Description of attribute `solution_steps`.
    set_initial_solution : type
        Description of attribute `set_initial_solution`.
    scheme

    """

    def __init__(self, domain=None, vform=None, bc=None,
                 initial_state=None, scheme=ImplicitEuler):
        super().__init__(domain=domain, vform=vform, bc=bc)

        self._relative_tolerance = None
        self._absolute_tolerance = None
        self._linear_solver = None
        self.time = Time()
        self.scheme = scheme(vform)
        self.previous_solution = self.solution.copy(deepcopy=True)
        self.scheme.set_previous_solution(self.previous_solution)
        self._progressbar = ProgressBar()
        self.solution_steps = None

        if initial_state is not None:
            self.set_initial_solution(initial_state)

    @property
    def _details(self):
        """Sums up the characteristics of the problem.

        Returns
        -------
        str
            A printable description of the problem.

        """

        description = super()._details
        description += '\nTime integration scheme\n-----------------------\n'
        description += f'    * Type: {self.scheme._type}'

        return description

    def integrate(self, t_i, t_f, dt,
                  filename=None, store_steps=False, **kwargs):
        self.set_time(t_i)
        self.scheme.dt = dt

        self._linear_solver = kwargs.pop('linear_solver', 'mumps')
        self._absolute_tolerance = kwargs.pop('absolute_tolerance', 1e-10)
        self._relative_tolerance = kwargs.pop('relative_tolerance', 1e-10)

        if store_steps:
            self.solution_steps = OrderedDict()
            self.solution_steps[self.time.current] = \
                self.solution.copy(deepcopy=True)

        if filename is not None:
            with fe.XDMFFile(MPI_COMM_WORLD, filename) as file:
                file.parameters['functions_share_mesh'] = True  # functions share the same mesh (reduce file size)
                file.write(self.solution, self.time.current)
            logger.setLevel(logging.WARNING)
            self._progressbar.set_range(t_i, t_f)

            while t_f - self.time.current >= dt:
                self.step(dt, **kwargs)
                with fe.XDMFFile(MPI_COMM_WORLD, filename) as file:
                    file.write(self.solution, self.time.current)
                if store_steps:
                    self.solution_steps[self.time.current] = \
                        self.solution.copy(deepcopy=True)
                self._progressbar.update(self.time.current)
                self._progressbar.show()
        else:
            logger.setLevel(logging.WARNING)
            self._progressbar.set_range(t_i, t_f)

            while t_f - self.time.current >= dt:
                self.step(dt, **kwargs)
                if store_steps:
                    self.solution_steps[self.time.current] = \
                        self.solution.copy(deepcopy=True)
                self._progressbar.update(self.time.current)
                self._progressbar.show()

            self.step(t_f - self.time.current, **kwargs)
            self._progressbar.update(self.time.current)
            self._progressbar.show()

    def set_initial_solution(self, solution):
        self.solution.interpolate(solution)
        self.previous_solution.interpolate(solution)

    def set_time(self, time):
        self.time.current = time

    def solve(self, lhs, rhs, **kwargs):
        if isinstance(rhs, Zero):
            for neumann in self.neumannCondition['constant']:
                lhs -= fe.dot(neumann.apply(self.domain.bdata), self.testFunction) * self.domain.ds(neumann._id)
            for neumann in self.neumannCondition['variable']:
                lhs -= fe.dot(neumann.apply(self.functionSpace, self.domain.bdata), self.testFunction) * self.domain.ds(
                    neumann._id)

            self.solver = NonLinearSolver()
            self._set_solver_parameter(**kwargs)
            jacobian = fe.derivative(lhs, self.solution, self.trialFunction)
            self.solver.set_problem(self.solution, lhs,
                                    jacobian, self.dirichletCondition,
                                    self._pointsources)
        else:

            for neumann in self.neumannCondition['constant']:
                rhs += fe.dot(neumann.apply(self.domain.bdata), self.testFunction) * self.domain.ds(neumann._id)
            for neumann in self.neumannCondition['variable']:
                rhs += fe.dot(neumann.apply(self.functionSpace, self.domain.bdata), self.testFunction) * self.domain.ds(
                    neumann._id)

            self.solver = LinearSolver()
            self._set_solver_parameter(**kwargs)
            self.solver.set_problem(self.solution, lhs,
                                    rhs, self.dirichletCondition,
                                    self._pointsources)

        self.solver.solve()

    def step(self, dt, **kwargs):
        self.scheme.dt = dt
        lhs, rhs = self.scheme.apply(self.trialFunction,
                                     self.testFunction,
                                     self.solution)

        if self.scheme.type == 'implicit':
            self.time.step(self.scheme.dt)
            self.solve(lhs, rhs,
                       linear_solver=self._linear_solver,
                       absolute_tolerance=self._absolute_tolerance,
                       relative_tolerance=self._relative_tolerance,
                       **kwargs)
            self.previous_solution.assign(self.solution)

        elif self.scheme.type == 'explicit':
            self.solve(lhs, rhs, **kwargs)
            self.time.step(self.scheme.dt)
            self.previous_solution.assign(self.solution)
