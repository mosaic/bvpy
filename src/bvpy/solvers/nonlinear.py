#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.solvers.nonlinear
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import logging

import fenics as fe
import time

from bvpy.solvers.parameter import SolverParameter
from bvpy import MPI_COMM_WORLD, logger


class GeneralNonlinearProblem(fe.NonlinearProblem):
    """Base class for general non-linear problem.

    Parameters
    ----------
    _L : :class:`Form<dolfin.cpp.fem.Form>`
        Linear form, rhs of the weak formulation.
    _a : :class:`Form<dolfin.cpp.fem.Form>`
        Bilinear form, lhs of the weak formulation.
    _bc : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
         list with dirichlet boundary conditions.

    """

    def __init__(self, L, a, bc, ps, **kwargs):
        """Creates an instance of a general non-linear problem.

        Parameters
        ----------
        L : :class:`Form<dolfin.cpp.fem.Form>`
            Linear form, rhs of the weak formulation.
        a : :class:`dolfin.cpp.fem.Form<dolfin.cpp.fem.Form>`
            Bilinear form, lhs of the weak formulation.
        bc : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
            list with dirichlet boundary conditions.
        ps : :class:`PointSource<dolfin.cpp.fem.PointSource>`, optional
             point source term (The default is None).

        Returns
        -------
        None

        """
        super().__init__(**kwargs)
        self._L = L
        self._a = a
        self._bc = bc
        self._ps = ps

    def F(self, b, x):
        """Short summary.

        Parameters
        ----------
        b : type
            Description of parameter `b`.
        x : type
            Description of parameter `x`.

        Returns
        -------
        type
            Description of returned object.

        """

        fe.assemble(self._L, tensor=b)
        if self._bc is not None:
            for c in self._bc:
                c.apply(b, x)
        if self._ps is not None:
            for ps in self._ps:
                ps.apply(b)

    def J(self, A, x):
        """Short summary.

        Parameters
        ----------
        A : type
            Description of parameter `A`.
        x : type
            Description of parameter `x`.

        Returns
        -------
        None

        """
        fe.assemble(self._a, tensor=A)
        if self._bc is not None:
            for c in self._bc:
                c.apply(A)


class NonLinearSolver():
    """Class implementing a non-linear solver.

    Attributes
    ----------
    _u : :class:`Function<dolfin.cpp.function.Function>`
        The solution we are looking for.
    _F : :class:`Form<dolfin.cpp.fem.Form>`
        Lhs of the weak formulation.
    _Jac : :class:`Form<dolfin.cpp.fem.Form>`
        Jacobian used in the weak formulation.
    _bnd : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
          list of dirichlet boundary conditions.
    _problem : :class:`GeneralNonlinearProblem<dolfin.cpp.fem.GeneralNonlinearProblem>`
        # TODO : Check that in Fenics
    _solver : type
        The non-linear solve to use.
    _snes : type
        A measure of the accuracy (convergence and error)
        of the performed computation.
    parameters : :class:`SolverParameter<bvpy.solver.parameter>`
        A container of all the parameters of the non-linear solver.
    _type : str
        Name of the fenics non-linear solver to use (the default is 'petsc').

    """

    def __init__(self):
        self._u = None
        self._F = None
        self._Jac = None
        self._bnd = None
        self._problem = None
        self._solver = None
        self._snes = None
        self._ps = None

        self.parameters = SolverParameter()

        self._type = 'petsc'

    def set_problem(self, u, F, Jac, bnd, ps):
        """Set the problem.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The solution we are looking for.
        F : :class:`Form<dolfin.cpp.fem.Form>`
            Description of attribute `F`.
        Jac : :class:`Form<dolfin.cpp.fem.Form>`
            Description of attribute `Jac`.
        bnd : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
              list of dirichlet boundary conditions.
        ps : :class:`PointSource<dolfin.cpp.fem.PointSource>`, optional
             point source term (The default is None).

        """
        self._u = u
        self._bnd = bnd
        self._F = F
        self._Jac = Jac
        self._ps = ps

        if self._type == 'petsc':
            self._problem = GeneralNonlinearProblem(
                self._F, self._Jac, self._bnd, self._ps)
            self._solver = fe.PETScSNESSolver(MPI_COMM_WORLD)

            for key, val in self.parameters._param.items():
                if isinstance(val, dict):
                    for subkey, subval in val.items():
                        try:
                            self._solver.parameters[key][subkey] = subval
                        except:
                            logging.warning(f'The parameter {key} {subkey} was not set!')
                else:
                    try:
                        self._solver.parameters[key] = val
                    except:
                        logging.warning(f'The parameter {key} was not set!')

            #paramters_dict = {}
            #paramters_dict.update(self.parameters['krylov_solver'])
            #paramters_dict['linear_solver'] = self.parameters['linear_solver']
            # TODO :: Check with Flo : is this normal that the NL solver has a 'linear_solver' param ?
            #paramters_dict['preconditioner'] = self.parameters['preconditioner']
            #paramters_dict['line_search'] = self.parameters['line_search']
            #paramters_dict['maximum_iterations'] = self.parameters['maximum_iterations']


            #self._solver.parameters.update(paramters_dict)
            #self._solver.parameters['report'] = monitor
            #self._solver.parameters['krylov_solver'].update(self.parameters['krylov_solver']) # set the krylov parameters
            #self._solver.parameters['absolute_tolerance']

            self._snes = self._solver.snes()
            self._snes.setConvergenceHistory()

    def solve(self):
        """Solve the problem.

        Returns
        -------
        residual : _solver.snes (???)
            A measure of the accuracy (convergence and error)
            of the performed computation.
        """
        if self._type == 'petsc':
            logger.info(' Backend       : petsc')
            logger.info(' Linear Solver : '
                        + str(self._solver.parameters['linear_solver']))
            logger.info(' Preconditioner: '
                        + str(self._solver.parameters['preconditioner']))
            logger.info(f" atol: {self._solver.parameters['absolute_tolerance']}")
            logger.info(f" rtol: {self._solver.parameters['relative_tolerance']}")
            logger.info(' Size          : '
                        + str(self._u.function_space().dim()))
            logger.info(' Solving NonLinearProblem ...')

            start = time.time()
            self._solver.solve(self._problem, self._u.vector())
            end = time.time()

            residuals = self._snes.getConvergenceHistory()[0]

            logger.info(' ... Done [' + str(end - start) + ' s]')
            conv = self._snes.getConvergedReason()
            logger.info(' Iterations    : ' + str(len(residuals)))
            logger.info(' Resiudal      : ' + str(residuals[-1]))

            return residuals