#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       bvpy.solvers.time_integration
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
from ufl.classes import Zero

class Time():
    """Container class for Time.

    Parameters
    ----------
    current : int or float
        Current time.

    Attributes
    ----------
    current

    """
    def __init__(self, current=0):
        self.current = current

    def step(self, dt):
        self.current += dt

class FirstOrderScheme():
    """Generic class for first order time integration scheme.

    Parameters
    ----------
    vform : subclass of :class:`AbstractVform<bvpy.vforms.abstract.AbstractVform>`
        The vform to integrate.

    Attributes
    ----------
    _vform : subclass of :class:`AbstractVform<bvpy.vforms.abstract.AbstractVform>`
    previous_solution : :class:`Function<dolfin.cpp.function.Function>`
        The previous solution.
    dt : int or float
        Integration time-step.
    _type : string
        Type of scheme: 'implicit' or 'explicit'.

    """
    def __init__(self, vform):
        self._vform = None
        self.previous_solution = None
        self.dt = None

        self.set_vform(vform)

        self._type = None

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        assert value in ['implicit', 'explicit']
        self._type = value

    def set_previous_solution(self, previous):
        self.previous_solution = previous

    def set_vform(self, vform):
        self._vform = vform

    @property
    def dx(self):
        return self._vform.dx

    @property
    def ds(self):
        return self._vform.ds


class ImplicitEuler(FirstOrderScheme):
    """Implicit Euler scheme.

    """
    def __init__(self, vform):
        super().__init__(vform)
        self._type = 'implicit'

    def apply(self, u, v, sol):
        lhs, rhs = self._vform.get_form(u, v, sol)
        if isinstance(rhs, Zero):
            dudt = fe.dot((sol-self.previous_solution)
                          / fe.Constant(self.dt), v) * self.dx
            lhs += dudt

        else:
            dudt = fe.dot((u-self.previous_solution)
                          / fe.Constant(self.dt), v) * self.dx

            dudt_lhs, dudt_rhs = fe.lhs(dudt), fe.rhs(dudt)

            lhs += dudt_lhs
            rhs += dudt_rhs

        return lhs, rhs


class ExplicitEuler(FirstOrderScheme):
    """Explicit Euler scheme.

    """
    def __init__(self, vform):
        super().__init__(vform)
        self._type = 'explicit'

    def apply(self, u, v, sol):
        lhs, rhs = self._vform.get_form(self.previous_solution, v, sol)

        F = fe.Constant(self.dt) * lhs + fe.Constant(self.dt) * rhs
        F += u * v * self.dx
        F -= self.previous_solution * v * self.dx

        return fe.lhs(F), fe.rhs(F)
