Welcome
-------

.. image:: https://joss.theoj.org/papers/10.21105/joss.02831/status.svg
   :target: https://doi.org/10.21105/joss.02831

**Bvpy** is a python library, based on `FEniCS <https://fenicsproject.org/>`_ and `GMSH <http://gmsh.info/>`_, to easily implement and study numerically Boundary Value Problems (BVPs) as well as Initial Boundary Value Problems (IBVPs) through the Finite Element Method (FEM).

Initially built up in the context of developmental biology and morphomechanics, **Bvpy** proposes an intuitive API as close as possible to the formal definition of BVPs. Its purpose is to enable users to quickly and efficiently estimate the behavior of a wide variety of fields (scalars, vectors, tensors) on biologically relevant structures (Riemannian and non-Rieamannian manifolds), inspired by biophysical and biochemical processes (morphogene patterning, active matter mechanics, diffusion-reaction processes, active transports...).

Despite this biological motivation, the **Bvpy** library has been implemented in an *agnostic* manner that makes it suitable for many other scientific context.


.. sidebar:: **Bvpy**

  :Lead Development: Manuel Petit

  :Coordination: Olivier Ali

  :Main Contributors: Florian Gacon

  :Other Contributors: Guillaume Cerutti, Adrien Heymans, Jonathan Legrand, Gonzalo Revilla

  :Active team: Inria project team `Mosaic <https://team.inria.fr/mosaic/>`_

  :Institutes: `Inria <http://www.inria.fr>`_

  :Language: Python

  :Supported OS: Linux, MacOS

  :Licence: `LGPL`

  :Funding: Inria ADT Gnomon (Christophe Godin)


Documentation
-------------

The documentation can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/>`_.

A quick introduction to boundary-value problems and Initial-Boundary-value problems, as well as a the general philosophy behind the **Bvpy** library development can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/general_introduction.html>`_.

A detailed description of the main classes and models of the library can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/library_description.html>`_.

Some tutorials explaining basic manipulations are gathered `here <https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.html>`_.


Requirements
------------

* Python 3.9
* FEniCS 2019.1.0
* GMSH 4.11


Installation
------------

The full installation procedure is available `here <https://mosaic.gitlabpages.inria.fr/bvpy/>`_.

To install ``bvpy``, follow these steps:

1. **Using Anaconda** (recommended for most users):

   - Install with a new environment:

     .. code-block:: bash

        conda create -n bvpy -c mosaic -c conda-forge bvpy
        conda activate bvpy

   - Or install in an existing environment:

     .. code-block:: bash

        conda activate <your-environment-name>
        conda install -c mosaic -c conda-forge bvpy

2. **Using Docker**:

   - Pull the Docker image:

     .. code-block:: bash

        docker pull registry.gitlab.inria.fr/mosaic/bvpy:<TAG>

   - Run the container and launch Jupyter Notebook:

     .. code-block:: bash

        docker run -it -p 8888:8888 registry.gitlab.inria.fr/mosaic/bvpy:<TAG>
        jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --port 8888

3. **From Sources**:

   - Clone the repository:

     .. code-block:: bash

        git clone https://gitlab.inria.fr/mosaic/bvpy.git
        cd bvpy
        conda env create --file conda/env.yaml -n bvpy-dev
        conda activate bvpy-dev
        python -m pip install -e .

.. The following content is commented out : no need
    **Important Note:** Bvpy is currently running on *FEniCS legacy* and not *FEniCSx*. If you are using an ARM-based computer (mac M1s, M2s), there is currently no ARM-Friendly conda package of *FEniCS legacy*. But you can still run x86-based packages. to do so type the following:
    .. code-block:: bash
      CONDA_SUBDIR=osx-64 conda create -n bvpy_x86
      conda activate bvpy_x86
      conda config --env --set subdir osx-64
    Once this is done, you can import manually all the required libraries, listed within the `conda/env_osx.yaml` configuration file.

Support
-------

If you encounter an error or need help, please `raise an issue <https://gitlab.com/oali/bvpy/-/issues>`_.

Contributing
------------

Bvpy is a is a collaborative project and contributions are welcome. If you want to contribute, please contact the `coordinator <mailto:olivier.ali@inria.fr>`_ prior to any `merge request <https://gitlab.com/oali/bvpy/-/merge_requests>`_ and
check the `gitlab merge request guidelines <https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html>`_ if needed.

Citation
--------

If you are using Bvpy in a published work, please use this bibtex entry as reference:

.. code-block::

  @article{Gacon2021,
  doi = {10.21105/joss.02831},
  url = {https://doi.org/10.21105/joss.02831},
  year = {2021},
  publisher = {The Open Journal},
  volume = {6},
  number = {59},
  pages = {2831},
  author = {Florian Gacon and Christophe Godin and Olivier Ali},
  title = {BVPy: A FEniCS-based Python package to ease the expression and study of boundary value problems in Biology.},
  journal = {Journal of Open Source Software}}
