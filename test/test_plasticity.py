import unittest

from bvpy import BVP, logger
from bvpy.domains import Rectangle, CustomDomain
from bvpy.vforms.plasticity import MorphoElasticForm
from bvpy.vforms.elasticity import StVenantKirchoffPotential
from bvpy.boundary_conditions import dirichlet
from bvpy.utils.pre_processing import create_expression
import tempfile
import fenics as fe
import numpy as np


class TestLinearElasticityProblem(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.L = 10
        self.W = 1

        mesh = fe.RectangleMesh(fe.Point(0, 0), fe.Point(self.L, self.W),
                                100, 10, "crossed")
        self.domain = CustomDomain(mesh.coordinates(), mesh.cells(), dim=2)

        self.rho_g = 1e-2
        young = 1e5
        self.theo_def_body = 3 * self.rho_g * self.L**4 / 2 / young / self.W**3

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_plastic(self):
        mdl = StVenantKirchoffPotential(young=1e5, poisson=.3)
        vform = MorphoElasticForm(potential_energy=mdl,
                                  F_g=np.diag([1, 1]),
                                  plane_stress=True,
                                  source=[0, -self.rho_g])

        vform.F_g = np.diag([1, 1])

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='lu',
                  absolute_tolerance=1e-9)

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)
