import unittest

import numpy as np

from bvpy import BVP, logger
from bvpy.domains import CustomDomain
from bvpy.vforms.elasticity import LinearElasticForm, HyperElasticForm
from bvpy.vforms.elasticity import NeoHookeanPotential, StVenantKirchoffPotential, QuadraticGreenLagrangePotential
from bvpy.boundary_conditions import dirichlet
import fenics as fe


class TestLinearElasticityProblem(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.L = 10
        self.W = 1

        mesh = fe.RectangleMesh(fe.Point(0, 0), fe.Point(self.L, self.W),
                                100, 10, "crossed")
        self.domain = CustomDomain(mesh.coordinates(), mesh.cells(), dim=2)

        dummy_mesh = fe.UnitSquareMesh(1, 1)
        self.dummy_V = fe.VectorFunctionSpace(dummy_mesh, "P", 1)
        self.dummy_T = fe.TensorFunctionSpace(dummy_mesh, "DG", 0)

        self.rho_g = 1e-2
        young = 1e5
        self.theo_def_body = 3 * self.rho_g * self.L**4 / 2 / young / self.W**3

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_elasticity_methods(self):
        mdl = StVenantKirchoffPotential()
        vform = HyperElasticForm(potential_energy=mdl)
        dummy_solution = fe.Function(self.dummy_V)
        dummy_solution.assign(fe.Constant((1, 0)))

        # Assign different values to some DOFs (to create non-null strain)
        vector = dummy_solution.vector()
        vector[0] = 0.5

        # strain
        F = vform.grad_deformation(dummy_solution, 'eulerian')
        assert np.allclose(fe.project(F, self.dummy_T).compute_vertex_values(), [0.667, 1.000, 0.667, 0.667,
                                                                                 0.333, 0.000, 0.333, 0.333,
                                                                                 0.000, 0.000, 0.000, 0.000,
                                                                                 1.000, 1.000, 1.000, 1.000], atol=1e-3)

        for types in ['biot', 'almansi', 'natural', 'other']:
            strain = vform.get_strain(dummy_solution, strain_type=types) # TODO: need values to compare

        # stress
        for types in ['PK1', 'PK2', 'cauchy']:
            stress = vform.get_stress(dummy_solution, stress_type=types) # TODO: need values to compare

        vform.poisson = 0.5
        self.assertEqual(vform.get_parameter('poisson')(0), 0.5)

        vform.young = 1e5
        self.assertEqual(vform.get_parameter('young')(0), 1e5)

        vform.plane_stress = True
        self.assertEqual(vform.plane_stress, True)

    def test_lame_coefficient(self):
        vform = LinearElasticForm(source=[0, -self.rho_g],
                                 young=1e5,
                                 poisson=.3,
                                 plane_stress=False) # set plane_stress to False
        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)

    def test_linear(self):
        vform = LinearElasticForm(source=[0, -self.rho_g],
                                 young=1e5,
                                 poisson=.3,
                                 plane_stress=True)

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(krylov_solver={'absolute_tolerance':1e-10}) # assert that we can set absolute_tolerance for the krylov_solver
        bvp.solve(absolute_tolerance=1e-10) # assert that the simplest way is also valid (autoset krylov parameter)

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

        # - create deformed mesh
        mesh = fe.RectangleMesh(fe.Point(0, 0), fe.Point(self.L, self.W), 100, 10, "crossed")
        domain = CustomDomain(mesh.coordinates(), mesh.cells(), dim=2)
        domain.move(bvp.solution)

    def test_linear_hyper(self):
        mdl = StVenantKirchoffPotential(young=1e5, poisson=.3)
        vform = HyperElasticForm(potential_energy=mdl,
                                 source=[0, -self.rho_g],
                                 small_displacement=True,
                                 plane_stress=True)

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='mumps') # assert that we can set absolute_tolerance for the krylov_solver

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

    def test_hyper(self):
        mdl = StVenantKirchoffPotential(young=1e5, poisson=.3)
        vform = HyperElasticForm(potential_energy=mdl,
                                 source=[0, -self.rho_g],
                                 plane_stress=True)

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='superlu') # try another linear solver

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

    def test_hyper_neo_hookean(self):
        mdl = NeoHookeanPotential(young=1e5, poisson=.3)
        vform = HyperElasticForm(potential_energy=mdl,
                                 source=[0, -self.rho_g],
                                 plane_stress=True)

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='mumps', absolute_tolerance=1e-7) # try another linear solver and update absolute_tol

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

    def test_quadratic(self):
        mdl = QuadraticGreenLagrangePotential()
        mdl.fill_orthotropic(young = [1e5, 1e5], poisson=.3, shear=.5)
        mdl.fill_isotropic(young=1e5, poisson=.3, dim=3)
        mdl.fill_isotropic(young=1e5, poisson=.3, dim=2, plane_stress=False)
        mdl.fill_isotropic(young=1e5, poisson=.3, dim=2)
        vform = HyperElasticForm(potential_energy=mdl,
                                 source=[0, -self.rho_g],
                                 plane_stress=True)


        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='mumps', absolute_tolerance=1e-7) # try another linear solver and update absolute_tol

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)
