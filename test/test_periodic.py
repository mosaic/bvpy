import unittest
import fenics as fe
import numpy as np
from bvpy.domains import Rectangle
from bvpy.boundary_conditions.periodic import SquarePeriodicBoundary, LinearPeriodicBoundary
from bvpy.vforms.elasticity import LinearElasticForm
from bvpy.bvp import BVP


class TestPeriodic(unittest.TestCase):
    def setUp(self):
        self.dom = Rectangle(length=1, width=1, dim=2, cell_size=0.5)
        self.dom.discretize()
        self.vform = LinearElasticForm(source=[0, 0])

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_square_periodic(self):
        periodic_bc = SquarePeriodicBoundary(length=1, width=1)
        x = [1, 1]
        y = [1, 0]
        periodic_bc.map(x, y)

        pb = BVP(domain =self.dom, vform=self.vform, bc=periodic_bc)
        pb.solve()


    def test_linear_periodic(self):
        # direction x
        periodic_bc = LinearPeriodicBoundary()
        x = [1, 1]
        y = [1, 0]
        periodic_bc.map(x, y)

        pb = BVP(domain=self.dom, vform=self.vform, bc=periodic_bc)
        pb.solve()

        # direction y
        periodic_bc = LinearPeriodicBoundary(direction='y')
        x = [1, 1]
        y = [1, 0]
        periodic_bc.map(x, y)

        pb = BVP(domain=self.dom, vform=self.vform, bc=periodic_bc)
        pb.solve()
