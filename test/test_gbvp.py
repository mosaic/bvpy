import unittest
import tempfile
import numpy as np

from bvpy.gbvp import GBVP
from bvpy.domains import Rectangle
from bvpy.vforms.elasticity import StVenantKirchoffPotential
from bvpy.vforms.plasticity import MorphoElasticForm
from bvpy.vforms.plasticity import ConstantGrowth
from bvpy.boundary_conditions import dirichlet
from bvpy.utils.pre_processing import create_expression

class TestGBVP(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        self.domain = Rectangle(x=-2, y=-2, length=4, width=4, cell_size=0.05, dim=2)
        self.bc = dirichlet(val=[0, 0], boundary="near(x, -2)")

        potential_energy = StVenantKirchoffPotential()
        self.vform = MorphoElasticForm(potential_energy=potential_energy, F_g=np.identity(2), source=[0, 0])
        self.growth_scheme = ConstantGrowth(dg = np.array([[0.1, 0], [0, 0]]))
        self.dir = tempfile.TemporaryDirectory()

    def tearDown(self):
        """Concludes and closes the test.
        """
        self.dir.cleanup()

    def test_constructor(self):
        gbvp = GBVP(self.domain, self.vform, self.bc, self.growth_scheme)
        self.assertIsNotNone(gbvp.domain.mesh)
        self.assertIsNotNone(gbvp.growth_scheme)

    def test_run_with_file(self):
        gbvp = GBVP(self.domain, self.vform, self.bc, growth_scheme=self.growth_scheme)
        gbvp.info()
        gbvp.run(0, 0.4, 0.2, filename=self.dir.name+'/solution.h5', linear_solver='mumps')

    def test_run_without_file(self):
        ibvp = GBVP(self.domain, self.vform, self.bc, growth_scheme=self.growth_scheme)
        sol_0 = create_expression(('1','1'), degree=2)
        ibvp.set_initial_solution(sol_0)
        ibvp.run(0, 0.4, 0.2, linear_solver='mumps')
