import numpy as np
import fenics as fe
import pytest
from bvpy.vforms.plasticity import MorphoElasticForm
from bvpy.vforms.plasticity import TimeDependentGrowth, ConstantGrowth, StrainDependentGrowth, HeterogeneousGrowth
from bvpy.vforms.elasticity import StVenantKirchoffPotential

@pytest.fixture
def constant_term():
    # Example constant term for the constant growth model
    return np.array([[0.1, 0.0], [0.0, 0.1]])

@pytest.fixture
def growth_func():
    # Given a function that returns a 2x2 array
    def _growth_func(t, a, b):
        dg = np.zeros((2, 2))
        dg[0, 0] = b * np.sin(t * np.pi) + a
        return dg

    return _growth_func

@pytest.fixture
def fenics_setup():
    # Create a simple mesh and function space in FEniCS for testing
    mesh = fe.UnitSquareMesh(1, 1)
    V = fe.VectorFunctionSpace(mesh, 'P', 1)
    return mesh, V

@pytest.fixture
def previous_solution(fenics_setup):
    # Create a mock function representing the previous solution
    _, V = fenics_setup
    prev_sol = fe.Function(V)
    prev_sol.assign(fe.Constant([1.0, 1.0]))  # Assign a constant value for simplicity
    vector = prev_sol.vector()

    # Assign different values to some DOFs (to create non-null strain)
    dof_indices = [0, 1, 2, 3, 4]
    new_values = [0.5, 0.8, 1.5, 1.2, 0.9]

    for i, value in zip(dof_indices, new_values):
        vector[i] = value

    return prev_sol

@pytest.fixture
def plastic_vform():
    potential_energy = StVenantKirchoffPotential()
    return MorphoElasticForm(potential_energy=potential_energy, F_g=np.eye(2), source = [0, 0])

@pytest.fixture
def growth_time_model(growth_func):
    a = 1.0
    b = 0.5
    return TimeDependentGrowth(growth_func, a, b)

@pytest.fixture
def growth_constant_model(constant_term):
    return ConstantGrowth(constant_term)

@pytest.fixture
def growth_strain_model(target_strain, extensibility, previous_solution):
    model = StrainDependentGrowth(target_strain=target_strain, extensibility=extensibility)
    model.prev_solution = previous_solution
    return model

@pytest.fixture
def target_strain():
    # Example target strain tensor for the strain-dependent growth model
    return np.array([[0.1, 0.0], [0.0, 0.1]])


@pytest.fixture
def extensibility():
    # Example extensibility factor for the strain-dependent growth model
    return 0.5

@pytest.fixture
def growth_heterogeneous_model(growth_constant_model, growth_strain_model, previous_solution):
    values = [1, 2, 1, 2]
    mesh = previous_solution.function_space().mesh()
    cdata = fe.MeshFunction("size_t", mesh, 2)

    # Assign value from cdata list to the MeshFunction's underlying array
    for cell, value in zip(fe.cells(mesh), values):
        cdata[cell.index()] = value

    growth_classes = {1: growth_constant_model, 2: growth_strain_model}
    model = HeterogeneousGrowth(growth_classes=growth_classes, cdata=cdata)
    return model

def test_constant_growth_init(growth_constant_model, plastic_vform, constant_term):
    growth_constant_model.plastic_vform = plastic_vform

    assert growth_constant_model.plastic_vform == plastic_vform
    assert growth_constant_model.type == 'constant'
    assert np.array_equal(growth_constant_model.dg, constant_term)

def test_constant_growth_increment(growth_constant_model, plastic_vform):
    # Ensure time and dt are set properly
    growth_constant_model.t = 0
    growth_constant_model.dt = 1
    growth_constant_model.plastic_vform == plastic_vform

    growth_constant_model.get_growth_increment()
    assert np.allclose(list(growth_constant_model.growth_incr.values()), [0.1, 0, 0, 0.1])

    # Update time
    growth_constant_model.update_time(1)
    assert growth_constant_model.t == 1

def test_growth_apply(growth_constant_model, fenics_setup, previous_solution, plastic_vform):
    # Ensure time and dt are set properly
    growth_constant_model.t = 0
    growth_constant_model.dt = 1
    growth_constant_model.prev_solution = previous_solution
    growth_constant_model.prev_growth = plastic_vform.F_g
    growth_constant_model.plastic_vform = plastic_vform

    # Build mock problem
    _, V = fenics_setup
    u = fe.TrialFunction(V)
    v = fe.TestFunction(V)
    sol = fe.Function(V)

    # Call apply_growth
    growth_constant_model.apply_growth(u, v, sol)

    # Check the new growth values
    growth = growth_constant_model.get_growth()
    assert np.allclose(growth.compute_vertex_values(), [1.1, 1.1, 1.1, 1.1,
                                                        0.0, 0.0, 0.0, 0.0,
                                                        0.0, 0.0, 0.0, 0.0,
                                                        1.1, 1.1, 1.1, 1.1])

def test_time_growth_init(growth_time_model, plastic_vform, growth_func):
    assert growth_time_model.type == 'time-dependent'
    assert growth_time_model.growth_func == growth_func
    assert growth_time_model.args == (1.0, 0.5)

    growth_time_model.plastic_vform = plastic_vform
    assert growth_time_model.plastic_vform == plastic_vform


def test_time_growth_increment(monkeypatch, growth_time_model, plastic_vform):
    # Mock quad_vec to return a specific value
    dg_dt = np.zeros((2, 2))
    dg_dt[0, 0] = 1.0
    monkeypatch.setattr('scipy.integrate.quad_vec', lambda *args, **kwargs: (dg_dt, 0))

    # Ensure time and dt are set properly
    growth_time_model.t = 0.5
    growth_time_model.dt = 0.5
    growth_time_model.plastic_vform = plastic_vform

    # Call method
    growth_time_model.get_growth_increment()

    # Check growth increment
    assert np.allclose(growth_time_model.growth_incr.values(), [0.65915494, 0, 0, 0])

def test_strain_growth_init(growth_strain_model, plastic_vform, target_strain, extensibility):
    assert growth_strain_model.type == 'strain-dependent'
    assert np.array_equal(growth_strain_model.target_strain, target_strain)
    assert growth_strain_model.extensibility == extensibility

    growth_strain_model.plastic_vform = plastic_vform
    assert growth_strain_model.plastic_vform == plastic_vform

def test_strain_growth_increment(growth_strain_model, target_strain, plastic_vform):
    # Ensure time and dt are set properly
    growth_strain_model.t = 0
    growth_strain_model.dt = 1
    growth_strain_model.plastic_vform = plastic_vform

    # Call the growth increment method with non-null target strain
    growth_strain_model.get_growth_increment()

    # - Check if the growth increment matches the expected result
    target_strain_growth_incr = growth_strain_model.get_growth(only_increment=True)
    assert np.allclose(target_strain_growth_incr.compute_vertex_values(),
                       [0.311, 0.002, 0.311, 0.311,
                        -0.242, -0.006, -0.242, -0.242,
                        -0.242, -0.006, -0.242, -0.242,
                           0.189, 0.020, 0.189, 0.189], atol=1e-3)

    # Call the growth increment method with null target strain
    growth_strain_model.target_strain = None
    growth_strain_model.get_growth_increment()

    # Check if the growth increment matches the expected  result
    no_target_strain_growth_incr = growth_strain_model.get_growth(only_increment=True)
    assert np.allclose(no_target_strain_growth_incr.compute_vertex_values(),
                       [ 0.25, -0.1775, 0.25, 0.25,
                         -0.32, -0.0625, -0.32, -0.32,
                         -0.32, -0.0625, -0.32, -0.32,
                         0.09,  0.0025, 0.09,  0.09])

def test_heterogeneous_init(growth_heterogeneous_model, plastic_vform):
    assert (growth_heterogeneous_model.type[1] == 'constant') and (growth_heterogeneous_model.type[2] == 'strain-dependent')
    assert growth_heterogeneous_model.t == 0

    growth_heterogeneous_model.dt = 0
    assert growth_heterogeneous_model.dt == 0

    # - Propagate plastic_vform & prev_growth
    growth_heterogeneous_model.plastic_vform = plastic_vform
    assert growth_heterogeneous_model.plastic_vform is not None

    growth_heterogeneous_model.prev_growth = plastic_vform.F_g
    assert growth_heterogeneous_model.prev_growth is not None

def test_heterogeneous_increment(growth_heterogeneous_model, previous_solution, plastic_vform):
    growth_heterogeneous_model.t = 0
    growth_heterogeneous_model.dt = 1
    growth_heterogeneous_model.prev_solution = previous_solution
    growth_heterogeneous_model.plastic_vform = plastic_vform

    growth_heterogeneous_model.get_growth_increment()

    growth_incr = growth_heterogeneous_model.get_growth(only_increment=True)
    assert np.allclose(growth_incr.compute_vertex_values(), [ 0.311, 0.100, 0.311, 0.311,
                                                              -0.242, 0.000, -0.242, -0.242,
                                                              -0.242, 0.000, -0.242, -0.242,
                                                              0.189, 0.100, 0.189, 0.189], atol=1e-3)



