import unittest
from bvpy.boundary_conditions.neumann import *
from bvpy.boundary_conditions import Boundary
import fenics as fe

import numpy as np

class TestNeumann(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_neumannInterface(self):
        neu_int = NeumannInterface(1, "all")
        self.assertEqual("<NeumannInterface object, location: all, value: 1>", neu_int.__repr__())

    def test_normal(self):
        mesh = fe.UnitSquareMesh(2,2)
        V = fe.VectorFunctionSpace(mesh, 'P', 1)
        mf = fe.MeshFunction('size_t', mesh, 1)
        neu = NormalNeumann(boundary='all')
        neu = NormalNeumann(boundary=Boundary('all'))
        self.assertEqual("<NormalNeumann object, location: all, value: 1>", neu.__repr__())

        d = neu.apply(V, mf=mf)
        np.testing.assert_array_almost_equal(d(1, 0.5), [1, 0])

    def test_constant(self):
        mesh = fe.UnitSquareMesh(2,2)
        V = fe.VectorFunctionSpace(mesh, 'P', 1)
        neu = ConstantNeumann(1, boundary='all')

        d = neu.apply()
        self.assertEqual(d.values()[0], 1)

    def test_marking_const(self):
        mesh = fe.UnitSquareMesh(2,2)
        neu = ConstantNeumann(1, boundary='all', ind=34)
        mf = fe.MeshFunction("size_t", mesh, 1, 0)

        d = neu.apply(mf)
        self.assertEqual(mf.array()[0], neu._id)

    def test_marking_var(self):
        mesh = fe.UnitSquareMesh(2,2)
        neu = VariableNeumann('x', boundary='all', ind=34)
        mf = fe.MeshFunction("size_t", mesh, 1, 0)
        V = fe.FunctionSpace(mesh, 'P', 1)

        d = neu.apply(V, mf)
        self.assertEqual(mf.array()[0], neu._id)
