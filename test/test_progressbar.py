import unittest

from bvpy.utils.progress_bar import ProgressBar


class TestProgressBar(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        pass

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_set_range(self):
        pb = ProgressBar()
        pb.set_range(0, 10, current=3)

        self.assertEqual(pb._start, 0)
        self.assertEqual(pb._stop, 10)
        self.assertEqual(pb._current, 3)

    def test_construct(self):
        pb = ProgressBar(text='Test', size_bar=2, item='◊')

        self.assertEqual(pb._text, 'Test')
        self.assertEqual(pb._size, 2)
        self.assertEqual(pb._item, '◊')

    def test_set_current(self):
        pb = ProgressBar()
        pb.set_current(1)

        self.assertEqual(pb._current, 1)

    def test_update(self):
        pb = ProgressBar()
        pb.set_range(0, 10, current=3)

        self.assertEqual(pb._progress_bar, '\rRun |■■■■■■              | 30 %')
