import unittest

# -- test class

from bvpy.domains import (Rectangle, Disk, Sphere, HemiSphere, Ellipsoid,
                          Torus, CustomPolygonalDomain, Cylinder, Cube)
import fenics as fe
import numpy as np


class TestPrimitive(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        pass

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_input(self):
        domain = Rectangle(cell_size=[0.05, 0.1], clear=True)
        domain.set_cell_size_from_curvature(2)

    def test_AbstractDomain_throws_exception(self):
        # - try to put dummy input in an AbstractDomain class
        self.assertRaises(ValueError, Rectangle,
                          cell_type='tri')  # invalid cell_type, can be {'line', 'triangle', 'tetra'}
        self.assertRaises(ValueError, Rectangle,
                          cell_size='1')  # invalid cell_size, should be a list of two numbers or one number
        self.assertRaises(ValueError, Rectangle, cell_size=[1])  # invalid cell_size
        self.assertRaises(ValueError, Rectangle, cell_size=[1, 2, 4])  # invalid cell_size
        self.assertRaises(ValueError, Rectangle, verbosity=1)  # invalid verbosity, can be {True, False}
        self.assertRaises(ValueError, Rectangle, dim=2,
                          cell_type='tetra')  # invalid embedded dimension : should be equal or larger to the dimension of finite element
        self.assertRaises(ValueError, Rectangle, cell_type='triangle', algorithm='MyAlgo')  # wrong algo for 2D
        self.assertRaises(ValueError, Rectangle, cell_type='tetra', algorithm='MyAlgo')  # wrong algo for 3D

        domain = Rectangle()
        self.assertRaises(ValueError, domain.set_cell_size_from_curvature, 'a')

    def test_rectangle(self):
        domain = Rectangle(cell_size=0.1, clear=True)
        domain.discretize()
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 1)

        domain._details

    def test_hemisphere(self):
        domain = HemiSphere(cell_size=0.1, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 2].min(), 0)

    def test_disk(self):
        domain = Disk(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 1].min(), -1, delta=1e-3)

    def test_sphere(self):
        domain = Sphere(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 1].max(), 1, delta=1e-3)

    def test_ellipsoid(self):
        domain = Ellipsoid(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 0].min(), -1, delta=1e-3)

    def test_torus(self):
        domain = Torus(cell_size=0.1, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 0].min(), -3, delta=1e-3)

    def test_cylinder(self):
        domain = Cylinder(open=True, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)

    def test_cube(self):
        domain = Cube(clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)

        domain.rotate((0, 0, 0), (1, 0, 0), 0.1) # try rotation

    def test_csg_sub(self):
        d1 = Rectangle(x=0, y=0, length=1, width=1, clear=True)
        d2 = Rectangle(x=0.5, y=0, length=0.5, width=1)
        d = d1 - d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].max(), 0.5)

    def test_csg_add(self):
        d1 = Rectangle(x=0, y=0, length=0.5, width=1, clear=True)
        d2 = Rectangle(x=0.5, y=0, length=0.5, width=1)
        d = d1 + d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].max(), 1)

    def test_csg_intersect(self):
        d1 = Rectangle(x=0, y=0, length=1.5, width=1, clear=True)
        d2 = Rectangle(x=1, y=0, length=1, width=1)
        d = d1 & d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].min(), 1)

    def test_primitive_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 1, 2, 3],
             [1, 4, 2]]

        domain = CustomPolygonalDomain(p, c, cell_size=0.05, isoriented=True, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 2)

    def test_orient_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 3, 2, 1],
             [1, 2, 4]]

        domain = CustomPolygonalDomain(p, c, cell_size=0.05, isoriented=False, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 2)

    def test_markers_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 3, 2, 1],
             [1, 2, 4]]

        domain = CustomPolygonalDomain(p, c, markers=[0, 1], cell_size=0.05, isoriented=False, clear=True)
        domain.discretize()
        ind = 10
        label = domain.cdata.array()[ind]
        p = fe.Cell(domain.mesh, ind).midpoint().array()
        if p[0] < 1:
            self.assertEqual(label, 0)
        else:
            self.assertEqual(label, 1)

    def test_domain_repr_str(self):
        dom = Rectangle(verbosity=True)
        self.assertEqual(dom.__repr__()[1:10], 'Rectangle')
        self.assertEqual(dom.__str__()[:22], 'Mesh not generate yet.')

    def test_discretize_square_cube(self):
        sqr = Rectangle(cell_type='line')
        sqr.discretize()
        sqr.info()

    def test_sphere_sub(self):
        big = Sphere(radius=10, cell_type='tetra')
        sml = Sphere(radius=9, cell_type='tetra')
        shl = big - sml
        shl.geometry()
        shl.discretize()

    def test_sphere_add(self):
        sph = Sphere(radius=2, cell_type='tetra')
        sph_2 = Sphere(radius=2, center=[0, 0, 2], cell_type='tetra')
        intsec = sph + sph_2
        intsec.discretize()

    def test_sphere_intersec(self):
        sph = Sphere(radius=2, cell_type='tetra')
        sph_2 = Sphere(radius=2, center=[0, 0, 2], cell_type='tetra')
        intsec = sph & sph_2
        intsec.discretize()

    def test_mesh_conversion(self):
        # - Assert that we can convert labelled meshes (entities in the gmsh mesh has an associated label + name)
        #   label can be used to produce sub-domains or sub-boundaries
        L = 2
        r = 0.2

        ### - 1D element dimension : line - ###
        ###########################################
        # - Create a domain as cylinder with two sub-domains
        geometry = Disk(radius=L, cell_type='line', cell_size=0.4, clear=True)
        geometry.factory.remove([(2, 1)])  # no need surface
        circle = geometry.factory.getEntities(dim=1)

        # - Cut the circle by adding first two points
        geometry.factory.addPoint(x=L, y=0, z=0, tag=2)
        geometry.factory.addPoint(x=-L, y=0, z=0, tag=3)

        # - Divide the circle according to the points : create sub-domains
        geometry.factory.fragment(objectDimTags=circle, toolDimTags=[(0, 2), (0, 3)])

        geometry.factory.synchronize()  # synchronize prior sub-domains labellisation

        # - Label the surface entities to create two sub-domains (0 <= x <= L/2 and L/2 <= x <= L)
        geometry.model.addPhysicalGroup(1, [1], 1)  # add label 1 to line 1
        geometry.model.setPhysicalName(1, 1, 'left_side')  # add label title
        geometry.model.addPhysicalGroup(1, [2], 2)  # add label 2 ti line 2
        geometry.model.setPhysicalName(1, 2, 'right_side')  # add label title

        # - Discretize
        geometry.discretize()

        # - Assert that the label and label_names are stored in the AbstractModel
        self.assertTrue(set(geometry.cdata.array()) == {1, 2})  # 1D : label are stored
        self.assertTrue(
            set(list(geometry.sub_domain_names.keys())) == {1, 2})  # 1D: label are also stored in sub_domain_names
        self.assertTrue(
            set(list(geometry.sub_domain_names.values())) == {'left_side', 'right_side'})  # 1D: name of the sub-domains

        ### - 2D element dimension : triangle - ###
        ###########################################
        # - Create a domain as cylinder with two sub-domains
        geometry = Cylinder(x=0, dx=L, dz=0, r=r, cell_type='triangle', cell_size=0.2, clear=True)
        geometry.factory.remove([(3, 1)])  # no need volume
        surfaces = geometry.factory.getEntities(dim=2)

        # - Create a circle at the middle of the cylinder
        circle = geometry.factory.addCircle(x=L / 2, y=0, z=0, r=r)
        geometry.factory.rotate([(1, circle)], x=L / 2, y=0, z=0, ax=0, ay=1, az=0,
                                angle=np.pi / 2)  # rotate the circle

        # - Divide the cylinder according to the circle : create sub-domains
        geometry.factory.fragment(objectDimTags=surfaces, toolDimTags=[(1, circle)])

        geometry.factory.synchronize()  # synchronize prior sub-domains labellisation

        # - Label the surface entities to create two sub-domains (0 <= x <= L/2 and L/2 <= x <= L)
        geometry.model.addPhysicalGroup(2, [2, 4], 1)  # add label 1 to surfaces 2 and 4
        geometry.model.setPhysicalName(2, 1, 'left_side')  # add label title
        geometry.model.addPhysicalGroup(2, [3, 5], 2)  # add label 1 to surfaces 3 and 5
        geometry.model.setPhysicalName(2, 2, 'right_side')  # add label title

        # - Discretize
        geometry.discretize()

        # - Assert that the label and label_names are stored in the AbstractModel
        self.assertTrue(set(geometry.cdata.array()) == {1, 2})  # 2D: label are stored
        self.assertTrue(
            set(list(geometry.sub_domain_names.keys())) == {1, 2})  # 2D: label are also stored in sub_domain_names
        self.assertTrue(
            set(list(geometry.sub_domain_names.values())) == {'left_side', 'right_side'})  # 2D: name of the sub-domains

        ### - 3D element dimension : tetra - ###
        ###########################################
        # - Create a domain as cylinder with two sub-domains
        geometry = Cylinder(x=0, dx=L, dz=0, r=r, cell_type='tetra', cell_size=0.2, clear=True)
        volumes = geometry.factory.getEntities(dim=3)

        # - Create a disk at the middle of the cylinder
        circle = geometry.factory.addCircle(x=L / 2, y=0, z=0, r=r)
        curve_loop = geometry.factory.addCurveLoop([circle])
        disk = geometry.factory.addPlaneSurface([curve_loop])

        geometry.factory.rotate([(2, disk)], x=L / 2, y=0, z=0, ax=0, ay=1, az=0,
                                angle=np.pi / 2)  # rotate the circle

        # - Divide the cylinder according to the circle : create sub-domains
        geometry.factory.fragment(objectDimTags=volumes, toolDimTags=[(2, disk)])

        geometry.factory.synchronize()  # synchronize prior sub-domains labellisation

        # - Label the volume entities to create two sub-domains (0 <= x <= L/2 and L/2 <= x <= L)
        geometry.model.addPhysicalGroup(3, [1], 1)  # add label 1 to volume 1
        geometry.model.setPhysicalName(3, 1, 'left_side')  # add label title
        geometry.model.addPhysicalGroup(3, [2], 2)  # add label 2 to volume 2
        geometry.model.setPhysicalName(3, 2, 'right_side')  # add label title

        # - Label also the surface entities to create two sub-boundaries (0 <= x <= L/2 and L/2 <= x <= L)
        geometry.model.addPhysicalGroup(2, [8], 1)  # add label 1 to surfaces 8
        geometry.model.setPhysicalName(2, 1, 'left_boundary')  # add label title
        geometry.model.addPhysicalGroup(2, [6], 2)  # add label 2 to surfaces 6
        geometry.model.setPhysicalName(2, 2, 'right_boundary')  # add label title
        geometry.model.addPhysicalGroup(2, [5, 7], 3)  # add label 3 to surfaces [5, 7]
        geometry.model.setPhysicalName(2, 3, 'other_boundary')  # add label title

        # - Discretize
        geometry.discretize()

        # - Assert that the label and label_names are stored in the AbstractModel
        self.assertTrue(set(geometry.cdata.array()) == {1, 2})  # 3D: label are stored
        self.assertTrue(
            set(list(geometry.sub_domain_names.keys())) == {1, 2})  # 3D: label are also stored in sub_domain_names
        self.assertTrue(
            set(list(geometry.sub_domain_names.values())) == {'left_side', 'right_side'})  # 3D: name of the sub-domains
        self.assertTrue(len({1, 2, 3} - set(geometry.bdata.array())) == 0)  # 3D: all boundary labels are stored
        self.assertTrue(
            set(list(geometry.sub_boundary_names.keys())) == {1, 2, 3})  # 3D: label are also stored in sub_domain_names
        self.assertTrue(set(list(geometry.sub_boundary_names.values())) == {'left_boundary', 'right_boundary',
                                                                            'other_boundary'})  # 3D: name of the sub-domains

        ### - RunTimeError - ###
        ###########################################
        # Let ele_dim be the topological dimension of the finite element of the gmsh mesh (ele_dim can be defined from
        # _cell_type in the AbstractDomain class).
        # If no PhysicalGroup is defined for entities at dim. ele_dim whereas for some other entities
        # at dim. < ele_dim PhysicialGroup have been set => error with RunTimeError
        def error_test():
            geometry = Cylinder(x=0, dx=L, dz=0, r=r, cell_type='tetra', clear=True)

            geometry.factory.synchronize()  # synchronize prior sub-domains labellisation

            # - Label surfaces entities WITHOUT labelling volume entities
            #geometry.model.addPhysicalGroup(3, [1], 1)  # add label 1 to volume 1
            #geometry.model.setPhysicalName(3, 1, 'my_domain')  # add label title

            geometry.model.addPhysicalGroup(2, [1, 2, 3], 1)  # add label 1 to surfaces 1, 2 and 3
            geometry.model.setPhysicalName(2, 1, 'my_boundary')  # add label title

            # - Discretize
            geometry.discretize()

        self.assertRaises(RuntimeError, error_test)
