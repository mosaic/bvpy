import unittest

from bvpy.utils.pre_processing import *
from fenics import UnitSquareMesh, FunctionSpace, VectorFunctionSpace
from numpy.testing import assert_array_almost_equal


class TestPreProcess(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.mesh = UnitSquareMesh(2, 2)
        self.V = FunctionSpace(self.mesh,'P', 1)
        self.VV = VectorFunctionSpace(self.mesh,'P', 1)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_create_expr_str(self):
        expr = create_expression("x", functionspace=self.V)
        self.assertAlmostEqual(expr(1,0), 1)

        expr = create_expression(("x", "y"), functionspace=self.VV)
        self.assertAlmostEqual(expr(1,1)[0], 1)
        self.assertAlmostEqual(expr(1,1)[1], 1)

    def test_create_expr_array(self):
        arr = list(range(self.mesh.num_entities(2)))
        expr = create_expression(arr, functionspace=self.V)

        self.assertAlmostEqual(expr(0.66666667, 0.83333333), 7)

    def test_create_expr_func_scal(self):
        def test(x):
            if x[0]>0.5:
                return 1
            else:
                return 0
        expr = create_expression(test, functionspace=self.V)

        self.assertAlmostEqual(expr(1, 0), 1)
        self.assertAlmostEqual(expr(0, 0), 0)

    def test_create_expr_func_vec(self):
        def test(x):
            if x[0]>0.5:
                return [1,1]
            else:
                return [0,0]
        expr = create_expression(test, functionspace=self.VV)

        assert_array_almost_equal(expr(1, 0), [1,1])
        assert_array_almost_equal(expr(0, 0), [0,0])

    def test_create_expr_func_tensor(self):
        def test(x):
            if x[0]>0.5:
                return [[1,1],[1,1]]
            else:
                return [[0,0],[0,0]]
        VVV = fe.TensorFunctionSpace(self.mesh, 'P', 1)
        expr = create_expression(test, functionspace=VVV)

        assert_array_almost_equal(expr(1, 0).reshape(2,2), [[1,1],[1,1]])
        assert_array_almost_equal(expr(0, 0).reshape(2,2), [[0,0],[0,0]])

    def test_heterogeneous_parameter(self):
        mf = fe.MeshFunction('size_t', self.mesh, 2, 0)
        sub = fe.CompiledSubDomain('x[0]>=0.5')
        sub.mark(mf, 1)
        para = HeterogeneousParameter(mf, {0:0, 1:1})
