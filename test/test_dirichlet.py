import unittest
from bvpy.boundary_conditions.dirichlet import NormalDirichlet, ZeroDirichlet, ConstantDirichlet, VariableDirichlet
from bvpy.boundary_conditions import Boundary, dirichlet
import fenics as fe

from numpy.testing import assert_array_equal


class TestDirichlet(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        self.mesh = fe.UnitSquareMesh(2, 2)
        self.V = fe.VectorFunctionSpace(self.mesh, 'P', 1)
        self.scalar_V = fe.FunctionSpace(self.mesh, 'P', 1)

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_warning(self):
        # Constant Dirichlet warning
        with self.assertLogs('bvpy', level='WARNING') as log:
            diri = ConstantDirichlet(1, boundary="near(x, -1)")
            diri.apply(self.scalar_V)

        self.assertTrue(any("No nodes are marked for this domain" in message for message in log.output))

        # Variable Dirichlet warning
        with self.assertLogs('bvpy', level='WARNING') as log:
            diri = VariableDirichlet('x', boundary="near(x, -1)")
            diri.apply(self.scalar_V)

        self.assertTrue(any("No nodes are marked for this domain" in message for message in log.output))

    def test_valid_input(self):
        result = dirichlet(val=Boundary('all'), boundary=Boundary('all'))
        self.assertIsNone(result)

    def test_normal(self):
        diri = NormalDirichlet(boundary='all')
        diri = NormalDirichlet(boundary=Boundary('all'))
        self.assertEqual("<NormalDirichlet object, location: all, value: 1>", diri.__repr__())
        d = diri.apply(self.V)
        self.assertEqual(d.get_boundary_values()[5], 1)

    def test_zero(self):
        diri = ZeroDirichlet("all", shape=0)
        assert_array_equal(diri._val, 0)
        diri = ZeroDirichlet("all", shape=3)
        assert_array_equal(diri._val, [0, 0, 0])

    def test_constant(self):
        # - vector
        val = (1, 2)
        boundary = Boundary('all')
        diri = ConstantDirichlet(val, boundary=boundary, method='topological')
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        self.assertEqual(diri._method, 'topological')
        d = diri.apply(self.V)
        for bc_val in d.get_boundary_values().values():
            self.assertIn(bc_val, val)

        # - scalar
        val = 2
        boundary = Boundary('all')
        diri = ConstantDirichlet(val, boundary=boundary)
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        d = diri.apply(self.scalar_V)
        bc_vals = d.get_boundary_values().values()
        assert_array_equal(list(bc_vals), [val] * len(bc_vals))

        # - subspace
        diri = ConstantDirichlet(val, boundary=boundary, subspace=0)
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        d = diri.apply(self.V)
        bc_vals = d.get_boundary_values().values()
        assert_array_equal(list(bc_vals), [val] * len(bc_vals))

    def test_variable(self):
        # - vector
        val = '(x, y)'
        boundary = Boundary('all')
        diri = VariableDirichlet(val, boundary=boundary, degree=1)
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        d = diri.apply(self.V)
        bc_vals = d.get_boundary_values().values()
        self.assertTrue(all(val >= 0 and val <= 2 for val in bc_vals))

        # - scalar
        val = "x"
        boundary = Boundary('all')
        diri = VariableDirichlet(val, boundary=boundary, degree=1)
        self.assertEqual(repr(diri), '<VariableDirichlet object, location: all, value: x>')
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        d = diri.apply(self.scalar_V)
        bc_vals = d.get_boundary_values().values()
        self.assertTrue(all(val >= 0 and val <= 1 for val in bc_vals))

        # - subspace
        diri = dirichlet(val, boundary=boundary, subspace=0, degree=1)
        self.assertEqual(diri._val, val)
        self.assertEqual(diri._boundary, boundary)
        d = diri.apply(self.V)
        bc_vals = d.get_boundary_values().values()
        self.assertTrue(all(val >= 0 and val <= 1 for val in bc_vals))
