import unittest
import tempfile
from pathlib import Path

from fenics import UnitSquareMesh
from bvpy import BVP
from bvpy.domains.primitives import Rectangle
from bvpy.vforms.poisson import PoissonForm
from bvpy.boundary_conditions.dirichlet import dirichlet
from bvpy.domains.custom_gmsh import CustomDomainGmsh
# -- test class

from bvpy.utils.io import *


class TestIO(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.tmp = tempfile.TemporaryDirectory()

        # Base directory where the `tutorial_data` folder resides
        self.test_data_dir = Path(__file__).parent / "data"

    def tearDown(self):
        """Concludes and closes the test.
        """
        self.tmp.cleanup()

    def test_read_gmsh_data(self):
        path = str(self.test_data_dir / "mesh_example.msh")
        dom = CustomDomainGmsh(path)

    def test_read_polygonal_data_ply(self):
        path = str(self.test_data_dir / "ply_example.ply")
        points, cells, _ = read_polygonal_data(path)

        self.assertEqual(len(points), 10)
        self.assertEqual(len(cells), 1)

    def test_read_polygonal_data_obj(self):
        path = str(self.test_data_dir / "obj_example.obj")
        points, cells, _ = read_polygonal_data(path)

        self.assertEqual(len(points), 6)
        self.assertEqual(len(cells), 2)

    def test_read_polygonal_data_txt(self):
        path = str(self.test_data_dir / "tissue_example.txt")
        points, cells, _ = read_polygonal_data(path)
        self.assertEqual(len(points), 220)
        self.assertEqual(len(cells), 61)

    def test_read_polygonal_data_txt_2(self):
        path = str(self.test_data_dir / "tissue_example_2.txt")
        points, cells, label = read_polygonal_data(path)
        self.assertEqual(label[0], 0)
        self.assertEqual(label[-1], 60)
        self.assertEqual(len(label), 61)

    def test_read_polygonal_data_wrong_type(self):
        path = str(self.test_data_dir / "test_recording_mesh.xdmf")

        with self.assertRaises(ValueError) as context:
            read_polygonal_data(path)

        # Check that the exception message is correct
        self.assertIn(
            f"The format of the given file {path} is not supported.",
            str(context.exception)
        )

    def test_write_ply_mesh(self):
        mesh = UnitSquareMesh(2, 2)
        path = self.tmp.name+'/mesh'
        write_ply(mesh, path)

    def test_write_ply_domain(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/domain.ply'
        write_ply(domain, path)

    def test_write_ply_wrong_type(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/domain.ply'
        write_ply(domain.cdata, path)

    def test_numerize(self):
        self.assertEqual(numerize('3'), 3)
        self.assertEqual(numerize('3.4'), 3.4)
        self.assertEqual(numerize('three'), 'three')

    def test_save_domain(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain, path)

    def test_save_cdata(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain.cdata, path)

    def test_save_mesh(self):
        domain = Rectangle()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain.mesh, path)

    def test_save_problem(self):
        prblm = BVP(domain=Rectangle(),
                    vform=PoissonForm(),
                    bc=dirichlet(0, 'all'))
        path = self.tmp.name+'/save_test'
        save(prblm, path)

    def test_save_solution(self):
        prblm = BVP(domain=Rectangle(),
                    vform=PoissonForm(),
                    bc=dirichlet(0, 'all'))
        path = self.tmp.name+'/save_test.xdmf'
        save(prblm.solution, path)
