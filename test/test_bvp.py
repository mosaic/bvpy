import unittest

from bvpy import BVP, logger
from bvpy.domains import Rectangle
from bvpy.vforms import PoissonForm
from bvpy.boundary_conditions import dirichlet, neumann
from bvpy.utils.pre_processing import create_expression


class TestPoisson(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.domain = Rectangle(x=-2, y=-2, length=4, width=4, cell_size=0.05)
        self.vform = PoissonForm(source=0)
        self.bc = dirichlet(0, boundary="all")


    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_constructor(self):
        bvp = BVP(self.domain, self.vform, self.bc)
        self.assertIsNotNone(bvp.domain.mesh)
        bvp.info()
        print(bvp)

        bvp = BVP()
        self.assertEqual("BVP", bvp.__repr__()[1:4])
        self.assertIsNone(bvp.domain)
        self.assertIsNone(bvp.vform)

    def test_add_variable_neumann(self):
        neu = neumann('x', boundary='all')
        bvp = BVP(self.domain, self.vform, neu)

    def test_solve(self):
        bvp = BVP(self.domain, self.vform, self.bc)
        bvp.solve(return_solver_info=True)
