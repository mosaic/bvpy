import unittest

import fenics as fe
import numpy as np
from numpy.testing import assert_array_equal
from bvpy import logger
from bvpy.utils.tensor import (applyElementwise, amplitude, sph,
                               dev, intensity, anisotropy)


class TestTensor(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.Zr2 = np.zeros((2, 2))
        self.Zr3 = np.zeros((3, 3))
        self.Id2 = np.eye(2)
        self.Id3 = np.eye(3)
        self.ev1 = 2
        self.ev2 = 3
        self.mtrx = np.array([[self.ev1, 0],
                              [0, self.ev2]])

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_applyEmentwise(self):
        def square(x):
            return x**2

        tnsr_fspace = fe.TensorFunctionSpace(fe.UnitSquareMesh(2, 2), 'DG', 0)
        fe_tensor = fe.Constant([[1, 2], [3, 4]])

        tensor = fe.project(fe_tensor, tnsr_fspace)
        squared_tensor = applyElementwise(square, tensor)

        for mtrx in fe.project(squared_tensor,
                               tnsr_fspace).vector().get_local().reshape(-1,
                                                                         2,
                                                                         2):
            assert_array_equal(mtrx, [[1, 4], [9, 16]])

    def test_amplitude(self):
        self.assertEqual(amplitude(self.Id2), 1)
        self.assertEqual(amplitude(self.Id3), 1)
        self.assertEqual(amplitude(self.Zr2), 0)
        self.assertEqual(amplitude(self.Zr3), 0)
        self.assertAlmostEqual(amplitude(self.mtrx),
                               np.sqrt((self.ev1**2 + self.ev2**2) / 2),
                               delta=1e-7)

    def test_sph(self):
        assert_array_equal(sph(self.Id2), self.Id2)
        assert_array_equal(sph(self.Id3), self.Id3)
        assert_array_equal(sph(self.Zr2), self.Zr2)
        assert_array_equal(sph(self.Zr3), self.Zr3)

        sph_mtrx = .5 * (self.ev1 + self.ev2) * self.Id2
        assert_array_equal(sph(self.mtrx), sph_mtrx)

    def test_dev(self):
        mtrx_dev = .5 * (self.ev1 - self.ev2) * np.array([[1, 0],
                                                          [0, -1]])

        assert_array_equal(dev(self.Id2), self.Zr2)
        assert_array_equal(dev(self.Id3), self.Zr3)
        assert_array_equal(dev(self.mtrx), mtrx_dev)

    def test_intensity(self):
        self.assertEqual(intensity(self.Id2), 1)
        self.assertEqual(intensity(self.Id3), 1)
        self.assertEqual(intensity(self.Zr2), 0)
        self.assertEqual(intensity(self.Zr3), 0)
        self.assertEqual(intensity(self.mtrx), .5 * (self.ev1 + self.ev2))

    def test_anisotropy(self):
        self.assertEqual(anisotropy(self.Id2), 0)
        self.assertEqual(anisotropy(self.Id3), 0)

        self.assertEqual(anisotropy(self.mtrx),
                         np.abs((self.ev1 - self.ev2) / (self.ev1 + self.ev2)))

        self.assertFalse(anisotropy(self.Zr2))
