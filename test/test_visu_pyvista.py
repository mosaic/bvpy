import unittest
import shutil
# -- test class

from bvpy.utils.visu_pyvista import (_check_is3D, _check_scalar_bar_title, _visu_feDiriclet, _visu_feFunction,
                                     _visu_feFunctionSizet, _visu_feMesh, visualize)
from bvpy.bvp import BVP
from bvpy.vforms.elasticity import LinearElasticForm
from bvpy.domains.primitives import Rectangle
from bvpy.boundary_conditions import dirichlet
import pyvista as pv
import fenics as fe
import numpy as np
class TestVisu(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        self.mesh = fe.UnitSquareMesh(2, 2)
        self.V = fe.FunctionSpace(self.mesh, 'P', 1)
        self.f = fe.project(fe.Constant(0), self.V)
        self.bc = fe.DirichletBC(self.V, fe.Constant(0), "on_boundary")
        self.mf = fe.MeshFunction("size_t", self.mesh, 2)

        # - Create dummy BVP file with vector field solution
        domain = Rectangle(dim=2)
        diri = dirichlet([0, 0], boundary='near(x, 0)')
        vform = LinearElasticForm(source=[0, -1e-2],
                                  young=1e5,
                                  poisson=.3,
                                  plane_stress=True)

        self.bvp = BVP(domain=domain, vform=vform, bc=diri)
        self.bvp.solve() # get solution

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_is3D(self):
        self.assertEqual(_check_is3D(np.ones((10, 1))).shape[1], 3)  # check if 1D data are converted in 3D data
        self.assertEqual(_check_is3D(np.ones((10, 2))).shape[1], 3) # check if 2D data are converted in 3D data
        self.assertEqual(_check_is3D(np.ones((10, 3))).shape[1], 3)  # check if 3D data re unchanged
        self.assertRaises(AssertionError, _check_is3D, np.ones((10, 4)))  # wrong input if 4D

    def test_check_scalar_bar_title(self):
        pl = pv.Plotter(off_screen=True)
        pl = _visu_feFunction(self.f, plotter=pl, show_plot=False)
        scalar_bar_name = list(pl.scalar_bars.keys())[0]
        self.assertNotEqual(scalar_bar_name, _check_scalar_bar_title(pl, scalar_bar_name)) # generate unique name ?
        pl.close()

    def test_visu_mesh(self):
        # - use dedicated function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(_visu_feMesh(self.mesh, plotter=pl, show_plot=False))
        pl.close()

        # - use generic function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.mesh, plotter=pl, show_plot=False))
        pl.close()

        # - use BVP
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.bvp, visu_type='mesh', plotter=pl, show_plot=False))
        pl.close()

    def test_visu_function(self):
        # - use dedicated function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(_visu_feFunction(self.f, plotter=pl, scale=1, show_plot=False))
        pl.close()

        # - use generic function & test show_mesh
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.f, plotter=pl, scale=1, show_plot=False, show_mesh=False))
        pl.close()

        # - use BVP
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.bvp, visu_type='solution', plotter=pl, show_plot=False))
        pl.close()

        # - use BVP & test parameter
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.bvp, visu_type='young', plotter=pl, show_plot=False))
        pl.close()

    def test_visu_dirichlet(self):
        # - use dedicated function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(_visu_feDiriclet(self.bc, plotter=pl, show_plot=False))
        pl.close()

        # - use generic function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.bc, plotter=pl, show_plot=False))
        pl.close()

    def test_visu_meshfunc(self):
        # - use dedicated function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(_visu_feFunctionSizet(self.mf, plotter=pl, show_plot=False, cmap="inferno"))
        pl.close()

        # - use generic function
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.mf, plotter=pl, show_plot=False, cmap="inferno"))
        pl.close()

        # - use BVP
        pl = pv.Plotter(off_screen=True)
        self.assertIsNotNone(visualize(self.bvp, visu_type='domain', plotter=pl, show_plot=False))
        pl.close()

    def test_multiplot(self):
        # - use generic function
        pl = pv.Plotter(off_screen=True, shape=(1, 4))
        pl.subplot(0, 0)
        visualize(self.mesh, plotter=pl, show_plot=False)
        pl.subplot(0, 1)
        visualize(self.bc, plotter=pl, show_plot=False)
        pl.subplot(0, 2)
        visualize(self.mf, plotter=pl, show_plot=False, cmap="inferno")
        pl.subplot(0, 3)
        visualize(self.f, plotter=pl, scale=1, show_plot=False)
        self.assertIsNotNone(pl)
        pl.close()

