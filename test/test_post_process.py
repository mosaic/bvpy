import unittest

import numpy as np

from bvpy.utils.post_processing import *
from bvpy import BVP
from bvpy.vforms import PoissonForm
from bvpy.domains import CustomDomain
from fenics import (UnitSquareMesh, FunctionSpace, VectorFunctionSpace, project, Constant)


class TestPreProcess(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.mesh = UnitSquareMesh(2,2)
        self.V = FunctionSpace(self.mesh ,'P', 1)
        self.sol = project(Constant(1), self.V)


    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_sol_explorer(self):
        solexplo = SolutionExplorer(self.sol)
        solexplo.get_dofs_positions()
        solexplo.to_vertex_values()
        # solexplo.to_dataframe()

    def test_sol_explorer_scal(self):
        c = fe.Constant(1)
        u = fe.project(c, self.V)
        u.vector()[0] = 0
        m = SolutionExplorer(u)
        mask =(m.get_vertex_positions()==m.get_dofs_positions()[0]).all(axis=1)
        self.assertEqual(m.to_vertex_values()[mask], 0)

    def test_sol_explorer_vec(self):
        VV = fe.VectorFunctionSpace(self.mesh, 'P', 1)
        cc = fe.Constant([1,0])
        mm = SolutionExplorer(fe.project(cc, VV))
        np.testing.assert_almost_equal(mm.to_vertex_values()[0], [1,0])

    def test_sol_explorer_tensor(self):
        VVV = fe.TensorFunctionSpace(self.mesh, 'P', 1, shape=(3,3))
        ccc= fe.Constant(np.diag([1,1,1]))
        uuu = fe.project(ccc, VVV)
        mmm = SolutionExplorer(uuu)
        np.testing.assert_almost_equal(mmm.to_vertex_values()[0], np.diag([1,1,1]))

    def test_sol_explorer_geom_filter(self):
        c = fe.Constant(1)
        u = fe.project(c, self.V)
        u.vector()[0] = 0
        m = SolutionExplorer(u)
        arr = m.geometric_filter(threshold=0.7, comparator=">", axe="y")
        self.assertEqual(len(arr), 3)

    def test_sol_explorer_label_filter(self):
        c = fe.Constant(1)
        u = fe.project(c, self.V)
        u.vector()[0] = 0
        m = SolutionExplorer(u)
        mf = fe.MeshFunction('size_t', self.mesh, 2, 0)
        mf[3] = 1
        mf[7] = 1
        arr = m.cell_label_filter(mf, 1)
        self.assertEqual(len(arr), 5)

    def test_relative_error(self):
        sol2 = project(Constant(1.1), self.V)
        err = relative_error(self.sol, sol2, self.mesh)

        expected = np.array([2*abs(-0.1)/(1+1.1) for i in range(9)])

        np.testing.assert_allclose(err , expected, atol=1e-15)

    def test_estimate_precision(self):
        sol2 = project(Constant(1.1), self.V)
        domain = CustomDomain(self.mesh.coordinates(), self.mesh.cells(), dim=2)
        vform = PoissonForm()
        bvp = BVP(domain, vform)
        bvp.solution = self.sol

        expected = np.array([2*abs(-0.1)/(1+1.1) for i in range(9)])
        err = estimate_precision(bvp, '1.1')
        np.testing.assert_allclose(err , expected, atol=1e-15)

        err = estimate_precision(bvp, '1.1', 'expression')
