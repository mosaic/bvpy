import unittest
import numpy as np
import fenics as fe
# -- test class

from bvpy.vforms import TransportForm, CoupledTransportForm
from bvpy.utils.pre_processing import create_expression
from bvpy.solvers.time_integration import ImplicitEuler
from bvpy.domains import Rectangle
from bvpy.boundary_conditions import dirichlet
from bvpy.boundary_conditions.periodic import SquarePeriodicBoundary
from bvpy import BVP, IBVP

from bvpy import logger


class TestTransport(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_TransportFrom_init(self):
        vform = TransportForm(diffusion_coef=2,
                              source=1,
                              velocity=[1, 0, 0],
                              reaction=1)
        self.assertEqual(vform._parameters['diffusion_coef'].values()[0], 2)
        self.assertEqual(vform._parameters['source'].values()[0], 1)
        self.assertEqual(vform._parameters['velocity'].values().all(),
                         np.array([1, 0, 0]).all())
        self.assertEqual(vform._parameters['reaction'].values()[0], 1)

    def test_TransportFrom_init_heterogeneous_diffusion(self):
        vform = TransportForm(diffusion_coef=create_expression('x', degree=1),
                              source=create_expression('x', degree=1),
                              velocity=[1, 0, 0])
        prblm = BVP(domain=Rectangle(),
                    vform=vform,
                    bc=dirichlet(0, boundary='all'))
        prblm.solve()
        self.assertIs(type(vform._parameters['diffusion_coef']), fe.Expression)
        self.assertIs(type(vform._parameters['source']), fe.Expression)

    def test_TransportForm_PoissonLike(self):
        prblm = BVP(domain=Rectangle(),
                    vform=TransportForm(source=-6),
                    bc=dirichlet('1 + x*x + 2*y*y', boundary='all'))
        prblm.solve()
        self.assertEqual(prblm.solution.compute_vertex_values().all(),
                         np.array([1, 2, 4, 3, 2]).all())

    def test_TransportForm_reactionTerm(self):
        def reaction(x):
            return -2*np.pi**2*x

        vform = TransportForm(reaction=reaction)
        vform.set_element_order(4)

        prblm = BVP(domain=Rectangle(cell_size=.05),
                    vform=vform,
                    bc=dirichlet('sin(pi*x)*sin(pi*y)', boundary='all'))
        prblm.solve()

    def test_CoupledTransportForm_init(self):
        dom = Rectangle(length=10, width=10, dim=2)
        dom.discretize()

        k1 = 9
        k2 = 11
        dt = 0.05

        vform = CoupledTransportForm()
        vform.add_species('A', 1, lambda a, b: k1*(b-(a*b)/(1+b*b)))
        vform.add_species('B', 0.02, lambda a, b: k2-b-(4*a*b)/(1+b*b))

        vform.set_expression()
        vform.info()

        random = np.random.uniform(low=-1,
                                   high=1,
                                   size=dom.mesh.num_entities(2))
        state = [[1+0.04*k1**2+0.1*r, 0.2*k2+0.1*r] for r in random]
        init = create_expression(state)

        periodic_bc = SquarePeriodicBoundary(length=10, width=10)
        x = [10, 10]
        y = [10, 0]
        z = [10, 0]
        periodic_bc.map(x, z)
        periodic_bc.map(y, z)

        ibvp = IBVP(domain=dom,
                    vform=vform,
                    bc=periodic_bc,
                    scheme=ImplicitEuler,
                    initial_state=init)

        ibvp.integrate(0, 1, dt,
                       linear_solver='mumps',
                       absolute_tolerance=1e-10,
                       relative_tolerance=1e-10, report=True)
