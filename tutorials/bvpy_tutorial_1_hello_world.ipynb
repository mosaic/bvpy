{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# First BVPY tutorial: The Poisson equation.\n",
    "\n",
    "\n",
    "In this first tutorial, you will discover the most basic commands and concepts of the **Bvpy** library by addressing the \"*Hello world!*\" of numerical estimation: **The Poisson problem**. This problem corresponds to the following *elliptic Partial Differential Equation*: $\\nabla^2 u(\\mathbf{x}) = f(\\mathbf{x})$ coupled with *Dirichlet* boundary conditions (*i.e.* constrains on the value of the seeked function $u(\\mathbf{x})$ on the border of the integration domain).\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- Basic classes: `Rectangle()` (domain), `PoissonForm()` (vform), `BVP()` and main methods/functions: `.solve()`, `.info()`, `save()`, `plot()`...\n",
    "\n",
    "- Error analysis.\n",
    "\n",
    "Download the tutorial notebooks and accompanying data as a ZIP file: [tutorials.zip](https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.zip)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mathematically the Poisson problem reads:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\left\\{\n",
    "\\begin{array}{rl}\n",
    "-\\nabla^2 u(\\mathbf{x}) = f(\\mathbf{x}) &\\text{in } \\Omega \\\\\n",
    "u(\\mathbf{x}) =  u_D(\\mathbf{x}) &\\text{on } \\partial\\Omega,\n",
    "\\end{array}\n",
    "\\right.\n",
    "\\end{equation*}\n",
    "\n",
    "where $\\Omega$ corresponds to the integration domain and $\\partial\\Omega$ to its border. $f(\\mathbf{x})$ is called the *source* function and needs to be specified prior to the cell_size. Similarly $u_D(\\mathbf{x})$ is the function that will prescribe the constrained values $u(\\mathbf{x})$ must verify on $\\partial\\Omega$.\n",
    "\n",
    "In order to show the usefulness of our approach, we will implement the same Poisson problem as the one tackle by [the first FEniCS tutorial](https://fenicsproject.org/pub/tutorial/html/._ftut1004.html). Meaning that we will use the following specific values and expressions for the various parameters of the problem:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\left\\{\n",
    "\\begin{array}{rl}\n",
    "\\Omega&=[0,1]\\times[0,1] \\\\\n",
    "f(\\mathbf{x})&=-6 \\\\\n",
    "u_D(\\mathbf{x}) &= 1 + x^2 + 2 y^2\n",
    "\\end{array}\n",
    "\\right.\n",
    "\\end{equation*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplest and quickest Poisson problem implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by creating the integration domain $\\Omega$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Rectangle\n",
    "rectangle = Rectangle(cell_size=0.05)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And to define the equation to solve on it:\n",
    "\n",
    "\\begin{equation*}\n",
    "\\begin{array}{ccc}\n",
    "-\\nabla^2u(\\mathbf{x}) = f(\\mathbf{x}) &\\text{with:}& f(\\mathbf{x})=-6.\n",
    "\\end{array}\n",
    "\\end{equation*}\n",
    "\n",
    "In the most general case one would have, at this point, to write a `Problem` class specific to the considered equation and to implement in it the variational form of this equation. But since the Poisson equation is a iconic one, its variational form is already implemented within the **Bvpy** library as a *built-in template*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import PoissonForm\n",
    "poisson = PoissonForm(source=-6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we define the boundary conditions. There are several ways to implement *BCs* in Bvpy. Here, in order to stick with the FEniCS first tutorial, we choose to implement them as a function ($u_D\\colon \\mathbf{x} \\mapsto 1+x^2+2y^2$) to apply all around the border of the domain ($\\partial\\Omega$):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import dirichlet\n",
    "diri = dirichlet('1 + x*x + 2*y*y', boundary='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** As mentionned in the FEniCS tutorial, the chosen function $u_D$ here is an exact solution of the considered Poisson equation. This will be useful to estimate the precision of our numerical estimation downrange."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We finally instanciate the Problem as follow:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy import BVP\n",
    "prblm_1 = BVP(domain= rectangle, vform=poisson, bc=diri)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And solve it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm_1.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A basic Poisson equation is therefore implemented and solved in less than ten lines of code within the **Bvpy** framework.\n",
    "\n",
    "**Bvpy** also offers some visualization tools to quickly assess the simulation results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.visu import plot\n",
    "\n",
    "plot(prblm_1.solution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In its current state, **Bvpy** provides minimalist visualization and analysis tools. One may want to turn to other frameworks, such as **Paraview** for instance, to visualize and analyze simulation results. To do so, **Bvpy** enable the recording of simulation in the `.xdmf` format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.io import save\n",
    "save(prblm_1, 'tutorial_data/tuto_1_result_1.xdmf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since $u_D(\\mathbf{x})$ is an exact solution of the considered Poisson equation, we can assess the precision of the computation by comparing the generated results and the theoretical prediction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.post_processing import estimate_precision\n",
    "error = estimate_precision(prblm_1, '1 + x*x + 2*y*y')\n",
    "\n",
    "print(f'Maximal discrepency between expectation and computed solution: {max(error):.2e}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also plot a quick visual representation of the spatial distribution of the error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.visu import plot\n",
    "error_distribution = estimate_precision(prblm_1, '1 + x*x + 2*y*y', output='expression')\n",
    "plot(error_distribution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First attempt to play a bit with the simulation parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now look at the simplest modifications we can perform.\n",
    "We can first check all the properties and parameter values of our simulation, as follow:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm_1.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.info()` method is implemented within Domain classes, Vform classes BVP and IBVP classes. It is a simple way to visualize many information concerning the parametrization of the instanced objects.\n",
    "\n",
    "We see here above that the *FEs* we used are of order 1. This is because, by default, *FEs* implemented within the `PoissonForm` class are first order. In the Poisson-related FEniCS tutorial, the implemented *FEs* are of second order.\n",
    "\n",
    "We can modify the *FEs* order with a *setter* inherited from the `AbstractVform` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poisson_2 = PoissonForm(source=-6)\n",
    "poisson_2.set_element_order(2)\n",
    "\n",
    "poisson_2.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now implement the corresponding problem and solve it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm_2 = BVP(domain= rectangle, vform=poisson_2, bc=diri)\n",
    "prblm_2.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check if it has an influence on the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "error = estimate_precision(prblm_2, '1 + x*x + 2*y*y')\n",
    "error_distribution = estimate_precision(prblm_2, '1 + x*x + 2*y*y', output='expression')\n",
    "\n",
    "print(f'Maximal discrepency between expectation and computed solution: {max(error):.2e}')\n",
    "plot(error_distribution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And this time, indeed, we get a very accurate result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inter-operability with FEniCS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**BVPy** goal is not only to provide a clear and simple *API* on top of the **FEniCS** library but also to make this *API* as transparent as possible. Users already experienced in **FEniCS** can therefore easily access familiar objects they are used to.\n",
    "\n",
    "To that end, many attributes from the main **BVPy** classes are **FEniCS** objects. Let's review the main ones."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### FEniCS mesh within the Domain class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider the `rectangle` domain previously defined and check the kind of attributes it contains:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"rectangle is of type {type(rectangle)} \\n\")\n",
    "\n",
    "for name, attribute in rectangle.__dict__.items():\n",
    "    print(f\"{name} is of type {type(attribute)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see from above, some attributes of the `Domain` class are **Dolfin** object. In particular, one can easily access the **FEniCS** mesh within the domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fenics as fe\n",
    "\n",
    "fe_mesh = rectangle.mesh\n",
    "isinstance(fe_mesh, fe.Mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Developpers used to the **FEniCS** *API* can benefit from it. For instance if one wants to get the positions of the vertices of the mesh defined on the domain, one can use the *classic* functions `fenics.vertices()` and `mesh.coordinates()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "points = np.array([fe_mesh.coordinates()[vtx.index()][:2] for vtx in fe.vertices(fe_mesh)]).transpose()\n",
    "\n",
    "plt.scatter(points[0], points[1])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "> **Note:** The `cdata` and `bdata` attributes are `MeshFunctionSizet`, *i.e.* **FEniCS** functions defined on a given mesh returning integer values. They can be useful for parametrization, as we will see in tutorial [Linear Elasticity](bvpy_tutorial_6_linear_elasticity.html)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### FEniCS function space within the BVP class "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another important concept in *FEM* is the one of *function space*. This concept is implemented within the **FEniCS** library within the `fenics.FunctionSpace` class, inherited from the **Dolfin** class `dolfin.functions.functionspace.FunctionSpace`. \n",
    "\n",
    "Within **BVPy** the instanciation of a function space is done in two steps:\n",
    "* First, when the `vform` is instanced, one can define the family, degree and type of elements to consider, *e.g.* `someForm._elements = [Element('P', 1, 'scalar')]`.\n",
    "* Then, once the corresponding `BVP` is instanciated, the `set_vform` methods instanciates the `fenics.FunctionSpace` to consider according the the various elements defined in the previous step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fe_func_space = prblm_1.functionSpace\n",
    "if isinstance(fe_func_space, fe.FunctionSpace):\n",
    "    print(\"There is a FEniCS function space deep inside the BVP class!\")\n",
    "    print(f\"it is of dimension {fe_func_space.dim()}\")\n",
    "    print(f\"and contains elements of type {fe_func_space.ufl_element()}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also get this function space, once the simulation is done by considering the solution of the considered `BVPy`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sol = prblm_1.solution\n",
    "isinstance(sol.function_space(), fe.FunctionSpace)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** For users not at ease with the **FEniCS** *API*, the `SolutionExplorer` class from the `utils.post_processing` module is designed to handle the conversion between **FEniCS** objects and *more usual* ones such as `numpy.nDarray`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.20"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
