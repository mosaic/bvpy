{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# General introduction to Bvpy\n",
    "This tutorial sets the context and the extent of use of the **Bvpy** library.\n",
    "\n",
    "**Bvpy in a nutshell:** Bvpy is a python library, based on [FEniCS](https://fenicsproject.org) and [Gmsh](https://gmsh.info), to easily implement and study numerically Boundary Value Problems as well as Initial Boundary Value Problems (*BVPs* and *IBVPs* for short) through the Finite Element Method (*FEM*)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Differential problems at stake\n",
    "\n",
    "**TODO:** State here the focus of the library -> Differential calculus is a huge domain of investigation -> The scope of the present library is (very) limited to biophysically relevant types of differential equations, *i.e.* no more than 4D variable space (in practice mostly 2D, spatial surfaces and 3D = 2D spatial surfaces plus time).\n",
    "\n",
    "**Note:** Bvpy has initially been designed to address biophysical-related problems where the variable space is a subset of either the usual *spatial* 3D Euclidean space ($\\mathbb{R}^3$) or the usual *spatio-temporal* 3D space ($\\mathbb{R}^+\\times\\mathbb{R}^3$).\n",
    "\n",
    "### Boundary-value problems\n",
    "A *Boundary-Value Problem* (*BVP* for short) is composed by:\n",
    "* A **differential equation**, or set of equations.\n",
    "* A **compact integration domain** where this equation is defined\n",
    "* A set of **boundary conditions** constraining the equation solutions on the borders of the integration domain.\n",
    "\n",
    "#### Differential equations\n",
    "Depending on the dimensionality of the considered variable space, two types of differential equations are usually defined:\n",
    "\n",
    "**Ordinary Differential Equaitons (*ODEs*)**\n",
    "correspond to differential equations featuring functions (and their derivatives) of one single independent variable. $$F(x, u(x), u^{\\prime}(x), ..)=0,$$ where $u^{\\prime} = du/dx$.\n",
    "\n",
    "**Partial Differential Equations (*PDEs*)**\n",
    "correspond to differential equations featuring functions (and their derivatives) of at least two independent variables. $$F(\\mathbf{x},  u(\\mathbf{x}), \\nabla(u(\\mathbf{x})))= 0 \\label{eq:eqdiff},$$ where $\\mathbf{x} = \\{x^0, x^1, ..., x^n\\}$ and $\\nabla(u)= \\{\\partial u / \\partial x^0, ...,\\partial u / \\partial x^n\\}$.\n",
    "\n",
    "**Note:** In practice, Bvpy has always been used to study *PDEs*. *ODEs* study is possible within the framework but so far no dedicated template domain and/or variational form have been implemented.\n",
    "\n",
    "#### Integration domain\n",
    "The integration domain, usually noted $\\Omega$, corresponds to the the compact subset where the solutions of the considered differential equation(s) are defined.\n",
    "* For *ODEs*, the integration domain is a compact of the real set: $\\Omega \\subset \\mathbb{R}$.\n",
    "* For *PDEs*, $\\Omega \\subset \\mathbb{R}^n$ with $n>1$ where n corresponds to the number of independant variables.\n",
    "\n",
    "#### Boundary conditions\n",
    "Boundary conditions (*BCs*) correspond to constrains on either the solution(s) or its(their) derivative(s) on the border of the integration domain, noted $\\partial \\Omega$. Within the Bvpy library, we restrict ourselves to the three main types: *Dirichlet*, *Neumann* and *mixed* boundary conditions:\n",
    "\n",
    "* **Dirichlet** *BCs* correspond to constrains on the solution values: $$u(\\mathbf{x})=u_{\\text{D}}(\\mathbf{x})\\: \\forall \\mathbf{x} \\in \\partial \\Omega,$$ where $g_{\\text{D}}(\\mathbf{x})$ is a given function. \n",
    "\n",
    "\n",
    "* **Neumann** *BCs* correspond to constrains on the solution derivatives values: $$\\nabla(u(\\mathbf{x}))\\cdot\\hat{\\mathbf{n}}(\\mathbf{x})=j_{\\text{N}}(\\mathbf{x})\\: \\forall \\mathbf{x} \\in \\partial \\Omega,$$ where $g_{\\text{N}}(\\mathbf{x})$ is a given function and $\\hat{\\mathbf{n}}$ marks the outward normal of the domain at the considered point $\\mathbf{x} \\in \\partial \\Omega$.\n",
    "\n",
    "\n",
    "* **Mixed** *BCs* correspond to the application of Dirichlet and Neumann *BCs* on subsets of the domain border:\n",
    "$$\n",
    "\\begin{cases}\n",
    "u(\\mathbf{x})=u_{\\text{D}}(\\mathbf{x}) & \\forall \\mathbf{x} \\in \\partial \\Omega_D \\\\\n",
    "\\nabla(u(\\mathbf{x}))\\cdot\\hat{\\mathbf{n}}(\\mathbf{x})=j_{\\text{N}}(\\mathbf{x}) & \\forall \\mathbf{x} \\in \\partial \\Omega_N,\n",
    "\\end{cases}\n",
    "$$\n",
    "with: $\\partial \\Omega_N \\cup \\partial \\Omega_D = \\partial \\Omega$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initial-boundary-value problems\n",
    "*Initial-Boundary-Value Problems* (*IBVP* for short) correspond to a subclass of *BVPs* where the integration domain is foliated, *i.e.*: $\\Omega = [t_0, t_{\\infty}]\\times\\Sigma \\subset \\mathbb{R}^n$, with $n>1$. Of course this type of integration domain is particularly well-suited for *real-world applications* where the obvious foliation sets time ($t$) as the normal undimensional variable and space as the tangent hyperplane.\n",
    "\n",
    "####  General form of the considered differential equations\n",
    "In its current version, Bvpy scope on *IBVPs* is restricted to first-order time-dependent *PDEs* of the form:\n",
    "$$\n",
    "\\partial_tu(t,\\mathbf{x}) = F(t,\\mathbf{x}, u(t, \\mathbf{x}), \\nabla(u(t, \\mathbf{x})))\n",
    "$$\n",
    "This means that wave equations are, so far, out of the scope of the Bvpy library.\n",
    "\n",
    "#### Initial conditions\n",
    "Initial conditions (*ICs*) correspond to partial boundary conditions along the time-direction of the integration domain. Such conditions are partial for they only constrain the system at the initial time ($t_0$) considered. A well-posed *IBVP* requires one initial condition and boundary conditions along its whole spatial domain border, *e.g.* in the case of Dirichlet BCs only we write:\n",
    "$$\\begin{cases}\n",
    "u(t_0, \\mathbf{x}) = u_0(\\mathbf{x}) & \\forall \\mathbf{x} \\in\\Omega \\\\\n",
    "u(t,\\mathbf{x}) = u_D(t, \\mathbf{x}) & \\forall (t, \\mathbf{x}) \\in [t_0, t_{\\infty}]\\times\\partial\\Omega_D \\\\\n",
    "\\end{cases}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The finite element method\n",
    "The *Finite Element Method* or *Finite Element Analysis* (*FEM\\FEA* for short), is a numerical method for estimating the solutions of *BVPs*. The idea is to look for an approximated solutions $\\tilde{u} \\colon \\mathbf{x} \\to \\tilde{u}(\\mathbf{x})$ in a **function space** of finite dimension (noted $\\mathbb{V}_{\\Omega}$ hereafter). In such cases, the differential equation at stake can be approximate by an algebraic one that can be numerically integrated. \n",
    "\n",
    "Implementation of *FEM* for a given *BVP* therefore relies on three main steps:\n",
    "* Setting the function space of the approximated solutions we are looking for.\n",
    "* Converting the differential equation into an algebraic formulation.\n",
    "* Integrating the obtained algebraic equation over the considered domain.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "### The space of approximated solutions\n",
    "Such functions can be expanded onto a base ($\\{ f_i\\}$) of $\\mathbb{V}_{\\Omega}$:\n",
    "$$\\forall \\tilde{u} \\in \\mathbb{V}_{\\Omega},\\quad \\tilde{u} = \\sum_{i} \\tilde{u}^i f_i, \\quad \\tilde{u}^i \\in \\mathbb{R}.$$\n",
    "\n",
    "To construct $\\mathbb{V}_{\\Omega}$, the first step is to split the integration domain $\\Omega$ into small pieces $\\{\\omega_i\\}$, the so-called *finite elements*. The size of these pieces is chosen so that one can assume that the solutions of the *BVP* we are looking for vary smoothly on them. Such an integration domain divided into adjacent finite elements ($\\Omega = \\cup_i \\omega_i$) is called a **mesh**.\n",
    "\n",
    "Once $\\Omega$ is divided into *FEs*, one can construct smooth, piecewize-linear, functions such that:\n",
    "$$\n",
    "\\forall i, f_i(\\mathbf{x})\n",
    "\\begin{cases}\n",
    "    \\in \\left[0,1\\right] & \\text{if} \\quad \\mathbf{x} \\in \\omega_i \\\\\n",
    "    = 0 & \\text{else.}\n",
    "\\end{cases}\n",
    "\\label{eq:baseFunctionDefinition}\n",
    "$$\n",
    "The smoothness of these functions must be chosen by the user depending on the precision requirements of the problem at hand. In most cases, one would consider linear (called *P1*) or harmonic (*P2*) functions; but other types are available.\n",
    "\n",
    "One crucial condition, for these functions to form a base of $\\mathbb{V}_{\\Omega}$ is to be squared-integrable on $\\Omega$, *i.e.* $$\\forall i, f_i \\in L^2(\\Omega) \\Leftrightarrow \\int_{\\Omega}|f_i(\\mathbf{x})|^2d\\mathbf{x} < \\infty.$$ \n",
    "\n",
    "By definition the constructed base functions and therefore the functionspace where we are going to project the solution of the considered *BVP*, depends on two things: *(i)* the discretization we made of the integration domain, see defintion \\ref{eq:baseFunctionDefinition}; *(ii)* the smoothness of the functions we want to consider.\n",
    "\n",
    "### From differential to algebraic equations\n",
    "From an algebraic perspective, the approximated solution $\\tilde{u}$ we are looking for is a vector of $\\mathbb{V}_{\\Omega}$ entirely defined by its components $\\tilde{\\mathbf{u}} = \\left[\\tilde{u}^1, ..., \\tilde{u}^n\\right]^t$. The *FEM* can therefore be seen as a way to approximate a differential equation by an algebraic one within a specific function space $\\mathbb{V}_{\\Omega}$:\n",
    "$$\n",
    "\\begin{array}{c}\n",
    "F(\\mathbf{x}, u(\\mathbf{x}), \\nabla(u(\\mathbf{x})))=0 \\\\\n",
    "\\Downarrow\\\\\n",
    "\\mathbf{A}\\cdot\\tilde{\\mathbf{u}} = \\mathbf{b} \n",
    "\\end{array}\n",
    "\\label{eq:algebraicEquation}\n",
    "$$\n",
    "where the matrix $\\mathbf{A}$ and vector $\\mathbf{b}$ are directly derived from $F$ expression and the considered boundary conditions. \n",
    "Although the complete derivation behind eq.($\\ref{eq:algebraicEquation}$) is beyond the scope of this tutorial (see any introductiory textbook about *FEMs*, *e.g.* the [FEniCS book](https://launchpadlibrarian.net/83776282/fenics-book-2011-10-27-final.pdf)), it is nonetheless worth mentionning the first step, for it plays an explicit role in the Bvpy *modus operandi*: The variational formulation.\n",
    "\n",
    "### The vform of a *BVP*\n",
    "In order to derive the algebraic formulation of any *BVPs* one needs to express the differential equation eq.($\\ref{eq:eqdiff}$) in its **variational formulation** (*vform* for short). Such *vform* reads:\n",
    "$$\n",
    "\\begin{array}{cc}\n",
    "a(u, v) = L(v) & \\forall v \\in \\hat{\\mathbb{V}}_{\\Omega},\n",
    "\\end{array}\n",
    "\\label{eq:vform}\n",
    "$$\n",
    "where $u \\in \\mathbb{V}_{\\Omega}$ is the solution we are looking for and $\\hat{\\mathbb{V}}_{\\Omega}$ is the *test functionspace*, *i.e.* a subset of $\\mathbb{V}_{\\Omega}$ where function vanishes on $\\partial\\Omega$. \n",
    "The idea is that eq.($\\ref{eq:eqdiff}$) yields eq.($\\ref{eq:vform}$) through multiplication by $v$ and integration by part.\n",
    "\n",
    "\n",
    "### Solving the problem\n",
    "This is done through the use of an integration scheme and a solver. This is handled by **Fenics**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "author": "u",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "nteract": {
   "version": "0.24.0"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
