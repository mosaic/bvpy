{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Variation forms\n",
    "\n",
    "\n",
    "This this tutorial, we will look a bit more into details of the `bvpy.vform` module and the corresponding *Vform* classes. The goal is to discover the main methods and attributes of such classes and to understand how to create it own.\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- Basic `vform` manipulations: instanciation, `.info()` and `.set_parameters()` methods.\n",
    "\n",
    "- Implementation of *de novo* `vform`: Definition of the variational formulation of a differential equation, instanciation of mandatory `@staticmethods`: `.construct_form()` and `.set_expression`.\n",
    "\n",
    "Download the tutorial notebooks and accompanying data as a ZIP file: [tutorials.zip](https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.zip)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vform basic manipulation\n",
    "\n",
    "Available *vforms* are all gathered within the the `bvpy.vforms` module. They are organized within this module in theme-related sub-modules. For instance the `bvpy.vforms.elasticity` sub-module contains classes that encode equations describing various types of elastic behaviors.\n",
    "\n",
    "Similar to the *domain* classes, the *vform* classes have an `info()` method that helps getting insights. Let's illsutrate this within the case of the `HelmholtzForm`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import HelmholtzForm\n",
    "\n",
    "eq = HelmholtzForm()\n",
    "eq.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The expression of the variational form implemented within the considered `vform` is given on the last line. One can get from it the vform parameters that can be set at instanciation. In the current case, there is only one: `source`.\n",
    "\n",
    "One can change the values of the parameters witht the `set_parameters` method. Several types of parameters can be used:\n",
    "* scalars (int or float)\n",
    "* vectors (list of int or float)\n",
    "* string expression (i.e. `x*x` to state that the parameter is an harmonic function of the x spatial coordinate)\n",
    "* function (i.e. `lambda u: u**2` to stata that the parameter is an harmonic function of the seeked solution function)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eq.set_parameters(source=\"x*x +y*z\", linear_coef=1.)\n",
    "eq.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The finite elements can also be modified thanks to the `set_element_family()` and the `set_element_order()` methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eq.set_element_family('DG')\n",
    "eq.set_element_order(4)\n",
    "eq.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation of a new *Vform* class\n",
    "\n",
    "Experienced users may want to study genuine *bvps* based on differential equations not already available within the library. To that end, new types of *Vforms* can easily be implemented. \n",
    "\n",
    "Let's review the process with the following example:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [*Helmholtz equation*](https://en.wikipedia.org/wiki/Helmholtz_equation):\n",
    "$$\n",
    " \\Delta u + k(\\mathbf{x})u +f(\\mathbf{x}) = 0 \\quad\\quad \\forall \\mathbf{x} \\in \\Omega\n",
    " \\tag{1}\n",
    " \\label{eq:hlmtz}\n",
    "$$\n",
    "> **Note:** In general, the coefficient of the first order term (second term in the *lhs* of eq.($\\ref{eq:hlmtz}$)) is constant. Within our framework, a spacial dependency comes for free."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Defining the variational form to implement\n",
    "\n",
    "The first thing to do is to compute the variational expression of eq.(1). This is done by multiplying eq.(1) by a trial function (noted $\\psi$ hereafter) and to integrate the resulting expression on the considered domain $\\Omega$:\n",
    "$$\n",
    "\\int_{\\Omega}d\\mathbf{x}\\Delta(u) v + \\int_{\\Omega}d\\mathbf{x}k(\\mathbf{x})uv + \\int_{\\Omega}d\\mathbf{x}f(\\mathbf{x})v = 0\n",
    "\\tag{2}\n",
    "\\label{eq:start_eq}\n",
    "$$\n",
    "\n",
    "Integration by part of the first term of the *rhs* of eq.(2) yields:\n",
    "$$\n",
    "\\int_{\\Omega}d\\mathbf{x}\\Delta(u)v =  \\Big[ \\nabla(u)v \\Big]_{\\partial\\Omega}-\\int_{\\Omega}d\\mathbf{x}\\nabla(u)\\nabla(v)\n",
    "\\tag{3}\n",
    "\\label{eq:ipp}\n",
    "$$\n",
    "\n",
    "Assuming the integrated term of the *lhs* of eq.(3) vanishes; combining eqs.(2 \\& 3) yields:\n",
    "\n",
    "$$\n",
    "\\int_{\\Omega}d\\mathbf{x}\\Big(\\nabla(u) \\nabla(v) - k(\\mathbf{x})uv\\Big) = \\int_{\\Omega}d\\mathbf{x}f(\\mathbf{x})v\n",
    "\\tag{4}\n",
    "\\label{eq:end_eq}\n",
    "$$\n",
    "\n",
    "where we moved the source term within the *lhs* in order to sort appart bilinear and linear terms.\n",
    "The integrant of the *lhs* and the one of the *rhs* will constitute respectively the bilinear ($B(u,v)$ hereafter) and the linear forms ($L(v)$ hereafter) of the `vform` to implement:\n",
    "\n",
    "$$\n",
    "B(u,v) = L(v) \\quad\\quad \\text{with:} \\quad\\quad\n",
    "\\begin{cases}\n",
    "B(u,v) = \\nabla(u) \\nabla(v) - kuv\\\\\n",
    "L(v) = f v\n",
    "\\end{cases}\n",
    "\\tag{5}\n",
    "\\label{eq:linbilin_terms}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### New *Vform* definition\n",
    "\n",
    "Any new *Vform* must inherit from the `AbstractVform` mother class; thereby it automatically benefits from a lot of *house-keeping* methods and attributes.\n",
    "\n",
    "The only mandatory methods to implement are:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* `construct_form(self, u, v, sol)`: In this method are defined the fenics expression of the bilinear and linear terms computed in eq.($\\ref{eq:linbilin_terms}$) in the previous section. This method is the beating heart of the new class and must be carrefully implemented. In the present case, it should read:\n",
    "```python\n",
    "def construct_form(self, u, v, sol):\n",
    "    self.lhs = fe.inner(fe.grad(u), fe.grad(v)) * self.dx\n",
    "    self.lhs -= self._parameters['linear_coef'] * fe.inner(u, v) * self.dx\n",
    "    self.rhs = self._parameters['source'] * v * self.dx\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* `set_expression(self)`: This method has a purely informative role. It provides the class with a string description of the variational form, close to eq.(4). This description is printed when the `.info()` method is called. In the present case, it should read:\n",
    "```python\n",
    "def set_expression(self):\n",
    "    self._expression = \"grad(u)*grad(v)*dx - linear_coef*u*v*dx\"\n",
    "    self._expression += \" = source*v*dx\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The various parameters featured by the variational form ($k$ and $f$ in eq.($\\ref{eq:linbilin_terms}$)) need to be defined as (\"hidden\") attributes of the new class, through the use of the `self._parameters` dictionnary. They can automatically be set by the `set_parameters(self, **kwargs)` of the `AbstractVform` class, that should be called at instanciation:\n",
    "```python\n",
    "self.set_parameters(source=source, linear_coef=linear_coef)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the last mandatory thing to set is the type of Finite element to use. This is done by setting, at instanciation, the \"hidden\" attribute `self._elements`. In our case, we choose:\n",
    "```python\n",
    "self._elements = [Element('P', 1, 'scalar')]\n",
    "```\n",
    "> **Notes:** \n",
    "> * The family and degree of *FE* can be modified afterwards with, respectively, the `set_element_family()` and the `set_element_order()` methods inherited from the `AbstractVform` class.\n",
    "> * The attributes `self._elements` is a list fo a variational form can feature several *FE* if for instance it corresponds to a system of differential equations where every solution has its own associated *FE*.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Following all these guidelines, the new *HelmholtzForm* class reads:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import pi\n",
    "import fenics as fe\n",
    "from bvpy.vforms.abstract import AbstractVform, Element\n",
    "\n",
    "class HelmholtzForm(AbstractVform):\n",
    "    def __init__(self, source=0, linear_coef=2*pi**2):\n",
    "        super().__init__()\n",
    "        self._elements = [Element('P', 1, 'scalar')]\n",
    "\n",
    "        self.set_parameters(source=source, linear_coef=linear_coef)\n",
    "\n",
    "    def construct_form(self, u, v, sol):\n",
    "        self.lhs = fe.inner(fe.grad(u), fe.grad(v)) * self.dx\n",
    "        self.lhs -= self._parameters['linear_coef'] * fe.inner(u, v) * self.dx\n",
    "        self.rhs = self._parameters['source'] * v * self.dx\n",
    "\n",
    "    def set_expression(self):\n",
    "        self._expression = \"grad(u)*grad(v)*dx - linear_coef*u*v*dx\"\n",
    "        self._expression += \" = source*v*dx\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need first to define a domain and some boundary conditions. Let's take a simple square domain with vanishing dirichlet conditions on its border:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Disk, Rectangle\n",
    "from bvpy.boundary_conditions import dirichlet\n",
    "\n",
    "domain = Rectangle(x= -.5, y=-.5, width=1, length=1, cell_size=.01, clear=True)\n",
    "fixed_bc = dirichlet(0, boundary=\"all\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, it's time to instanciate our newly defined *vform*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hlmtz = HelmholtzForm(linear_coef=50**2)\n",
    "hlmtz.add_point_source([0,0,0], 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The *point source* we added above acts as a perturbation necessary to enable the convergence of the `bvp.solve()` method toward a non-null solution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now combine the domain, the new vform and the boundary conditions into a *bvp* and compute the corresponding solution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy import BVP\n",
    "prblm = BVP(domain, hlmtz, fixed_bc)\n",
    "prblm.solve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.visu import plot\n",
    "from bvpy.utils.io import save\n",
    "\n",
    "plot(prblm.solution)\n",
    "save(prblm.solution, 'tutorial_data/tuto_3_result_1.xdmf')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
