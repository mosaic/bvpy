{
 "cells": [
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "\n",
    "# Visualization with `PyVista`\n",
    "\n",
    "This tutorial explores the use of `PyVista` for visualizing various objects within the `bvpy` library. While previous visualizations in `bvpy` primarily used `Plotly`, this tutorial demonstrates how `PyVista` can enhance the visualization experience. Users can still use `Plotly` if preferred; however, `PyVista` provides specific advantages in handling complex 3D meshes and volumetric data more efficiently.\n",
    "\n",
    "Download the tutorial notebooks and accompanying data as a ZIP file: [tutorials.zip](https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.zip).\n"
   ],
   "id": "c57d32315ff947c"
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "## 1. Visualizable Objects in `bvpy` with `PyVista`\n",
    "\n",
    "The `visualize` function in `bvpy` allows effective visualization of the following object types:\n",
    "\n",
    "- **Meshes/Domains**: Examine the spatial structure and configuration of domains.\n",
    "- **Boundary Conditions (Dirichlet)**: Verify the proper definition of boundary conditions.\n",
    "- **Parameters Fields**: Inspect parameter values repartition within a domain, e.g. Young's modulus distribution across various zones in an hyper-elasticity problem.\n",
    "- **Solution Fields**: Display results from solved simulations, such as scalar, vector, or field distributions across a spatial domain.\n",
    "\n",
    "The type of output visualization can be selected through the `visu_type` input argument.\n",
    "\n",
    "*Note: Visualization of tensor fields is currently not supported but will be introduced in a future release.*"
   ],
   "id": "a316fe34f491896a"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "outputs": [],
   "execution_count": null,
   "source": [
    "import pyvista as pv\n",
    "import math\n",
    "from bvpy.utils.visu_pyvista import visualize"
   ],
   "id": "afbf7befc1a34a8b"
  },
  {
   "cell_type": "markdown",
   "id": "feb4bcda-ba5f-4fe8-91b8-b895dc41fb34",
   "metadata": {},
   "source": [
    "### Mesh\n",
    "\n",
    "This section demonstrates how to visualize a basic mesh in `bvpy` with `PyVista`, providing an overview of its spatial structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6fd0fb4b-7f5d-4979-af33-ef96041d75cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains.primitives import Cylinder\n",
    "\n",
    "# - Parameters of the cylinder\n",
    "L, r = 2, 0.2\n",
    "\n",
    "# - Create a cylinder and set up some parameters for the mesh construction\n",
    "cylinder  = Cylinder(x=0, dx=L, dz=0, r=r, cell_type='triangle',\n",
    "                     cell_size=.1, algorithm='Frontal-Delaunay', clear=True)\n",
    "cylinder.discretize() # discretize (from Gmsh -> FeniCS)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eb7c4af4c10edee",
   "metadata": {},
   "source": [
    "Visualize the AbstractDomain object (a Cylinder here)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45c8981941e15904",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cylinder, visu_type='mesh')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eb5b9c3fd496d6d",
   "metadata": {},
   "source": [
    "Notice that it is possible to give directly the `FeniCS` Mesh object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b43a12b3-005d-4d2e-afc8-ce69eb00df43",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cylinder.mesh, visu_type='mesh', window_size=[600, 400]) # give the Mesh object and control the window size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1971be654ad339d",
   "metadata": {},
   "source": [
    "### Domain\n",
    "\n",
    "If multiple sub-domains are defined, you can visualize them by setting the `visu_type` input to `domain`."
   ]
  },
  {
   "metadata": {},
   "cell_type": "markdown",
   "source": [
    "\n",
    "As an example, we will define a custom geometry called `CutCylinder`. This geometry represents a cylinder that is divided into two distinct subdomains. For detailed steps on generating this geometry, refer to the tutorial [Generate advanced geometries using Gmsh](bvpy_tutorial_9_gmsh.html)."
   ],
   "id": "dc47158816d1ea4d"
  },
  {
   "metadata": {},
   "cell_type": "code",
   "outputs": [],
   "execution_count": null,
   "source": [
    "from bvpy.domains.abstract import OccModel, AbstractDomain\n",
    "\n",
    "class CutCylinder(AbstractDomain, OccModel):\n",
    "    \"\"\"A cylinder divided into two subdomains with unique labels.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    length : float\n",
    "        The total length of the cylinder along the x-direction.\n",
    "    radius : float\n",
    "        The radius of the cylinder\n",
    "    \"\"\"\n",
    "    def __init__(self, length=4.0, radius=.3, **kwargs):\n",
    "        super(CutCylinder, self).__init__(**kwargs)\n",
    "        self.geometry(length, radius)\n",
    "\n",
    "    def geometry(self, length, radius):\n",
    "        cylinder = self.factory.addCylinder(0, 0, 0, dx=length, dy=0, dz=0, r=radius)\n",
    "        circle = self.factory.addCircle(length/2, 0, 0, radius)\n",
    "        self.factory.rotate([(1, circle)], x=length / 2, y=0, z=0, ax=0, ay=1, az=0, angle=math.pi / 2)  # rotate the circle\n",
    "        curve_loop = self.factory.addCurveLoop([circle])\n",
    "        circ_surface = self.factory.addPlaneSurface([curve_loop])\n",
    "        outDimTags, _ = self.factory.fragment([(3, cylinder)], [(2, circ_surface)])\n",
    "\n",
    "        self.factory.synchronize()\n",
    "\n",
    "        right_surface, left_surface = outDimTags[1][1], outDimTags[2][1]\n",
    "        self.model.addPhysicalGroup(3, [left_surface], tag=1)\n",
    "        self.model.setPhysicalName(3, 1, \"Left_Subdomain\")\n",
    "        self.model.addPhysicalGroup(3, [right_surface], tag=2)\n",
    "        self.model.setPhysicalName(3, 2, \"Right_Subdomain\")"
   ],
   "id": "6547c0d4549458df"
  },
  {
   "cell_type": "markdown",
   "id": "674698ebdbd01f11",
   "metadata": {},
   "source": [
    "We can now initialize and visualize an instance of the `CutCylinder` class, which represents the custom geometry defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b6950b42f835af5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cut_cylinder = CutCylinder(length=L, radius=r,\n",
    "                           cell_type='tetra', algorithm='Frontal-Delaunay',\n",
    "                           cell_size=.1, clear=True)\n",
    "cut_cylinder.discretize()\n",
    "\n",
    "print(f\"The available sub-domains are {cut_cylinder.sub_domain_names}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be0836fa9004f4fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cut_cylinder, visu_type='domain')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66f4f0ee9e579385",
   "metadata": {},
   "source": [
    "Notice that it is possible to give directly the `FeniCS` MeshSizet object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5be726f73bf8fa0",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cut_cylinder.cdata, visu_type='domain', show_edges=False) # give the MeshSizet object and control the display of the edges"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5d23d182bc6da05",
   "metadata": {},
   "source": [
    "If you directly provide the `FeniCS` `MeshSizet` object, you can retrieve the sub-domain names by using the optional argument `dict_values`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c9d36506e6a17ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cut_cylinder.cdata, visu_type='domain', dict_values=cut_cylinder.sub_domain_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bf14848-0641-4826-b4a2-d9f760344d79",
   "metadata": {},
   "source": [
    "### Dirichlet boundary conditions\n",
    "\n",
    "In this section, we will visualize the Dirichlet boundary conditions to verify that they are correctly applied in the problem setup.\n",
    "We will use a simple linear elastic problem where the previously defined cylinder is stretched at its ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40f911dc-78ae-4b73-bd6d-65af44d5ed44",
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import dirichlet, Boundary\n",
    "from bvpy.utils.pre_processing import HeterogeneousParameter\n",
    "from bvpy.vforms import LinearElasticForm\n",
    "from bvpy.bvp import BVP\n",
    "\n",
    "### - Parameters - ###\n",
    "young_values = {1: .2, 2: .1} # young modulus according to the sub-domain label\n",
    "poisson = 0.3 # poisson coefficient\n",
    "\n",
    "### - Set up boundary conditions - ###\n",
    "bc1 = [dirichlet(val=[-3, 0, 0], boundary=\"near(x, 0, 1e-2)\")]\n",
    "bc2 = [dirichlet(val=[3, 0, 0], boundary=Boundary(\"near(x, L, 1e-2)\", L=L))]\n",
    "\n",
    "### - Set up model - ###\n",
    "heterogeneous_young = HeterogeneousParameter(cut_cylinder.cdata, young_values) # set young values for each sub-domain\n",
    "elastic_model = LinearElasticForm(young=heterogeneous_young, poisson=poisson)\n",
    "\n",
    "### - Build the problem - ###\n",
    "elastic_pb = BVP(cut_cylinder, elastic_model, bc1+bc2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "517946951f653aad",
   "metadata": {},
   "source": [
    "Visualize the boundary conditions (dirichlet)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "def488a2833689cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(elastic_pb, visu_type='dirichlet', scale=0.1, val_range=[0, 5])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8024e956-064a-432d-9875-8a7905abdcb9",
   "metadata": {},
   "source": [
    "### Parameters\n",
    "\n",
    "In this section, we visualize parameter fields like Young's modulus and Poisson's ratio, crucial for understanding simulation setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7793c8bac4680bf9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Available parameters from the weak form\n",
    "print(f\"Available parameters from the weak form : {list(elastic_pb.vform._parameters.keys())}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "467cf8b5-7433-4bac-a347-6f9f38f28360",
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(elastic_pb, visu_type='young')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad63e001-dd9c-448b-93cc-6a6e379b753f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do the same for the poisson parameter but change the camera view of the pyvista.Plotter\n",
    "pl = visualize(elastic_pb, visu_type='poisson') # get the pyvisya.Plotter\n",
    "pl.view_xz() # change the camera view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1d601c5-a57f-4426-b54b-cc9e89a67083",
   "metadata": {},
   "source": [
    "### Solution field\n",
    "\n",
    "When solving problems with `bvpy`, you will end up with solution that corresponds to scalar or vector field. You can use `visu_type='solution'` method to display them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3d84515-748e-4f9b-8e12-c2a5d6e0f05c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# - Solve the previous elastic problem\n",
    "elastic_pb.solve()\n",
    "\n",
    "# - Visualize the resulting displacement field\n",
    "visualize(elastic_pb, 'solution', scale=0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b965cf59-d94e-4b74-b711-3e3468f37480",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(elastic_pb.solution))\n",
    "\n",
    "# - You can also visualize any FeniCS Function\n",
    "visualize(elastic_pb.solution, scale=0.1, show_mesh=False) # show directly the FeniCS Function & remove mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2de99ae6b9ab69ea",
   "metadata": {},
   "source": [
    "You can add multiple objects to the plotter using the optional `plotter` argument. This allows you to separately add a vector field and the associated mesh. In that case, it is required to set `show_plot=False` until the last object is displayed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e77fab2-6171-4cbb-a093-348545cd9b0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "pl = visualize(elastic_pb.solution, scale=0.1, show_mesh=False, show_plot=False) # show the FeniCS Function without the mesh\n",
    "visualize(elastic_pb.domain.mesh, plotter=pl, actor_kwargs={'style':'wireframe', 'line_width': 2}) # add the mesh as a 'wireframe' & specify the edge size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "958bf154-31bd-40ec-a0a3-bd82eeb4554d",
   "metadata": {},
   "source": [
    "## 2. Comparative visualization using multiple subplots\n",
    "\n",
    "In `PyVista`, you can visualize multiple subplots simultaneously, making it an excellent tool for comparative analysis of different parameters or results. By creating side-by-side or matrix-style views, you can quickly gain insights into how different variables interact within a simulation, all within the same visual context.\n",
    "\n",
    "In this example, we demonstrate a **cantilever beam experiment**. A cantilever beam is a structural element anchored at one end, with the other end extending freely. When a load is applied to the free end, the beam experiences bending, which generates stresses and displacements throughout its structure.\n",
    "\n",
    "For this simulation, the goal is to visualize key parameters, such as:\n",
    "\n",
    "* Young's Modulus: Which defines the material's stiffness and influences how much the beam will bend under loading.\n",
    "\n",
    "* Boundary Conditions: To confirm the correct application of constraints at the anchored end of the beam.\n",
    "\n",
    "* Displacement Field: Which shows how the beam physically deforms in response to the load.\n",
    "\n",
    "* Undeformed vs. Deformed Configuration: To observe how the mesh changes when the beam is under load, comparing its original shape with the displaced shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "354f8ef0-2f33-44ac-a077-f451c9abe24b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# - Some parameters\n",
    "young = {1: 0.5, 2: 1.5}\n",
    "poisson = 0.3\n",
    "g = 0.016\n",
    "rho = .1\n",
    "\n",
    "# - Make the simulation : cantilever experiment\n",
    "heterogeneous_young = HeterogeneousParameter(cut_cylinder.cdata, young)\n",
    "vform = LinearElasticForm(young=heterogeneous_young,\n",
    "                          poisson=poisson,\n",
    "                          source=[0, 0, -rho*g])\n",
    "bc = [dirichlet(val=[0, 0, 0], boundary=\"near(x, 0, 1e-2)\")]\n",
    "bvp = BVP(cut_cylinder, vform, bc)\n",
    "bvp.solve()\n",
    "\n",
    "# - Compute the deformed mesh\n",
    "deformed_cylinder = cut_cylinder.move(bvp.solution, return_cdata=False) # create a copy of the object & apply displacement field"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60f96a29-b395-45d3-974c-bd74b5819cd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# - Define a pv.Plotter with 6 subplots (shape = (nrow, ncol))\n",
    "pl = pv.Plotter(shape=(2, 3), window_size=[1000, 800])\n",
    "\n",
    "pl.subplot(0, 0) # active the subplot at row=0, col=0\n",
    "visualize(bvp, visu_type='dirichlet', val_range=[-0.5, 0.5],  plotter=pl, show_plot=False) # give the plotter and set show_plot to False.\n",
    "\n",
    "pl.subplot(0, 1) # activated the subplot at row=0, col=1\n",
    "visualize(bvp, visu_type='domain', plotter=pl, show_plot=False)\n",
    "\n",
    "pl.subplot(0, 2)\n",
    "visualize(bvp, visu_type='young', cmap='Pastel2', plotter=pl, show_plot=False)\n",
    "\n",
    "pl.subplot(1, 0)\n",
    "visualize(bvp, visu_type='mesh', plotter=pl, show_plot=False)\n",
    "pl.add_title('Mesh \\n(undeformed config.)', font_size=12) # add title after to control the fontsize\n",
    "\n",
    "pl.subplot(1, 1)\n",
    "visualize(bvp, visu_type='solution', title='Displacement field', plotter=pl, show_plot=False)\n",
    "\n",
    "pl.subplot(1, 2)\n",
    "visualize(deformed_cylinder, plotter=pl, show_plot=False)\n",
    "pl.add_title('Mesh \\n(deformed config.)', font_size=12) # add title after to control the fontsize\n",
    "\n",
    "pl.link_views() # link the camera of the subplots\n",
    "pl.show() # finally show the plot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "502481a6ce756d17",
   "metadata": {},
   "source": [
    "## 3. Advanced visualization options"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1cd028f30cc62ca9",
   "metadata": {},
   "source": [
    "### Visualize sub-space of `FeniCS` object"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58c88dced441eba8",
   "metadata": {},
   "source": [
    "Lets visualize some components of the stress tensor from the previous cantilever experiment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8b0e47b3808d048",
   "metadata": {},
   "outputs": [],
   "source": [
    "stress = vform.get_stress(bvp.solution)\n",
    "\n",
    "print(f\"The stress field is a tensor field of shape {stress.ufl_shape}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27f4e7c4595d0f24",
   "metadata": {},
   "source": [
    "Define optional visualization arguments for the scalar bar. See https://docs.pyvista.org/examples/02-plot/scalar-bars.html."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "145fc3ce7d3ce05c",
   "metadata": {},
   "outputs": [],
   "source": [
    "SCALAR_BAR_ARGS = dict(title_font_size=12,\n",
    "                              label_font_size=10,\n",
    "                              n_labels=3,\n",
    "                              italic=True,\n",
    "                              fmt=\"%.1e\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "634611b76a038552",
   "metadata": {},
   "source": [
    "Loop over the component xx, yy, zz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5619019b6d87755",
   "metadata": {},
   "outputs": [],
   "source": [
    "dict_sub = {'xx': 0, 'yy': 4, 'zz': 8} # FeniCS subspace index are organized in 1D --> xx, xy, xz, yx, yy, yz, zx, zy, zz\n",
    "\n",
    "pl = pv.Plotter(shape=(1, 3), window_size=[1000, 400])\n",
    "for ix, component in enumerate(dict_sub.keys()):\n",
    "    pl.subplot(0, ix)\n",
    "    visualize(stress.sub(dict_sub[component]), val_range=[-0.01, 0.01], plotter=pl, cmap='viridis', show_plot=False,\n",
    "              title=f'stress ({component})',  actor_kwargs={'scalar_bar_args': SCALAR_BAR_ARGS}) # add custom scalar bar\n",
    "\n",
    "pl.link_views()\n",
    "pl.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7a5faacfdbc7609",
   "metadata": {},
   "source": [
    "### Use `Pyvista` widgets to enhance visualization options"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce8999330d5618e4",
   "metadata": {},
   "source": [
    "Various `Pyvista` widgets can be used to help the visualization of `FeniCS` object. Feel free to consult the widgets from https://docs.pyvista.org/examples/03-widgets/index.html."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79aa7bcb54a5df16",
   "metadata": {},
   "source": [
    "Visualization of inner part of volumetric data can be done using Plane Widget:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed38c8e729addf54",
   "metadata": {},
   "outputs": [],
   "source": [
    "pl = visualize(bvp.solution, show_plot=False, show_mesh=False, show_scalar_bar=False, actor_kwargs={'show_glyph': False}) # do not show mesh & glyph (arrows)\n",
    "pl.add_mesh_clip_plane(pl.meshes[0], invert=True, assign_to_axis='y') # then retrieve the mesh and add a clip plane. Assign to one axis (can be deactivated)\n",
    "pl.add_title('Displacement field\\n(with clip plane)', font_size=10)\n",
    "pl.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.20"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
