.. image:: https://gitlab.inria.fr/mosaic/bvpy/badges/develop/coverage.svg
  :target: https://gitlab.inria.fr/mosaic/bvpy/-/commits/develop

.. image:: https://gitlab.inria.fr/mosaic/bvpy/badges/master/pipeline.svg
  :target: https://gitlab.inria.fr/mosaic/bvpy/pipelines

.. image:: https://anaconda.org/mosaic/bvpy/badges/platforms.svg
  :target: https://anaconda.org/MOSAIC/bvpy

.. image:: https://anaconda.org/mosaic/bvpy/badges/version.svg
  :target: https://anaconda.org/MOSAIC/bvpy/files

.. image:: https://img.shields.io/badge/license-LGPL3-black
  :target: https://www.gnu.org/licenses/lgpl-3.0.html

.. image:: https://anaconda.org/mosaic/bvpy/badges/latest_release_date.svg
  :target: https://anaconda.org/MOSAIC/bvpy

.. image:: https://anaconda.org/mosaic/bvpy/badges/downloads.svg
  :target: https://anaconda.org/MOSAIC/bvpy

.. toctree::
   :caption: Table of contents
   :maxdepth: 2
   :hidden:

   Home <self>
   general_introduction
   library_description
   tutorials
   Sources <_dvlpt/modules>


Welcome
-------

.. image:: https://joss.theoj.org/papers/10.21105/joss.02831/status.svg
   :target: https://doi.org/10.21105/joss.02831

**Bvpy** is a python library, based on `FEniCS <https://fenicsproject.org/>`_ and `GMSH <http://gmsh.info/>`_, to easily implement and study numerically Boundary Value Problems (BVPs) as well as Initial Boundary Value Problems (IBVPs) through the Finite Element Method (FEM).

Initially built up in the context of developmental biology and morphomechanics, **Bvpy** proposes an intuitive API as close as possible to the formal definition of BVPs. Its purpose is to enable users to quickly and efficiently estimate the behavior of a wide variety of fields (scalars, vectors, tensors) on biologically relevant structures (Riemannian and non-Rieamannian manifolds), inspired by biophysical and biochemical processes (morphogene patterning, active matter mechanics, diffusion-reaction processes, active transports...).

Despite this biological motivation, the **Bvpy** library has been implemented in an *agnostic* manner that makes it suitable for many other scientific context.


.. sidebar:: **Bvpy**

  :Lead Development: Manuel Petit

  :Coordination: Olivier Ali

  :Main Contributors: Florian Gacon

  :Other Contributors: Guillaume Cerutti, Adrien Heymans, Jonathan Legrand, Gonzalo Revilla

  :Active team: Inria project team `Mosaic <https://team.inria.fr/mosaic/>`_

  :Institutes: `Inria <http://www.inria.fr>`_

  :Language: Python

  :Supported OS: Linux, MacOS

  :Licence: `LGPL`

  :Funding: Inria ADT Gnomon (Christophe Godin)


Documentation
-------------

A quick introduction to boundary-value problems and Initial-Boundary-value problems, as well as a the general philosophy behind the **Bvpy** library development can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/general_introduction.html>`_.

A detailed description of the main classes and models of the library can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/library_description.html>`_.

Some tutorials explaining basic manipulations are gathered `here <https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.html>`_.


Requirements
------------

* Python 3.9
* FEniCS 2019.1.0
* GMSH 4.11


Installation
------------

You will need conda in order to install ``bvpy``. If you don’t have conda installed, you can download it  `here <https://docs.conda.io/en/latest/miniconda.html>`_.

.. tabs::

   .. tab:: Installing from Anaconda

    ``bvpy`` can be installed directly from the `Anaconda` distribution using the **conda package manager**. You have the flexibility to either create a new environment or install it into an existing one.

    1. Open a terminal or Conda environment prompt.

    2. Choose one of the following installation methods:

       - **Create a New Environment** (recommended):

         .. code-block:: bash

            conda create -n bvpy -c mosaic -c conda-forge bvpy
            conda activate bvpy

       - **Install into an Existing Environment**:

         .. code-block:: bash

            conda activate <your-environment-name>
            conda install -c mosaic -c conda-forge bvpy

    3. Verify the ``bvpy`` installation:

       .. code-block:: bash

          python -c "from bvpy.utils.examples import cantilever_beam; cantilever_beam()"

       This should produce a Pyvista Plotter showing the displacement field of the cantilever beam. If you see the correctly rendered visualization, the installation and dependencies of ``bvpy`` are working as expected.

    **Note**: Replace `<your-environment-name>` with the name of your existing environment.

   .. tab:: Installing from Docker

      To install ``bvpy`` using Docker, follow these steps:

      1. Pull the Docker image:

         .. code-block:: bash

            docker pull registry.gitlab.inria.fr/mosaic/bvpy:<TAG>

         Replace `<TAG>` with the desired version of ``bvpy``.
         You can find all available versions on the `repository <https://gitlab.inria.fr/mosaic/bvpy/container_registry/>`_.

      2. Run the Docker container and launch Jupyter Notebook:

         .. code-block:: bash

            docker run -it -p 8888:8888 registry.gitlab.inria.fr/mosaic/bvpy:<TAG>
            jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --port 8888

         Use the URL displayed in the terminal to open the Jupyter Notebook in your browser for running tutorials.

   .. tab:: Installing from Sources

      If you want to install ``bvpy`` from its source code (e.g., for development purposes or using the latest version), use the following steps:

      1. Clone the Repository:

      .. code-block:: bash

         git clone https://gitlab.inria.fr/mosaic/bvpy.git
         cd bvpy

      2. Create a Development Environment:

      Use the provided `conda` environment file to create a new development environment:

      .. code-block:: bash

         conda env create --file conda/env.yaml -n bvpy-dev

      After the environment is created, activate it:

      .. code-block:: bash

         conda activate bvpy-dev

      3. Install ``bvpy`` in Editable Mode:

      Install the repository in editable mode (using pip) to allow changes to the source code to take effect immediately:

      .. code-block:: bash

         python -m pip install -e .

      4. Verify the ``bvpy`` installation:

         .. code-block:: bash

            python -c "from bvpy.utils.examples import cantilever_beam; cantilever_beam()"

      This should produce a Pyvista Plotter showing the displacement field of the cantilever beam. If you see
      the correctly rendered visualization, the installation and dependencies of ``bvpy`` are working as expected.

      **Optional: Run Tests**

        If you installed ``bvpy`` from sources, install ``pytest`` and ``pytest-cov`` inside your environment:

        .. code-block:: bash

           python -m pip install -e ".[test]"

        Then, run the tests from the root of the project directory to ensure that everything works correctly:

        .. code-block:: bash

           pytest -v --cov=bvpy test/


.. The following content is commented out : no need
    **Important Note:** Bvpy is currently running on *FEniCS legacy* and not *FEniCSx*. If you are using an ARM-based computer (mac M1s, M2s), there is currently no ARM-Friendly conda package of *FEniCS legacy*. But you can still run x86-based packages. to do so type the following:
    .. code-block:: bash
      CONDA_SUBDIR=osx-64 conda create -n bvpy_x86
      conda activate bvpy_x86
      conda config --env --set subdir osx-64
    Once this is done, you can import manually all the required libraries, listed within the `conda/env_osx.yaml` configuration file.

Support
-------

If you encounter an error or need help, please `raise an issue <https://gitlab.com/oali/bvpy/-/issues>`_.

Contributing
------------

Bvpy is a is a collaborative project and contributions are welcome. If you want to contribute, please contact the `coordinator <mailto:olivier.ali@inria.fr>`_ prior to any `merge request <https://gitlab.com/oali/bvpy/-/merge_requests>`_ and
check the `gitlab merge request guidelines <https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html>`_ if needed.

Citation
--------

If you are using Bvpy in a published work, please use this bibtex entry as reference:

.. code-block::

  @article{Gacon2021,
  doi = {10.21105/joss.02831},
  url = {https://doi.org/10.21105/joss.02831},
  year = {2021},
  publisher = {The Open Journal},
  volume = {6},
  number = {59},
  pages = {2831},
  author = {Florian Gacon and Christophe Godin and Olivier Ali},
  title = {BVPy: A FEniCS-based Python package to ease the expression and study of boundary value problems in Biology.},
  journal = {Journal of Open Source Software}}

