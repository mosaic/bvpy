# User Guide

#### *BVP* & *IBVP* in a nutshell

By definition, a *BVP* encompasses three elements:

*  A (set) differential equation(s), $F(\dots)$ in eq.($\ref{eq:bvp}$) below.
* An integration domain ($\body$).
* A set of constrains defined on the boundaries of the integration domain ($\surf_N\cup\surf_D$).

$$
\newcommand{\euclid}[1]{\mathbb{R}^{#1}}
\newcommand{\pos}{\boldsymbol{x}}
\newcommand{\field}{u}
\newcommand{\source}{g}
\newcommand{\body}{\Omega}
\newcommand{\surf}{\partial\body}
\newcommand{\grad}[1]{\boldsymbol{\nabla}(#1)}

\begin{cases}
F(\field, \grad{\field} \dots) = 0 & \text{in} \quad \body \\
\field(\pos) = u_N(\pos) & \text{on} \quad \surf_N \\
\grad{\field(\pos)} = \boldsymbol{j}(\pos) & \text{on} \quad \surf_D
\end{cases}

\label{eq:bvp}
$$

In the case of dynamic systems, the integration domain can be split into a one-dimension line and a spatial hyperplane: $\body = \tline\times\body_{t_0}$. Where the spatial hyperplane $\body_{t_0}$ is amused not to evolve with time[^1]. In the case of differential equation featuring only first-order time derivatives, this variable separation yields the following formalization:
$$
\newcommand{\tline}{[t_0, t_{\infty}]}

\begin{cases}
\partial_t u = F(\field, \grad{\field} \dots) & \text{in} \quad \body \\
\field(t_0, \pos) = u_0(\pos) & \text{on} \quad \body_{t_0}\\
\field(t,\pos) = u_N(t,\pos) & \text{on} \quad \surf_N\\
\grad{\field(t,\pos)} = \boldsymbol{j}(t,\pos) & \text{on} \quad \surf_D
\end{cases}

\label{eq:ibvp}
$$
where $\surf_N$ and $\surf_D$ now stand respectively for $\tline\times\surf_{t_0N}$ and $\tline\times\surf_{t_0D}$.

> **Scope and restriction: ** In its current version, **Bvpy** only encompass first and second-order BVPs and, as depicted by eq.($\ref{eq:ibvp}$), first-order time derivative in the case of IBVPs.  

#### Bvpy general philosophy

Inspired by **FEniCS** approach and principle, the **Bvpy** library has been organized as close as possible to the *BVP*  and *IBVP* mathematical formulations given in eqs.($\ref{eq:bvp}\& \ref{eq:ibvp}$). The goal is to provide users, and especially non-experienced ones, with the most intuitive *API* possible.

To that end, the library is built around two main classes, named `BVP` and `IBVP`, that respectively encapsulate **FEniCS** implementations of eqs.($\ref{eq:bvp}\& \ref{eq:ibvp}$). These classes are parametrized by three main arguments that echoe the three *BVPs* components mentioned above. Instanciation of these arguments is handle through corresponding modules: `bvpy.domains`, `bvpy.vforms` and `bvpy.boundary_condition`. These two main classes combined with these three main modules constitute the backbone of **Bvpy**.

Numerical estimation of the instanced *BVPs* and *IBVPs* requires solver, those are handled by the `bvpy.solvers` module. Finally, a `bvpy.utils` module gathers housekeeping methods and minor classes organized into thematic sub-modules (*i.e.* `bvpy.utils.io`, `bvpy.utils.visu`...).

![Representation of the main modules and submodules forming the Bvpy library. \label{fig:schema}](../paper/fig1.png)

<center> Fig.1: Representation of the main modules and submodules forming the Bvpy library. </center>

## Library description

### Main classes

#### BVP

The `bvp` class is the main class of the library.

It features a minimalistic signature: `bvp(domain, vform, bc)` that matches exactly the three main elements on which the formal definition of a boundary-value problem relies. The three corresponding arguments `self.domain`, `self.vform` and `self.bnd_conds` are set during initiation. They can be adjusted afterwards with specific setters, respectively: `self.set_domain()`, `self.set_vform` and `self.add_boundary_condition()`.

The `self.set_vform()` method, called within `self.__init__()`, instanciates the functionspace bases on the mesh specified within the `domain` argument and the finite element characteristics set within the `vform` argument.

The central method of the `bvp` class is the `solve()` one. This method instanciates the `vform.lhs` and `vform.rhs`, set the **FEniCS** solver to use depending on the variational form at stake and runs the simulation. If the considered `vform` has a vanishing `rhs` term, a non-linear solver will be chosen ; otherwize, a linear one will be picked.

Once `bvp.solve()` is called, the argument `bvp.solution`, initially set to `None`, is updated with the `fenics.function` computed solution.

The `bvp.solution` can then be visualized with the `plot()` function of the `bvp.utils.visu` module (*c.f.* description of this module hereafter). A `self.info()` method gathers and displays all the parameters of the simulation. This method makes use of the `domain.info()` and `vform.info()` ones. To performed more in-depth analysis of the simulation results, the whole problem — with its solution — can be saved on disk in the **.xdmf** format with the `self.save_solution()` method. The  genericity of the **.xdmf** format enables then the analysis of the results with powerful softwares such as [paraview](https://www.paraview.org).

#### IBVP

The `ibvp` class inherits directly from the `bvp` one. Its signature features two additional arguments: `scheme` and `initial_state`.

The former corresponds to the time-integration scheme to use. It must corresponds to a daughter class of the `FirstOrderScheme` class within the  `bvpy.solvers.time_integration` module. Currently, the **bvpy** library features two such classes `ImplicitEuler` or `ExplicitEuler`.

The latter corresponds to the initial value from which the solution will be evolving. It must be given as a `fenics.function`. Note that the `self.initial_state` argument can be set after the `ibvp` initialization with the `ibvp.set_initial_solution()` method.

The main method of the `ibvp` class is `self.integrate()` that handles the time-integration loop. During each iteration of the loop, it perfoms two tasks:

* Run a *spatial resolution* of the problem at the current time as a *classic BVP*. This is performed by the `self.step()` method that calls the `self.solve()` method inherited from the `bvp` mother class.
* Record the solution of this *spatial resolution* as a "layer" in a temporal stack recorded as an **.xdmf** file.

### Modules

#### Domains

The `bvpy.domains` module contains the classes that can be used to implement the notion of integration domain, $\body$ in eq.($\ref{eq:bvp}$). It is built around the (abstract) mother class `AbstractDomain`. All *domain classes* used as arguments for *BVPs*/*IBVPs* inherits from it. The  `AbstractDomain` encompasses two complementary description of the integration domain:

* A `gmsh.model` called the *geometry* of the domain and set by the `geometry()` abstract method.
* A `fenics.mesh` called the *mesh* of the domain and produced by the `discretize()` method.

The notion of *geometry* and the related `geometry()`  method are of paramount importance, for each domain class corresponds to a specific implementation of the `geometry()` method:

* The classes contained within the `bvpy.domains.primitives` sub-module are built around  `geometry()` methods that construct basic geometric elements (such as rectangles, disk, spheres...) exclusively with **Gmsh** tools.
* The `CustomPolygonalDomain` class (contained within the  `bvpy.domains.custom_polygonal` sub-module) is built around a `geometry()` method that construct piecewise polygonal structures from lists of oriented points. This is also done thanks to **Gmsh** tools. The purpose of this class is to enable the use of cellularized structures as integration domains. This is an important feature in the context of modeling biological tissues and other multicellular continua.
* Finally, the `CustomDomain` class (within the `bvpy.domains.custom_domain` sub-module) features an empty `geometry()` method for its purpose is to define a domain from an already existing mesh, sorted as a `.ply`-formatted file.

>  Besides a *geometry* and a *mesh*, domain classes also encompasses `fenics.Measures` elements defined within the domain (`self.dx`) and onto its border (`self.ds`) and some *setter* methods to parametrize

#### Vforms

The goal of the `bvpy.vforms` module is to gather weak formulations of differential equations and systems that can be implemented in **Bvpy**. The corresponding *vform classes* constitute the second mandatory argument of the `bvp` and `ibvp` class signatures. Similarly to the `bvpy.domains` module, the `bvpy.vforms` module is built around one main (abstract) mother class `AbstractVform`, other  *vform classes* contained within sub-modules inherit directly from it.

The heart of the `AbstractVform` class is the `contruct_form()` abstract method. Its purpose is to instantiate, in **FEniCS**, the bilinear (`fenics.lhs`) and the linear (`fenics.rhs`) operators that constitute the variational form of the differential equation (or system) at stake. In that matter, this method has to be written *de novo* each time a new (set of) differential equation(s) is to be considered.

In the current version of **Bvpy**, the resulting daughter classes of `AbstractVform` are compiled in field-specific sub-modules :

* `bvpy.vforms.poisson` contains the simplest *vform class*,  the one that encodes the scalar Poisson equation, which stands as the "Hello world !" for numerical analysis.
* `bvpy.vforms.elasticity` encompasses two *vform classes*: `LinearElasticForm` and  `HyperElasticForm` implementing respectively linear and hyper- elastic equations.
* `bvpy.vforms.transport` contains two classes dedicated to transport equations: `TransportForm` and `CoupledTransportForm`. The latter specifically tackles the problem differential systems composed of several interdependent equations.

#### Boundary_conditions

The `bvpy.boundary_conditions` enables an easy implementation of boundary conditions through the use of the `dirichlet` function. This function takes two mandatory arguments : `val` and `boundary`. They respectively correspond to the value that one wants to impose on a boundary.

The `val` argument can have several types:

* `int` ,`float`, `list`, `tuple`, `numpy.ndarray` . These types will encode constant dirichlet conditions, *i.e.* a (list of) constant value(s) on a given (list of) subset(s) of the border.
* Non-constant dirichlet conditions will be encoded with  `val` arguments given as strings.

The `boundary` argument must be an instance of the `Boundary` class defined within the `bvpy.boundary_conditions.boundary` module. This class enables the definition of the boundary or a subset of it from a string expression.

#### Solvers

The `bvpy.solvers` module gathers two types of finite element solvers: `LinearSolver`  and `NonLinearSolver`. They are not ment to be set by the user directly. The choice between both types is made automatically by the `bvp` class depending on the variational form at stake. More precisely, if the instanced `vform` features a non-zero `rhs` element a `NonLinearSolver` will automatically instanced.

Besides the finite element solvers, the `bvpy.solvers` module also feature a `time_integration` sub-module that defines, in the current version, two time-integration schemes: `ImplicitEuler` and `ExplicitEuler`.

#### Utils

The `bvpy.utils` module gathers various useful functions and classes regrouped in thematic submodules.

* `bvpy.utils.io`: Read and write methods.
* `bvpy.utils.post_processing`:
* `bvpy.utils.pre_processing`:
* `bvpy.utils.visu` :

## Tutorials

## Contents

### Index

### Module index

### Search



------

#### Footnotes:

[^2]: *i.e.* low-order, homogeneous, differential equations solved on highly symmetrical and smooth domains.

[^3]: Often refered to as the *weak form* or *variational formulation* of the differential equation at stake.
[^1]: This means that $DS^2$, *i.e.* Dynamical Systems with Dynamical Structures are out of the scope of the present library.

------

**IBVP description:**

First-order time-dependent differential problems, *e.g.* first line of eq.($\ref{eq:ibvp}$), can be solved by first discretizing the time derivative through a finite difference approximation. The resulting stationnary problems can then considered as a recursive set of *BVPs*. First and second line of eq.($\ref{eq:schemes}$) below shows respectively (explicit) forward and (implicit) backward Euler schemes that can be used. A tag enableling the selection of one of them is added as a fourth mandatory argument to the `ibvp` class signature.
$$
\begin{cases}
u_{n+1}(\pos) = u_n(\pos) - F(t_n, \field_n, \grad{\field_n} \dots)\Delta t\\
u_{n+1}(\pos) + F(t_{n+1}, \field_{n+1}, \grad{\field_{n+1}} \dots)\Delta t = u_n
\end{cases}
\label{eq:schemes}
$$
