.. _turing_example:


=====================
Lengyel-Epstein Model
=====================

Problem formulation
-------------------

Code
----


.. code-block:: python

  from bvpy.domains import Torus
  from bvpy.ibvp import IBVP
  from bvpy.solvers.time_integration import ImplicitEuler
  from bvpy.utils.pre_processing import create_expression
  from bvpy.vforms import CoupledTransportForm
  import numpy as np


  np.random.seed(1093)

  d = Torus(cell_size=0.2)

  k1 = 9
  k2 = 11
  dt = 0.1
  vform = CoupledTransportForm()
  vform.add_species('A', 1, lambda a, b: k1*(b-(a*b)/(1+b*b)))
  vform.add_species('B', 0.02, lambda a, b: k2-b-(4*a*b)/(1+b*b))

  random = np.random.uniform(low=-1, high=1, size=d.mesh.num_entities(2))
  state = [[1+0.04*k1**2+0.1*r,
            0.2*k2+0.1*r] for r in random]
  init = create_expression(state)

  ibvp = IBVP(domain=d,
            vform=vform,
            scheme=ImplicitEuler,
            initial_state=init)


  ibvp.integrate(0, 9, dt, 'solution.xdmf',
                linear_solver='mumps',
                absolute_tolerance=1e-10,
                relative_tolerance=1e-10)


Results
-------

.. image:: _static/lengyel_epstein_torus.gif
