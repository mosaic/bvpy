import nbformat
import re
import os
from nbconvert import HTMLExporter
from nbconvert.preprocessors import ExecutePreprocessor, ClearOutputPreprocessor

def process_notebook(input_notebook_path, output_notebook_path, dynamic_plot=False):
    """ Process a Jupyter notebook: Clear outputs, modify for PyVista and write to another path.

    This function orchestrates the following:
      1. Loads the notebook from the specified path.
      2. Clears all outputs.
      3. Modifies the notebook to support PyVista and Sphinx documentation.
      4. Write the notebook to the specified path

    Parameters
    ----------
    input_notebook_path : str
        Path to the input Jupyter notebook file (.ipynb).
    output_notebook_path : str
        Path to write the Jupyter notebook file (.ipynb).
    dynamic_plot : bool, optional
        Whether to enable PyVista dynamic plot mode, by default False.

    Raises
    ------
    FileNotFoundError
        If the notebook specified by `input_notebook_path` does not exist.
    Exception
        If any issues arise during the notebook clearing or modification

    Notes
    -----

    - This function is part of a workflow for preparing Jupyter notebooks for HTML-based documentation.
    - By enabling `dynamic_plot`, PyVista's interactive plot mode is incorporated into the notebook.
    """
    try:
        # Step 1: Load the notebook from the specified path
        if not os.path.exists(input_notebook_path):
            raise FileNotFoundError(f"Notebook not found: {input_notebook_path}")

        with open(input_notebook_path, 'r', encoding='utf-8') as f:
            notebook_content = nbformat.read(f, as_version=4)
        print(f"Loaded notebook: {input_notebook_path}")

        # Step 2: Clear outputs
        cleared_notebook = clear_notebook(notebook_content)

        # Step 3: Modify notebook content for PyVista
        modified_notebook = modify_notebook_for_pyvista(cleared_notebook,
                                                        dynamic_plot=dynamic_plot)

        # Step 4: Write the modified notebook to the specified output path
        with open(output_notebook_path, 'w', encoding='utf-8') as f:
            nbformat.write(modified_notebook, f)
        print(f"Modified notebook written to: {output_notebook_path}")

    except Exception as e:
        print(f"Unexpected error during notebook processing: {e}")
        return None

def clean_and_reset_notebook(input_notebook_path, output_notebook_path):
    """
    Clean and reset a Jupyter notebook.

    Steps:
      1. Open the input notebook.
      2. Remove output cells using clear_notebook.
      3. Undo modifications made by modify_notebook_for_pyvista by removing specific cells.
      4. Save the cleaned and reset notebook to the specified output path.

    Parameters
    ----------
    input_notebook_path : str
        Path to the input Jupyter notebook file (.ipynb).
    output_notebook_path : str
        Path to save the cleaned and reset Jupyter notebook file (.ipynb).

    """
    try:
        # Step 1: Load the notebook
        if not os.path.exists(input_notebook_path):
            raise FileNotFoundError(f"Notebook not found: {input_notebook_path}")

        with open(input_notebook_path, 'r', encoding='utf-8') as f:
            notebook_content = nbformat.read(f, as_version=4)
        print(f"Loaded notebook: {input_notebook_path}")

        # Step 2: Clear all notebook outputs
        cleared_notebook = clear_notebook(notebook_content)

        # Step 3: Undo modifications from `modify_notebook_for_pyvista`
        cleaned_cells = []
        for cell in cleared_notebook["cells"]:
            # Only keep the cell if it doesn't match the pattern added by `modify_notebook_for_pyvista`
            if cell["cell_type"] == "code" and re.search(r"# ---- for online doc only ---- #", cell["source"]):
                print("Removing cell from notebook with PyVista modifications.")
                continue  # Skip this cell
            # Add the cell to the cleaned list
            cleaned_cells.append(cell)

        # Update the notebook content with the cleaned cells
        cleared_notebook["cells"] = cleaned_cells

        # Step 4: Write the cleaned and reset notebook to output path
        with open(output_notebook_path, 'w', encoding='utf-8') as f:
            nbformat.write(cleared_notebook, f)
        print(f"Cleaned and reset notebook written to: {output_notebook_path}")

    except Exception as e:
        print(f"Error during notebook cleaning and resetting: {e}")


def clear_notebook(notebook_content):
    """ Clear all outputs in a Jupyter notebook content.

    This function removes all cell outputs and return the notebook content.

    Parameters
    ----------
    notebook_content : dict, optional
        The loaded notebook content (as a Python dictionary).
    Returns
    -------
    dict
        Cleared notebook content.
    """
    try:
        # Clear outputs from the notebook
        clear_preprocessor = ClearOutputPreprocessor()
        cleared_notebook, _ = clear_preprocessor.preprocess(notebook_content, {})
        print("Cleared notebook outputs.")
        return cleared_notebook
    except Exception as e:
        print(f"Error while clearing notebook outputs: {e}")

def modify_notebook_for_pyvista(notebook_content, dynamic_plot=False):
    """ Modify notebook content for Sphinx documentation by changing pyvista backend configurations

        TODO: follow issues to know when a full dynamic plot with `html` can be done
              https://github.com/pyvista/pyvista/issues/6250 --> add_text (title)
              https://github.com/pyvista/pyvista/issues/5389 --> labelled scalar bar

    Parameters
    ----------
    notebook_content : dict
        The loaded notebook content (as a Python dictionary).
    dynamic_plot : bool, optional
        Make the pyvista plot dynamic or static, by default False.

    Returns
    -------
    dict
        Modified notebook content.
    """
    try:
        # List to store modified cells
        new_cells = []

        # Set the PyVista backend flag
        pyvista_flag = 'html' if dynamic_plot else 'static'

        for cell in notebook_content['cells']:
            # Append the original (or modified) cell
            new_cells.append(cell)

            # Check if the cell is a code cell and contains 'import pyvista as pv'
            if cell['cell_type'] == "code" and 'import pyvista as pv' in cell['source']:
                # Create a new cell with PyVista backend configuration
                new_cell = nbformat.v4.new_code_cell(
                    source=(
                        "# ---- for online doc only ---- #\n"
                        f"pv.set_jupyter_backend('{pyvista_flag}')\n"
                        "pv.start_xvfb(3)\n"
                        "# ---- end of online doc only ---- #"
                    )
                )

                # Add metadata to hide the new cell in Sphinx-generated documentation
                new_cell['metadata']["nbsphinx"] = "hidden"
                new_cells.append(new_cell)

        # Update notebook content
        notebook_content['cells'] = new_cells
        return notebook_content

    except Exception as e:
        print(f"Error while modifying notebook: {e}")
        return None